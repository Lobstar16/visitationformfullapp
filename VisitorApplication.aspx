<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitorApplication.aspx.cs" Inherits="COV.DOC.Visitation.WebApplication.VisitorApplication" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Commonwealth of Virginia - Department of Corrections - Offenders, Adult Visitor Application and Background Investigation Authorization</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <meta name="keywords" content="virginia corrections, department of correctons, corrections, offender visitation" />
    <meta name="description" content="The Virginia Department of Corrections is a model correctional agency and a proven innovative leader in the profession." />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <!--<link href="https://vadoc.virginia.gov/scripts/allt.css" rel="stylesheet" type="text/css" />-->
    <link href="styles/reset.css" rel="stylesheet" type="text/css" />
    <link href="styles/react-day-picker.css" rel="stylesheet" type="text/css" />
    <link href="styles/rc-slider.css" rel="stylesheet" type="text/css" />
    <!--<link href="styles/main.css" rel="stylesheet" type="text/css" />-->
    <link href="styles/main.css" rel="stylesheet" type="text/css" />
    <link href="styles/visitation-form.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://vadoc.virginia.gov/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        function scrollToAnchor() {
            location.hash = "#scrollAnchor";
        }
        function hideProof() {
            var label = document.getElementById("lblVisitorProof");
            label.className = "hideThis";

            var textbox = document.getElementById("txtVisitorProof");
            textbox.className = "hideThis";
            textbox.value = "visitor";
        }
        function validateFormClient() {
            if (!window.Page_ClientValidate("Full")) {
                alert("The required fields above are missing.  Please enter the information for all required fields.");
                return false;
            }
            if (document.getElementById('txtMinorLegalFirstName') != null) {
                if (!window.Page_ClientValidate("Minor")) {
                    alert("The required Minor Visitor fields above are missing.  Please enter the information for all required Minor Visitor fields.");
                    return false;
                }
            }
            return true;
        }
        function validateFormServer() {
            alert("The required fields above are missing.  Please enter the information for all required fields.");
        }

        $(document).ready(function () {
            $('#content').hide();
            $('#spnVaResident, #spnNonVaResident').hide();

            if ($('#ddlState').val() == '1047') {
                $('#trVaResident').show();
                $('#trNonVaResident').hide();
            } else {
                $('#trVaResident').hide();
                $('#trNonVaResident').show();
            }

            $('#ddlState').change(function () {
                if ($('#ddlState').val() == '1047') {
                    $('#trVaResident').show();
                    $('#trNonVaResident').hide();
                    $('#txtCityTownOfResidence').val('');
                } else {
                    $('#trVaResident').hide();
                    $('#trNonVaResident').show();
                    $('#ddlCity').val('');
                }
            });

            if ($('#ddlCountry').val() == '2397') {
                $('#ddlState').removeAttr('disabled');
            } else {
                $('#trVaResident').hide();
                $('#trNonVaResident').show();
                $('#ddlCity').val('');
                $('#ddlState').val('');
                $('#ddlState').attr('disabled', 'disabled');
            }

            $('#ddlCountry').change(function () {
                if ($('#ddlCountry').val() == '2397') {
                    $('#ddlState').removeAttr('disabled');
                } else {
                    $('#trVaResident').hide();
                    $('#trNonVaResident').show();
                    $('#ddlCity').val('');
                    $('#ddlState').val('');
                    $('#ddlState').attr('disabled', 'disabled');
                }
            });

            if ($('#rblContactType input:checked').val() == '13') {
                $('#ddlRelationshipQualifier, #ddlRelationshipType').removeAttr('disabled');
            } else {
                $('#ddlRelationshipQualifier, #ddlRelationshipType').attr('disabled', 'disabled');
            }

            $('#rblContactType input').change(function () {
                if ($('#rblContactType input:checked').val() == '13') {
                    $('#ddlRelationshipQualifier, #ddlRelationshipType').removeAttr('disabled');
                } else {
                    $('#ddlRelationshipQualifier, #ddlRelationshipType').val('');
                    $('#ddlRelationshipQualifier, #ddlRelationshipType').attr('disabled', 'disabled');
                }
            });

            if ($('#rblMinorContactType input:checked').val() == '13') {
                $('#ddlMinorRelationshipQualifier, #ddlMinorRelationshipType').removeAttr('disabled');
            } else {
                $('#ddlMinorRelationshipQualifier, #ddlMinorRelationshipType').attr('disabled', 'disabled');
            }

            $('#rblMinorContactType input').change(function () {
                if ($('#rblMinorContactType input:checked').val() == '13') {
                    $('#ddlMinorRelationshipQualifier, #ddlMinorRelationshipType').removeAttr('disabled');
                } else {
                    $('#ddlMinorRelationshipQualifier, #ddlMinorRelationshipType').val('');
                    $('#ddlMinorRelationshipQualifier, #ddlMinorRelationshipType').attr('disabled', 'disabled');
                }
            });

            if ($('#rblHaveVehicle input:checked').val() == 'Yes') {
                $('#txtVehicleMake, #txtVehicleModel, #ddlVehicleYear, #txtVehiclePlateNumber').removeAttr('disabled');
            } else {
                $('#txtVehicleMake, #txtVehicleModel, #ddlVehicleYear, #txtVehiclePlateNumber').attr('disabled', 'disabled');
            }

            $('#rblHaveVehicle input').change(function () {
                if ($('#rblHaveVehicle input:checked').val() == 'Yes') {
                    $('#txtVehicleMake, #txtVehicleModel, #ddlVehicleYear, #txtVehiclePlateNumber').removeAttr('disabled');
                } else {
                    $('#txtVehicleMake, #txtVehicleModel, #ddlVehicleYear, #txtVehiclePlateNumber').attr('disabled', 'disabled');
                }
            });

            if ($('#rblUnderActiveParole input:checked').val() == 'Yes') {
                $('#ddlProbationParoleDistrict, #txtDocIdNumber').removeAttr('disabled');
            } else {
                $('#ddlProbationParoleDistrict, #txtDocIdNumber').attr('disabled', 'disabled');
            }

            $('#rblUnderActiveParole input').change(function () {
                if ($('#rblUnderActiveParole input:checked').val() == 'Yes') {
                    $('#ddlProbationParoleDistrict, #txtDocIdNumber').removeAttr('disabled');
                } else {
                    $('#ddlProbationParoleDistrict, #txtDocIdNumber').attr('disabled', 'disabled');
                }
            });
        });
    </script>
    <style type="text/css">
        .hideThis {
            display: none;
        }

        .pnlMinors {
            margin: 0 2% 0 2%;
        }

        #content {
            display: none;
        }
        /*#root{display: none;}*/
    </style>
</head>
<body class="visitation-form-section">
    <!-- Header -->
    <!--#include virtual="/includes/topv1.htm"-->

    <!-- Breadcrumbs -->
    <!--#include virtual="/includes/breadcrumbs.htm"-->
    <!-- React Visitation Form -->
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
    <script type="text/javascript" src="/static/js/main.e22f7c1b.js"></script>
    <!--End React Visitation Form -->

    <div id="content">
        <div id="divFormError" runat="server" visible="False">
            <h1>Adult Visitor Application and Background Investigation Authorization</h1>
            <p>A form submission error has occurred. If this problem persists, please <a href="mailto:webmaster@vadoc.virginia.gov?subject=Visitation%20Form%20Error">contact us</a>.</p>
            <br />
        </div>
        <form id="form1" runat="server">
            <div>
                <h1>Adult Visitor Application and Background Investigation Authorization</h1>
                <p>
                    By completing this request and authorization, I acknowledge that visitation of offenders at this VADOC facility is a 
                    privilege. This privilege may be revoked or suspended for violation of rules, overcrowding, or as a result of suspicious 
                    behavior. A visiting brochure is available upon request.
                </p>
                <br />
            </div>

            <table style="width: 100%; border-collapse: collapse; border: 1px solid black;">
                <tr>
                    <td colspan="2" style="text-align: center; font-weight: bold;">Visitor Information
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 360px;">
                        <asp:Label runat="server" ID="lblEmancipatedMinor" Text="Check Box if Emancipated Minor" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkEmancipatedMinor" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVisitorLegalFirstName" AssociatedControlID="txtVisitorLegalFirstName" Text="Visitor's Legal First Name" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVisitorLegalFirstName" MaxLength="50" Width="200px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvVisitorLegalFirstName" ControlToValidate="txtVisitorLegalFirstName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVisitorLegalMiddleName" AssociatedControlID="txtVisitorLegalMiddleName" Text="Visitor's Legal Middle Name" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVisitorLegalMiddleName" MaxLength="50" Width="200px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvVisitorLegalMiddleName" ControlToValidate="txtVisitorLegalMiddleName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVisitorLegalLastName" AssociatedControlID="txtVisitorLegalLastName" Text="Visitor's Legal Last Name" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVisitorLegalLastName" MaxLength="50" Width="200px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvVisitorLegalLastName" ControlToValidate="txtVisitorLegalLastName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVisitorMaidenName" AssociatedControlID="txtVisitorMaidenName" Text="Visitor's Maiden Name" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVisitorMaidenName" MaxLength="50" Width="200px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVisitorAliasName" AssociatedControlID="txtVisitorAliasName" Text="Visitor's Alias Name" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVisitorAliasName" MaxLength="50" Width="200px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblIdNumber" AssociatedControlID="txtIdNumber" Text="ID Number" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtIdNumber" MaxLength="30" Width="200px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvIdNumber" ControlToValidate="txtIdNumber" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:DropDownList runat="server" ID="ddlIdType" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvIdType" ControlToValidate="ddlIdType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblSsn" AssociatedControlID="txtSsn" Text="SSN (last 4)" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSsn" MaxLength="4" Width="70px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvSsn" ControlToValidate="txtSsn" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:RegularExpressionValidator runat="server" ID="revSsn" ControlToValidate="txtSsn" ValidationExpression="^(?!0{4})\d{4}$" ErrorMessage="Invalid last 4 of SSN value" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblRace" AssociatedControlID="ddlRace" Text="Race" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlRace" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvRace" ControlToValidate="ddlRace" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblEthnicOrigin" AssociatedControlID="ddlEthnicOrigin" Text="Ethnic Origin" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlEthnicOrigin" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblGender" AssociatedControlID="ddlGender" Text="Sex" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlGender" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvGender" ControlToValidate="ddlGender" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblHairColor" AssociatedControlID="ddlHairColor" Text="Hair Color" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlHairColor" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvHairColor" ControlToValidate="ddlHairColor" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblEyeColor" AssociatedControlID="ddlEyeColor" Text="Eye Color" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlEyeColor" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvEyeColor" ControlToValidate="ddlEyeColor" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblHeight" AssociatedControlID="txtHeightFeet" Text="Height" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtHeightFeet" MaxLength="2" Width="20px" />
                        Ft.
                            <asp:RequiredFieldValidator runat="server" ID="rfvHeightFeet" ControlToValidate="txtHeightFeet" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:RangeValidator runat="server" ID="rngHeightFeet" ControlToValidate="txtHeightFeet" ErrorMessage="Value must be a number between 2 and 10" Type="Integer" MinimumValue="2" MaximumValue="10" Display="Dynamic" ValidationGroup="Full" />
                        <asp:TextBox runat="server" ID="txtHeightInches" MaxLength="2" Width="20px" />
                        In.
                            <asp:RequiredFieldValidator runat="server" ID="rfvHeightInches" ControlToValidate="txtHeightInches" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:RangeValidator runat="server" ID="rngHeightInches" ControlToValidate="txtHeightInches" ErrorMessage="Value must be a number between 0 and 11" Type="Integer" MinimumValue="0" MaximumValue="11" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblWeight" AssociatedControlID="txtWeight" Text="Weight" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtWeight" MaxLength="3" Width="40px" />
                        Lbs.
                            <asp:RequiredFieldValidator runat="server" ID="rfvWeight" ControlToValidate="txtWeight" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:RangeValidator runat="server" ID="rngWeight" ControlToValidate="txtWeight" ErrorMessage="Value must be a number between 0 and 999" Type="Integer" MinimumValue="0" MaximumValue="999" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblDateOfBirth" AssociatedControlID="ddlMonthDateOfBirth" Text="Date of Birth" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlMonthDateOfBirth" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvMonthDateOfBirth" ControlToValidate="ddlMonthDateOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:DropDownList runat="server" ID="ddlDayDateOfBirth" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvDayDateOfBirth" ControlToValidate="ddlDayDateOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:DropDownList runat="server" ID="ddlYearDateOfBirth" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvYearDateOfBirth" ControlToValidate="ddlYearDateOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:CustomValidator runat="server" ID="cvDateOfBirth" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblPlaceOfBirth" AssociatedControlID="txtPlaceOfBirth" Text="Place of Birth (City/County, State, and Country)" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPlaceOfBirth" MaxLength="100" Width="240px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvPlaceOfBirth" ControlToValidate="txtPlaceOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
            </table>
            <br />
            <br />

            <table style="width: 100%; border-collapse: collapse; border: 1px solid black;">
                <tr>
                    <td colspan="2" style="text-align: center; font-weight: bold;">Your Current Mailing Address
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 360px;">
                        <asp:Label runat="server" ID="lblStreetAddress" AssociatedControlID="txtStreetAddress" Text="Street Address/PO Box" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtStreetAddress" MaxLength="50" Width="240px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvStreetAddress" ControlToValidate="txtStreetAddress" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblState" AssociatedControlID="ddlState" Text="State" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlState" />
                        <asp:CustomValidator runat="server" ID="cvState" ControlToValidate="ddlState" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr id="trVaResident">
                    <td style="text-align: right">
                        <span id="spnVaResident">If you reside in the state of Virginia, please select a city or town from the following dropdown list. Virginia Resident:
                        </span>
                        <asp:Label runat="server" ID="lblDdlCity" AssociatedControlID="ddlCity" Text="City/Town/Other Principal Subdivision" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlCity" />
                        <asp:CustomValidator runat="server" ID="cvDdlCity" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr id="trNonVaResident">
                    <td style="text-align: right">
                        <span id="spnNonVaResident">If you reside outside of the state of Virginia, please enter your city or town in the free form field following the drop down list. Non-Virginia Resident:
                        </span>
                        <asp:Label runat="server" ID="lblTxtCity" AssociatedControlID="txtCityTownOfResidence" Text="City/Town/Other Principal Subdivision" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCityTownOfResidence" MaxLength="25" Width="240px" />
                        <asp:CustomValidator runat="server" ID="cvTxtCity" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblZip" AssociatedControlID="txtZip" Text="Postal Code (ZIP)" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtZip" MaxLength="10" Width="100px" />
                        <asp:CustomValidator runat="server" ID="cvZip" ControlToValidate="txtZip" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblCountry" AssociatedControlID="ddlCountry" Text="Country" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlCountry" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvCountry" ControlToValidate="ddlCountry" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblEmailAddress" AssociatedControlID="txtEmailAddress" Text="E-mail Address" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEmailAddress" MaxLength="254" Width="240px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvEmailAddress" ControlToValidate="txtEmailAddress" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:RegularExpressionValidator ID="revEmailAddress" runat="server" ValidationExpression="[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?" ControlToValidate="txtEmailAddress" ErrorMessage="Invalid e-mail address" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblPhoneNumber" AssociatedControlID="txtPhoneNumber" Text="Phone Number" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPhoneNumber" MaxLength="20" Width="100px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvPhoneNumber" ControlToValidate="txtPhoneNumber" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <asp:DropDownList runat="server" ID="ddlPhoneType" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvPhoneType" ControlToValidate="ddlPhoneType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
            </table>
            <br />

            <asp:Panel runat="server" ID="pnlOffenderList" />
            <br />

            <a name="scrollAnchor"></a>

            <asp:Panel runat="server" ID="pnlOffenders">
                <table style="width: 100%; border-collapse: collapse; border: 1px solid black;">
                    <asp:HiddenField runat="server" ID="hdnOffenderId" />
                    <tr>
                        <td colspan="2" style="text-align: center; font-weight: bold;">Information on Offender You Want to Visit
                                <asp:CustomValidator runat="server" ID="cvOffenders" ErrorMessage="You must enter information for at least one Offender." Display="Dynamic" ValidationGroup="Full" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 360px;">
                            <asp:Label runat="server" ID="lblOffenderName" AssociatedControlID="txtOffenderName" Text="Offender's Incarcerated Name (First and Last)" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtOffenderName" MaxLength="150" Width="200px" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvOffenderName" ControlToValidate="txtOffenderName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Offender" />
                            <a href="http://vadoc.virginia.gov/offenders/locator/" target="_blank">Offender Locator</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:Label runat="server" ID="lblOffenderNumber" AssociatedControlID="txtOffenderNumber" Text="Offender's Incarcerated Number" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtOffenderNumber" MaxLength="7" Width="200px" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvOffenderNumber" ControlToValidate="txtOffenderNumber" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Offender" />
                            <asp:RegularExpressionValidator runat="server" ID="revOffenderNumber" ControlToValidate="txtOffenderNumber" ValidationExpression="^\d{7}$" ErrorMessage="Value must be a 7 digit number" Display="Dynamic" ValidationGroup="Offender" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:Label runat="server" ID="lblOffenderFacility" AssociatedControlID="ddlOffenderFacility" Text="Offender's Facility" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlOffenderFacility" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvOffenderFacility" ControlToValidate="ddlOffenderFacility" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Offender" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:Label runat="server" ID="lblLegalRelationshipToOffender" AssociatedControlID="ddlRelationshipQualifier" Text="Your relationship to Offender" />
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblContactType" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <Items>
                                    <asp:ListItem Text="Family" Value="13" />
                                    <asp:ListItem Text="Friend" Value="6" />
                                </Items>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator runat="server" ID="rfvContactType" ControlToValidate="rblContactType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Offender" /><br />

                            <asp:DropDownList runat="server" ID="ddlRelationshipQualifier" />
                            <asp:DropDownList runat="server" ID="ddlRelationshipType" />
                            <asp:CustomValidator runat="server" ID="cvRelationshipType" ControlToValidate="ddlRelationshipType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Offender" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="float: right; padding: 0 0 15px 0">To add minor visitors for this offender, click the Add Minor Visitor button.
                                    <asp:Button runat="server" ID="btnAddMinor" Text="Add Minor Visitor" OnClick="btnAddMinor_Click" ValidationGroup="Offender" />
                            </span>
                            <br />

                            <asp:Panel runat="server" ID="pnlMinorList" />
                            <br />

                            <asp:Panel runat="server" ID="pnlMinors" Visible="False" CssClass="pnlMinors">
                                <table style="width: 100%; border-collapse: collapse; border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                    <asp:HiddenField runat="server" ID="hdnMinorId" />
                                    <tr>
                                        <td colspan="2" style="text-align: center; font-weight: bold;">Minor Visitor Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; width: 337px;">
                                            <asp:Label runat="server" ID="lblMinorLegalFirstName" AssociatedControlID="txtMinorLegalFirstName" Text="Minor Visitor's Legal First Name" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtMinorLegalFirstName" MaxLength="50" Width="200px" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorLegalFirstName" ControlToValidate="txtMinorLegalFirstName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorLegalMiddleName" AssociatedControlID="txtMinorLegalMiddleName" Text="Minor Visitor's Legal Middle Name" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtMinorLegalMiddleName" MaxLength="50" Width="200px" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorLegalMiddleName" ControlToValidate="txtMinorLegalMiddleName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorLegalLastName" AssociatedControlID="txtMinorLegalLastName" Text="Minor Visitor's Legal Last Name" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtMinorLegalLastName" MaxLength="50" Width="200px" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorLegalLastName" ControlToValidate="txtMinorLegalLastName" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorIdNumber" AssociatedControlID="txtMinorIdNumber" Text="ID Number" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtMinorIdNumber" MaxLength="25" Width="200px" />
                                            <asp:DropDownList runat="server" ID="ddlMinorIdType" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorSsn" AssociatedControlID="txtMinorSsn" Text="SSN (last 4)" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtMinorSsn" MaxLength="4" Width="70px" />
                                            <asp:RegularExpressionValidator runat="server" ID="revMinorSsn" ControlToValidate="txtMinorSsn" ValidationExpression="^\d{4,4}$" ErrorMessage="Value must be a 4 digit number" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorRace" AssociatedControlID="ddlMinorRace" Text="Race" />
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlMinorRace" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorRace" ControlToValidate="ddlMinorRace" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorEthnicOrigin" AssociatedControlID="ddlMinorEthnicOrigin" Text="Ethnic Origin" />
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlMinorEthnicOrigin" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorGender" AssociatedControlID="ddlMinorGender" Text="Sex" />
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlMinorGender" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorGender" ControlToValidate="ddlMinorGender" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorDateOfBirth" AssociatedControlID="ddlMinorMonthDateOfBirth" Text="Date of Birth" />
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlMinorMonthDateOfBirth" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorMonthDateOfBirth" ControlToValidate="ddlMinorMonthDateOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                            <asp:DropDownList runat="server" ID="ddlMinorDayDateOfBirth" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorDayDateOfBirth" ControlToValidate="ddlMinorDayDateOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                            <asp:DropDownList runat="server" ID="ddlMinorYearDateOfBirth" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorYearDateOfBirth" ControlToValidate="ddlMinorYearDateOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorPlaceOfBirth" AssociatedControlID="txtMinorPlaceOfBirth" Text="Place of Birth (City/County, State, and Country)" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtMinorPlaceOfBirth" MaxLength="100" Width="240px" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorPlaceOfBirth" ControlToValidate="txtMinorPlaceOfBirth" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label runat="server" ID="lblMinorRelationshipToOffender" AssociatedControlID="ddlMinorRelationshipQualifier" Text="Minor's relationship to Offender" />
                                        </td>
                                        <td>
                                            <asp:RadioButtonList runat="server" ID="rblMinorContactType" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <Items>
                                                    <asp:ListItem Text="Family" Value="13" />
                                                    <asp:ListItem Text="Friend" Value="6" />
                                                </Items>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorContactType" ControlToValidate="rblMinorContactType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" /><br />

                                            <asp:DropDownList runat="server" ID="ddlMinorRelationshipQualifier" />
                                            <asp:DropDownList runat="server" ID="ddlMinorRelationshipType" />
                                            <asp:CustomValidator runat="server" ID="cvMinorRelationshipType" ControlToValidate="ddlMinorRelationshipType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%; border-collapse: collapse; border-right: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    <tr>
                                        <td colspan="2" style="text-align: center; font-weight: bold;">Conditions (Minor Visitor)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 96px; padding: 0 0 0 20px;">
                                            <asp:RadioButtonList runat="server" ID="rblMinorFelonyConviction" RepeatDirection="Horizontal">
                                                <Items>
                                                    <asp:ListItem Text="Yes" Value="Yes" />
                                                    <asp:ListItem Text="No" Value="No" />
                                                </Items>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorFelonyConviction" ControlToValidate="rblMinorFelonyConviction" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                        <td style="padding: 0 20px 0 0;">
                                            <asp:Label runat="server" ID="lblMinorFelonyConviction" AssociatedControlID="rblMinorFelonyConviction" Text="Has the minor been convicted of a felony in any jurisdiction?" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0 0 0 20px;">
                                            <asp:RadioButtonList runat="server" ID="rblMinorUnderActiveCourtSupervision" RepeatDirection="Horizontal">
                                                <Items>
                                                    <asp:ListItem Text="Yes" Value="Yes" />
                                                    <asp:ListItem Text="No" Value="No" />
                                                </Items>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorUnderActiveCourtSupervision" ControlToValidate="rblMinorUnderActiveCourtSupervision" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                        <td style="padding: 0 20px 0 0;">
                                            <asp:Label runat="server" ID="lblMinorUnderActiveCourtSupervision" AssociatedControlID="rblMinorUnderActiveCourtSupervision" Text="Is the minor currently under active Court supervision?" /><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0 0 0 20px;">
                                            <asp:RadioButtonList runat="server" ID="rblMinorVictimOfCurrentCrime" RepeatDirection="Horizontal">
                                                <Items>
                                                    <asp:ListItem Text="Yes" Value="Yes" />
                                                    <asp:ListItem Text="No" Value="No" />
                                                </Items>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorVictimOfCurrentCrime" ControlToValidate="rblMinorVictimOfCurrentCrime" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                        <td style="padding: 0 20px 0 0;">
                                            <asp:Label runat="server" ID="lblMinorVictimOfCurrentCrime" AssociatedControlID="rblMinorVictimOfCurrentCrime" Text="Is the minor a victim or family of a victim of the current crime committed by the offender with whom they wish to visit?" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0 0 0 20px;">
                                            <asp:RadioButtonList runat="server" ID="rblMinorAssociatedWithGang" RepeatDirection="Horizontal">
                                                <Items>
                                                    <asp:ListItem Text="Yes" Value="Yes" />
                                                    <asp:ListItem Text="No" Value="No" />
                                                </Items>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvMinorAssociatedWithGang" ControlToValidate="rblMinorAssociatedWithGang" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Minor" />
                                        </td>
                                        <td style="padding: 0 20px 0 0;">
                                            <asp:Label runat="server" ID="lblMinorAssociatedWithGang" AssociatedControlID="rblMinorAssociatedWithGang" Text="Is the minor now or have they ever been a member or associated with any gang, motorcycle club, racial supremacy group, or other such group or organization as defined in Code of Virginia &sect;18.2-46.1?" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 0 20px 10px 20px; border-bottom: 1px solid black; font-weight: bold;">
                                            <asp:CheckBox runat="server" ID="chkMinorParentOrGuardian" />
                                            I am the parent or legal guardian of the minor identified above or I am presenting written notarized approval from the parent or legal guardian for the minor to visit with me.
                                                <span style="float: right;">
                                                    <asp:Button runat="server" ID="btnSaveMinor" Text="Save Minor Visitor" OnClick="btnSaveMinor_Click" ValidationGroup="Minor" />
                                                </span>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="float: right; padding: 0 0 10px 0;">To add multiple offenders, enter offender information and click Save Offender.
                                    <asp:Button runat="server" ID="btnSaveOffender" Text="Save Offender" OnClick="btnSaveOffender_Click" ValidationGroup="Offender" />
                            </span>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </asp:Panel>

            <table style="width: 100%; border-collapse: collapse; border: 1px solid black;">
                <tr>
                    <td colspan="2" style="text-align: center; font-weight: bold;">Vehicle Information
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 360px;">
                        <asp:Label runat="server" ID="lblHaveVehicle" AssociatedControlID="rblHaveVehicle" Text="Own a vehicle?" />
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblHaveVehicle" RepeatDirection="Horizontal">
                            <Items>
                                <asp:ListItem Text="Yes" Value="Yes" Selected="True" />
                                <asp:ListItem Text="No" Value="No" />
                            </Items>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvHaveVehicle" ControlToValidate="rblHaveVehicle" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVehicleMake" AssociatedControlID="txtVehicleMake" Text="Make" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVehicleMake" MaxLength="30" Width="200px" />
                        <asp:CustomValidator runat="server" ID="cvVehicleMake" ControlToValidate="txtVehicleMake" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVehicleModel" AssociatedControlID="txtVehicleModel" Text="Model" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVehicleModel" MaxLength="30" Width="200px" />
                        <asp:CustomValidator runat="server" ID="cvVehicleModel" ControlToValidate="txtVehicleModel" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVehicleYear" AssociatedControlID="ddlVehicleYear" Text="Year" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlVehicleYear" />
                        <asp:CustomValidator runat="server" ID="cvVehicleYear" ControlToValidate="ddlVehicleYear" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label runat="server" ID="lblVehiclePlateNumber" AssociatedControlID="txtVehiclePlateNumber" Text="Plate Number" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtVehiclePlateNumber" MaxLength="20" Width="120px" />
                        <asp:CustomValidator runat="server" ID="cvVehiclePlateNumber" ControlToValidate="txtVehiclePlateNumber" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
            </table>
            <br />
            <br />

            <table style="width: 100%; border-collapse: collapse; border: 1px solid black;">
                <tr>
                    <td colspan="2" style="text-align: center; font-weight: bold;">Conditions
                    </td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:RadioButtonList runat="server" ID="rblFelonyConviction" RepeatDirection="Horizontal">
                            <Items>
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No" Value="No" />
                            </Items>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvFelonyConviction" ControlToValidate="rblFelonyConviction" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblFelonyConviction" AssociatedControlID="rblFelonyConviction" Text="Have you been convicted of a felony in any jurisdiction?" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblEmployeeOfDoc" RepeatDirection="Horizontal">
                            <Items>
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No" Value="No" />
                            </Items>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvEmployeeOfDoc" ControlToValidate="rblEmployeeOfDoc" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblEmployeeOfDoc" AssociatedControlID="rblEmployeeOfDoc" Text="Have you ever been employed by, volunteered with, or contracted by the Virginia Department of Corrections or Virginia Department of Correctional Education?" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblUnderActiveParole" RepeatDirection="Horizontal">
                            <Items>
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No" Value="No" />
                            </Items>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvUnderActiveParole" ControlToValidate="rblUnderActiveParole" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblUnderActiveParole" AssociatedControlID="rblUnderActiveParole" Text="Are you currently under active parole or probation supervision?" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblProbationParoleDistrict" AssociatedControlID="ddlProbationParoleDistrict" Text="P&amp;P District:" />
                        <asp:DropDownList runat="server" ID="ddlProbationParoleDistrict" />&nbsp;&nbsp;&nbsp;
                            <asp:Label runat="server" ID="lblDocIdNumber" AssociatedControlID="txtDocIdNumber" Text="DOC ID Number:" />
                        <asp:TextBox runat="server" ID="txtDocIdNumber" MaxLength="7" />
                        <asp:CompareValidator runat="server" ID="cmpDocIdNumber" ControlToValidate="txtDocIdNumber" Operator="DataTypeCheck" Type="Integer" ErrorMessage="Value must be a number" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblVictimOfCurrentCrime" RepeatDirection="Horizontal">
                            <Items>
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No" Value="No" />
                            </Items>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvVictimOfCurrentCrime" ControlToValidate="rblVictimOfCurrentCrime" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblVictimOfCurrentCrime" AssociatedControlID="rblVictimOfCurrentCrime" Text="Are you a victim or family of a victim of the current crime committed by the offender with whom you wish to visit?" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblAssociatedWithGang" RepeatDirection="Horizontal">
                            <Items>
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No" Value="No" />
                            </Items>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvAssociatedWithGang" ControlToValidate="rblAssociatedWithGang" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblAssociatedWithGang" AssociatedControlID="rblAssociatedWithGang" Text="Are you now or have you ever been a member or associated with any gang, motorcycle club, racial supremacy group, or other such group or organization as defined in Code of Virginia &sect;18.2-46.1?" />
                    </td>
                </tr>
            </table>
            <br />

            <table style="width: 100%; border: 1px solid black; border-collapse: collapse;">
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblVisitorProof" AssociatedControlID="txtVisitorProof" Text="Please type &quot;visitor&quot;" />
                        <asp:TextBox runat="server" ID="txtVisitorProof" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="chkAuthorizeRecordCheck" />
                        <asp:CustomValidator runat="server" ID="cvAuthorizeRecordCheck" ErrorMessage="Required" Display="Dynamic" ValidationGroup="Full" />
                        <span style="font-weight: bold;">I authorize the Department of Corrections to conduct a criminal records check, or to use any Department of Corrections records to verify accuracy of information provided on this form.<br />
                            <br />
                            The above information is true and correct. I understand that providing false information on this form is grounds for denying visiting privileges. By clicking the Submit button, I agree that I have read and understand the above statements.
                        </span>
                        <span style="float: right; padding: 0 0 10px 0;">
                            <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClientClick="return validateFormClient();" OnClick="btnSubmit_Click" ValidationGroup="Full" />
                        </span>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <!-- Footer -->
    <!--#include virtual="/includes/footer.htm"-->
</body>
</html>
