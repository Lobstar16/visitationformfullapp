﻿using System;

namespace COV.DOC.Visitation.WebApplication
{
    /// <summary>
    /// ApplicationSubmitted class
    /// </summary>
    public partial class ApplicationSubmitted : System.Web.UI.Page
    {
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}
