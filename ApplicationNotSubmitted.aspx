<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplicationNotSubmitted.aspx.cs" Inherits="COV.DOC.Visitation.WebApplication.ApplicationNotSubmitted" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>Commonwealth of Virginia - Department of Corrections - Offenders, Adult Visitor Application and Background Investigation Authorization</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
        <meta name="keywords" content="virginia corrections, department of correctons, corrections, offender visitation" />
        <meta name="description" content="The Virginia Department of Corrections is a model correctional agency and a proven innovative leader in the profession." />
        <link href="https://vadoc.virginia.gov/scripts/allt.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <!--#include virtual="/includes/topv1.htm"-->
        
        <div id="breadcrumb">
            <div class="breadCrumbText"><a href="https://vadoc.virginia.gov/default.shtm" accesskey="1" >Home</a> &gt; <a href="https://vadoc.virginia.gov/offenders/default.shtm">Offenders</a> &gt; Adult Visitor Application and Background Investigation Authorization</div>
    	    
            <!-- #include virtual="/includes/searchBar.htm" -->
        </div>
        
        <!-- #include virtual="/includes/left.htm" -->
        
        <div id="content">
            <h1>Adult Visitor Application and Background Investigation Authorization</h1>
            <h3><asp:Literal runat="server" ID="litSubmissionMessage" Text="Your visitation application submission failed." /></h3>
            <p>Your visitation application submission failed due to a system error. This error has been recorded, and the VADOC's technical staff have been notified. Please try again later.</p>
        </div>
        
        <!-- #include virtual="/includes/bottom.htm" -->
    </body>
</html>