﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.Schema;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace COV.DOC.Visitation.WebApplication
{
    /// <summary>
    /// VisitorApplication class
    /// </summary>
    public partial class VisitorApplication : System.Web.UI.Page
    {
#pragma warning disable 1591
        ///<Summary>
        /// Indicates if submitted by JS
        ///</Summary>
        public bool jsSubmission = false;

        /// Final list of primary applicant form values

        [WebMethod]
        public static HttpResponseMessage SubmitReactVisitationForm(VisitationApplicationSubmission submission)
        {

            List<MinorVisitor> MinorVisitors = new List<MinorVisitor>();
            List<Offender> Offenders = new List<Offender>();

            string minorsJson = submission.SelectedMinorsJson;
            //List of offenders as JSON
            string offendersJson = submission.SelectedOffendersJson;

            // Deserialize Offenders and Minors Json objects into list of models
            submission.SelectedMinors = JsonConvert.DeserializeObject<List<MinorVisitor>>(minorsJson);
            submission.SelectedOffenders = JsonConvert.DeserializeObject<List<Offender>>(offendersJson);

            // Add all selected offenders to Offenders List
            if(submission.SelectedOffenders != null)
            {
                foreach (var offender in submission.SelectedOffenders)
                {
                    Offenders.Add(offender);
                }
            }

            // Add all selected Minors to MInorVisitors List
            if (submission.SelectedMinors != null)
            {
                foreach (var minor in submission.SelectedMinors)
                {
                    MinorVisitors.Add(minor);
                }
            }

            //Set final form values
            string applicantFirstName = submission.LegalFirstName;
            string applicantLastName = submission.LegalLastName;
            string applicantMiddleName = submission.LegalMiddleName;
            string applicantMaidenName = submission.LegalMaidenName;
            string applicantAlias = submission.Alias;

            string applicantRehire = submission.ApplicantRehire;
            string meetOffender = submission.MeetOffender;
            string employmentStatus = submission.EmploymentStatus;
            string employmentStartDate = submission.EmploymentStartDate.ToString("yyyy-MM-dd");
            string employmentEndDate = submission.EmploymentEndDate.ToString("yyyy-MM-dd");
            string employmentTitle = submission.EmploymentTitle;
            string employmentAlias = submission.EmploymentAlias;
            string applicantIdCardNumber = submission.ApplicantIdNumber;
            string applicantIdCardType = submission.ApplicantIdType;
            string applicantIdTypeName = submission.ApplicantIdTypeName;
            string applicantSsn = submission.ApplicantSsn;
            string applicantRace = submission.ApplicantRace;
            string applicantRaceName = submission.ApplicantRaceName;
            string applicantGender = submission.ApplicantSex;
            string applicantGenderName = submission.ApplicantSexName;
            string applicantDob = submission.ApplicantDOB.ToString("yyyy-MM-dd");
            string applicantPob = submission.ApplicantPlaceOfBirth;
            string applicantEthnicOrigin = submission.EthnicOrigin;
            string applicantHair = submission.ApplicantHair;
            string applicantHairName = submission.ApplicantHairName;
            string applicantEyes = submission.ApplicantEyes;
            string applicantEyesName = submission.ApplicantEyesName;
            string applicantHeightFt = submission.ApplicantHeightFt;
            string applicantHeightIn = submission.ApplicantHeightIn;
            string applicantWeight = submission.ApplicantWeight;
            string applicantStreet = submission.ApplicantStreet;
            string applicantCity = submission.ApplicantCity;
            string applicantOutOfStateCity = submission.ApplicantCity;
            string applicantState = submission.ApplicantState;
            string applicantStateNumCode = submission.ApplicantStateNumCode;
            string applicantZip = submission.ApplicantZip;
            string applicantCountryCode = submission.ApplicantCountryCode;
            string applicantCountryName = submission.ApplicantCountryName;
            string applicantEmail = submission.ApplicantEmail;
            string applicantPhoneNumber = submission.ApplicantPhone;
            string applicantPhoneType = submission.ApplicantPhoneType;
            string applicantPhoneTypeName = submission.ApplicantPhoneTypeName;
            string applicantFelony = submission.FelonyConviction;
            string applicantGang = submission.GangAssociation;
            string applicantParole = submission.ActiveParole;
            string applicantParoleDistrict = submission.PpDistrict;
            string applicantVadocId = submission.VADOCId;
            string applicantVadocEmployee = submission.VADOCEmployee;
            bool applicantVictim = submission.MinorCertification;
            bool applicantAuthorization = submission.FinalAuthorization;

            bool GenerateXmlFile()
            {
                // get the xsd file
                var offenderVisitationXsdFile = ConfigurationManager.AppSettings["offenderVisitationXsdFile"];
                
                // if xsd file does not exist, notify someone
                if (!File.Exists(offenderVisitationXsdFile))
                {
                    var message = new MailMessage
                    {
                        From =
                            new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"],
                                "Visitation Applications (VADOC)")
                    };
                    message.To.Add(new MailAddress(ConfigurationManager.AppSettings["offenderVisitationXmlEmailAddress"]));
                    message.Subject = "Public Website - Visitation Issue - XSD File Is Missing";
                    message.Priority = MailPriority.High;
                    message.IsBodyHtml = true;

                    var body = new StringBuilder();
                    body.Append(
                        "The Offender Visitation XSD file does not exist. Please copy the file to the following location: " +
                        offenderVisitationXsdFile);

                    message.Body = body.ToString();

                    SendEmail(message);

                    return false;
                }

                var applicantId = 1;
                var minorApplicantsXElement = new XElement("minorApplicants");

                // loop through minor visitors and generate xml for them
                foreach (var minorVisitor in MinorVisitors)
                {
                    applicantId++;

                    minorVisitor.ApplicantId = applicantId;

                    minorApplicantsXElement.Add(
                        new XElement("minorApplicant",
                            new XElement("applicantId", applicantId),
                            new XElement("lastName", minorVisitor.LastName),
                            new XElement("firstName", minorVisitor.FirstName),
                            new XElement("middleName", minorVisitor.MiddleName),
                            !string.IsNullOrEmpty(minorVisitor.IdNumber)
                                ? new XElement("idCardNumber",
                                    minorVisitor.IdNumber)
                                : null,
                            !string.IsNullOrEmpty(minorVisitor.IdType)
                                ? new XElement("idCardType",
                                    minorVisitor.IdType)
                                : null,
                            !string.IsNullOrEmpty(minorVisitor.Ssn)
                                ? new XElement("ssn", minorVisitor.Ssn)
                                : null,
                            new XElement("race", minorVisitor.Race),
                            new XElement("gender", minorVisitor.Gender),
                            new XElement("birthDate",
                                minorVisitor.YearDob + "-" + minorVisitor.MonthDob + "-" + minorVisitor.DayDob),
                            new XElement("placeOfBirth", minorVisitor.PlaceOfBirth),
                            !string.IsNullOrEmpty(minorVisitor.EthnicOrigin)
                                ? new XElement("ethnicOrigin",
                                    minorVisitor.EthnicOrigin)
                                : null));
                }

                const int adultVisitorApplicantId = 1;
                var offendersXElement = new XElement("offenders");

                // loop through offenders and generate xml for them
                foreach (var offender in Offenders)
                {
                    var visitationApplicantsXElement = new XElement("visitationApplicants");

                    // adult visitor's offender entry
                    visitationApplicantsXElement.Add(
                        new XElement("visitationApplicant",
                            new XElement("applicantId", adultVisitorApplicantId),
                            new XElement("contactType", offender.ContactType),
                            offender.ContactType == "13" && !string.IsNullOrEmpty(offender.RelationshipQualifier)
                                ? new XElement("relationshipQualifier", offender.RelationshipQualifier)
                                : null,
                            offender.ContactType == "13" && !string.IsNullOrEmpty(offender.RelationshipType)
                                ? new XElement("relationshipTypeId", offender.RelationshipType)
                                : null));

                    // minor visitors' offender entries
                    var offender1 = offender;
                    var minorsForOffender = MinorVisitors.Where(x => x.OffenderId == offender1.Id);

                    if (minorsForOffender != null)
                    {
                        foreach (var minorVisitor in minorsForOffender)
                        {
                            visitationApplicantsXElement.Add(
                                new XElement("visitationApplicant",
                                    new XElement("applicantId", minorVisitor.ApplicantId),
                                    new XElement("contactType", minorVisitor.ContactType),
                                    minorVisitor.ContactType == "13" &&
                                    !string.IsNullOrEmpty(minorVisitor.RelationshipQualifier)
                                        ? new XElement("relationshipQualifier", minorVisitor.RelationshipQualifier)
                                        : null,
                                    minorVisitor.ContactType == "13" && !string.IsNullOrEmpty(minorVisitor.RelationshipType)
                                        ? new XElement("relationshipTypeId", minorVisitor.RelationshipType)
                                        : null));
                        }
                    }

                    offendersXElement.Add(
                        new XElement("offender",
                            new XElement("offenderId", offender.Number),
                            new XElement("locationId", offender.Facility),
                            visitationApplicantsXElement));
                }

                var doc = new XDocument(
                    new XElement("visitationRegistrationRequest",
                        new XElement("registrationId", Guid.NewGuid().ToString("N")),
                        new XElement("requestDate", DateTime.Now.ToString("yyyy-MM-dd")),
                        new XElement("primaryApplicant",
                            new XElement("applicantId", adultVisitorApplicantId),
                            new XElement("lastName", applicantFirstName),
                            new XElement("firstName", applicantLastName),
                            new XElement("middleName", applicantMiddleName),
                            !string.IsNullOrEmpty(applicantMaidenName)
                                ? new XElement("maidenName",
                                    applicantMaidenName)
                                : null,
                            !string.IsNullOrEmpty(applicantAlias)
                                ? new XElement("alias",
                                    applicantAlias)
                                : null,
                            new XElement("idCardNumber", applicantIdCardNumber),
                            new XElement("idCardType", applicantIdCardType),
                            new XElement("ssn", applicantSsn),
                            new XElement("race", applicantRace),
                            new XElement("gender", applicantGender),
                            new XElement("birthDate", applicantDob),
                            new XElement("placeOfBirth", applicantPob),
                            !string.IsNullOrEmpty(applicantEthnicOrigin)
                                ? new XElement("ethnicOrigin", applicantEthnicOrigin)
                                : null,
                            new XElement("hairColor", applicantHair),
                            new XElement("eyeColor", applicantEyes),
                            new XElement("heightFt", applicantHeightFt),
                            new XElement("heightIn", applicantHeightIn),
                            new XElement("weight", applicantWeight),
                            new XElement("streetAddress", applicantStreet),
                            !string.IsNullOrEmpty(applicantCity)
                                ? new XElement("outOfStateCity", applicantCity)
                                : null,
                            !string.IsNullOrEmpty(applicantStateNumCode)
                                ? new XElement("state", applicantStateNumCode)
                                : null,
                            new XElement("zipCode", applicantZip),
                            new XElement("country", applicantCountryCode),
                            new XElement("emailAddress", applicantEmail),
                            new XElement("phoneNumber", applicantPhoneNumber),
                            new XElement("phoneType", applicantPhoneType),
                            new XElement("emancipatedMinor", "N")),
                        MinorVisitors.Any() ? new XElement(minorApplicantsXElement) : null,
                        new XElement(offendersXElement)));

                var offenderVisitationXmlStagingFolder =
                    ConfigurationManager.AppSettings["offenderVisitationXmlStagingFolder"];
                var xmlFilename = "VisitorApplication_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml";

                // create staging directory, if it doesn't exist
                if (!Directory.Exists(offenderVisitationXmlStagingFolder))
                    Directory.CreateDirectory(offenderVisitationXmlStagingFolder);

                // save file to staging location for validation
                doc.Save(offenderVisitationXmlStagingFolder + xmlFilename);

                // load xml file for validation
                var xdoc = XDocument.Load(offenderVisitationXmlStagingFolder + xmlFilename);
                var schemas = new XmlSchemaSet();
                schemas.Add(string.Empty, offenderVisitationXsdFile);

                try
                {
                    // validate xml file against xsd
                    xdoc.Validate(schemas, null);
                }
                catch (XmlSchemaValidationException ex)
                {
                    // if unsuccessful, move file to failed folder
                    var offenderVisitationXmlFailedFolder =
                        ConfigurationManager.AppSettings["offenderVisitationXmlFailedFolder"];

                    // create failed directory, if it doesn't exist
                    if (!Directory.Exists(offenderVisitationXmlFailedFolder))
                        Directory.CreateDirectory(offenderVisitationXmlFailedFolder);

                    // move file
                    File.Move(offenderVisitationXmlStagingFolder + xmlFilename,
                        offenderVisitationXmlFailedFolder + xmlFilename);

                    // notify someone of failed validation
                    var message = new MailMessage
                    {
                        From =
                            new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"],
                                "Visitation Applications (VADOC)")
                    };
                    message.To.Add(new MailAddress(ConfigurationManager.AppSettings["offenderVisitationXmlEmailAddress"]));
                    message.Subject = "Public Website – Visitation Application – Message Failed XML Validation";
                    message.Priority = MailPriority.High;
                    message.IsBodyHtml = true;

                    var body = new StringBuilder();
                    body.Append(
                        "The generated Offender Visitation XML file (" + xmlFilename +
                        ") did not validate against the XSD file and has been moved to the failed folder (" +
                        offenderVisitationXmlFailedFolder + ").<br /><br />The error message is: " +
                        ex.Message);

                    message.Body = body.ToString();

                    SendEmail(message);

                    return false;
                }

                // if successful, move file to transmit folder
                var offenderVisitationXmlTransmitFolder =
                    ConfigurationManager.AppSettings["offenderVisitationXmlTransmitFolder"];

                // create transmit directory, if it doesn't exist
                if (!Directory.Exists(offenderVisitationXmlTransmitFolder))
                    Directory.CreateDirectory(offenderVisitationXmlTransmitFolder);

                // move file
                File.Move(offenderVisitationXmlStagingFolder + xmlFilename,
                    offenderVisitationXmlTransmitFolder + xmlFilename);

                return true;
            }

            void SendVisitationEmails()
            {
                var message = new MailMessage
                {
                    From =
                            new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"],
                                            "Visitation Applications (VADOC)")
                };
                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"]));
                message.Subject = "Visitation Form Submission";
                message.IsBodyHtml = true;

                const string htmlBreak = "<br />";
                var body = new StringBuilder();
                body.Append("<h3>Visitation Web Form<h3>" + htmlBreak);
                body.Append("<b>Visitor Information</b>" + htmlBreak);
                body.Append("Visitor's Legal First Name: " + applicantFirstName + htmlBreak);
                body.Append("Visitor's Legal Middle Name: " + applicantMiddleName + htmlBreak);
                body.Append("Visitor's Legal Last Name: " + applicantLastName + htmlBreak);
                body.Append("Visitor's Maiden Name: " + applicantMaidenName + htmlBreak);
                body.Append("Visitor's Alias Name: " + applicantAlias + htmlBreak);
                body.Append("E-mail Address: " + applicantEmail + htmlBreak);
                body.Append("Date of Birth: " + applicantDob + htmlBreak);
                body.Append("Place of Birth: " + applicantPob + htmlBreak);
                body.Append("Race: " + applicantRaceName + htmlBreak);
                body.Append("Ethnic Origin: " + applicantEthnicOrigin + htmlBreak);
                body.Append("Gender: " + applicantGenderName + htmlBreak);
                body.Append("Eye Color: " + applicantEyesName + htmlBreak);
                body.Append("Hair Color: " + applicantHairName + htmlBreak);
                body.Append("Height: " + applicantHeightFt + " Ft. " + applicantHeightIn + " In." + htmlBreak);
                body.Append("Weight: " + applicantWeight + " Lbs." + htmlBreak);
                body.Append("Street Address/PO Box: " + applicantStreet + htmlBreak);
                body.Append("City/Town/Other Principal Subdivision: " + applicantCity + htmlBreak);
                body.Append("State: " + applicantState + htmlBreak);
                body.Append("Postal Code (ZIP): " + applicantZip + htmlBreak);
                body.Append("Country: " + applicantCountryName + htmlBreak);
                body.Append("Phone Number: " + applicantPhoneNumber + htmlBreak);
                body.Append("Phone Type: " + applicantPhoneTypeName + htmlBreak);
                body.Append("ID Number: " + applicantIdCardNumber + htmlBreak);
                body.Append("ID Type: " + applicantIdTypeName + htmlBreak);
                body.Append("SSN (last 4): " + applicantSsn + htmlBreak + htmlBreak);

                foreach (var offender in Offenders)
                {
                    body.Append("<b>Information on Offender</b>" + htmlBreak);
                    body.Append("Offender's Incarcerated Name: " + offender.Name + htmlBreak);
                    body.Append("Offender's Incarcerated Number: " + offender.Number + htmlBreak);
                    body.Append("Offender's Facility: " + offender.FacilityName + htmlBreak);
                    body.Append("Relationship to Offender: " + 
                        (offender.ContactType == "13" ? "Family" : "Friend") + 
                        "(" + offender.RelationshipName + ")" + htmlBreak);

                    var offenderId = offender.Id;
                    var minorsForOffender = MinorVisitors.Where(x => x.OffenderId == offenderId);
                    foreach (var minor in minorsForOffender)
                    {
                        body.Append("<b>Minor Visitor Information</b>" + htmlBreak);
                        body.Append("Minor Visitor's Legal First Name: " + minor.FirstName + htmlBreak);
                        body.Append("Minor Visitor's Legal Middle Name: " + minor.MiddleName + htmlBreak);
                        body.Append("Minor Visitor's Legal Last Name: " + minor.LastName + htmlBreak);
                        body.Append("ID Number: " + minor.IdNumber + htmlBreak);
                        body.Append("SSN (last 4): " + minor.Ssn + htmlBreak);
                        body.Append("Race: " + minor.Race + htmlBreak);
                        body.Append("Gender: " + minor.Gender + htmlBreak);
                        body.Append("Date of Birth: " + minor.MonthDob + "/" + minor.DayDob + "/" + minor.YearDob +
                                    htmlBreak);
                        body.Append("Place of Birth: " + minor.PlaceOfBirth + htmlBreak);
                        body.Append("Minor's relationship to Offender: " + (minor.ContactType == "13" ? "Family" : "Friend") +
                        "(" + minor.RelationshipType + ")" + htmlBreak);
                        body.Append("<b>Conditions (Minor Visitor)</b>" + htmlBreak);
                        body.Append("Has the minor been convicted of a felony in any jurisdiction? " + minor.ConvictedFelon +
                                    htmlBreak);
                        body.Append("Is the minor currently under active Court supervision? " + minor.UnderActiveSupervision +
                                    htmlBreak);
                        body.Append(
                            "Is the minor a victim or family of a victim of the current crime committed by the offender with whom they wish to visit? " +
                            minor.VictimOfCurrentCrime + htmlBreak);
                        body.Append(
                            "Is the minor now or have they ever been a member or associated with any gang, motorcycle club, racial supremacy group, or other such group or organization as defined in Code of Virginia &sect;18.2-46.1? " +
                            minor.GangAssociation + htmlBreak);
                        body.Append(
                            "I am the parent or legal guardian of the minor identified above or I am presenting written notarized approval from the parent or legal guardian for the minor to visit with me: " +
                            (minor.ParentLegalGuardian ? "Yes" : "No") + htmlBreak + htmlBreak);
                    }
                }

                body.Append("<b>Conditions</b>" + htmlBreak);
                body.Append("Have you been convicted of a felony in any jurisdiction? " + applicantFelony
                             + htmlBreak);
                body.Append(
                    "Have you ever been employed by, volunteered with, or contracted by the Virginia Department of Corrections or Virginia Department of Correctional Education? " +
                    applicantVadocEmployee + htmlBreak);
                if (applicantVadocEmployee == "Yes")
                {
                    body.Append("Are you eligible for re-hiring? If a volunteer, are you allowed to return?" +
                            applicantRehire + htmlBreak);
                    body.Append("Did you meet any of the offenders while you were an employee, contractor, or volunteer at the VADOC?" +
                            meetOffender + htmlBreak);
                    body.Append("VADOC Employment Status:" +
                            employmentStatus + htmlBreak);
                    body.Append("VADOC Employment Start Date:" +
                            employmentStartDate + htmlBreak);
                    body.Append("VADOC Employment End Date:" +
                            employmentEndDate + htmlBreak);
                    body.Append("VADOC Employment Position/Title:" +
                            employmentTitle + htmlBreak);
                    body.Append("Name Used if Different from Current" +
                            employmentAlias + htmlBreak);
                }
                body.Append("Are you currently under active parole or probation supervision? " +
                            applicantParole + htmlBreak);
                body.Append("P&P District: " + applicantParoleDistrict + htmlBreak);
                body.Append("DOC ID Number: " + applicantVadocId + htmlBreak);
                body.Append(
                    "Are you a victim or family of a victim of the current crime committed by the offender with whom you wish to visit? " +
                    (applicantVictim ? "No" : "Yes") + htmlBreak);
                body.Append(
                    "Are you now or have you ever been a member or associated with any gang, motorcycle club, racial supremacy group, or other such group or organization as defined in Code of Virginia &sect;18.2-46.1? " +
                    applicantGang + htmlBreak + htmlBreak);

                body.Append(
                    "I authorize the Department of Corrections to conduct a criminal records check, or to use any Department of Corrections records to verify accuracy of information provided on this form: " +
                    (applicantAuthorization ? "Yes" : "No") + htmlBreak + htmlBreak);

                message.Body = body.ToString();

                SendEmail(message);

                message = new MailMessage
                {
                    From = new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"], "Visitation Applications (VADOC)")
                };
                message.To.Add(new MailAddress(applicantEmail));
                message.Subject = "Virginia Department of Corrections Visitation Application";
                message.IsBodyHtml = true;

                var now = DateTime.Now;

                body = new StringBuilder();
                body.Append(applicantFirstName + " " + applicantLastName + ":" + htmlBreak + htmlBreak);
                body.Append("Your Visitation Application was submitted at " + now.ToString("h:mm") + " " + (now.Hour > 11 ? "p.m." : "a.m.") + " on " + now.ToString("MMMM dd, yyyy") + "." + htmlBreak + htmlBreak);
                body.Append("In-state applicants should allow 30 days for applications to be processed. Out of state applicants should allow 90 days." + htmlBreak + htmlBreak);
                body.Append("Information regarding visitation can be found on our website at: http://vadoc.virginia.gov/offenders/visitation/default.shtm.");

                message.Body = body.ToString();

                SendEmail(message);
            }

            var generateXmlSuccessful = true;
            bool generateOffenderVisitationXmlFiles;
            if (bool.TryParse(ConfigurationManager.AppSettings["generateOffenderVisitationXmlFiles"],
                out generateOffenderVisitationXmlFiles) && generateOffenderVisitationXmlFiles)
            {
                generateXmlSuccessful = GenerateXmlFile();
            }

            if (generateXmlSuccessful)
            {
                SendVisitationEmails();
                HttpResponseMessage success = new HttpResponseMessage(HttpStatusCode.Accepted);
                return success;
                
            }
            else
            {
                string message = "Unable to process request.";
                HttpResponseMessage err = new HttpResponseMessage(HttpStatusCode.BadRequest);
                err.ReasonPhrase = message;
                return err;
            }

        }
        private List<Offender> Offenders
        {
            get
            {
                if (ViewState["Offenders"] != null)
                {
                    return (List<Offender>)ViewState["Offenders"];
                }

                return null;
            }
            set
            {
                ViewState["Offenders"] = value;
            }
        }
        private int OffenderIdIncrementer
        {
            get
            {
                if (ViewState["OffenderIdIncrementer"] != null)
                {
                    return (int)ViewState["OffenderIdIncrementer"];
                }

                return 0;
            }
            set
            {
                ViewState["OffenderIdIncrementer"] = value;
            }
        }
        private List<MinorVisitor> MinorVisitors
        {
            get
            {
                if (ViewState["MinorVisitors"] != null)
                {
                    return (List<MinorVisitor>)ViewState["MinorVisitors"];
                }

                return null;
            }
            set
            {
                ViewState["MinorVisitors"] = value;
            }
        }
        private int MinorIdIncrementer
        {
            get
            {
                if (ViewState["MinorIdIncrementer"] != null)
                {
                    return (int)ViewState["MinorIdIncrementer"];
                }

                return 0;
            }
            set
            {
                ViewState["MinorIdIncrementer"] = value;
            }
        }
        private bool? AddingMinor
        {
            get
            {
                if (ViewState["AddingMinor"] != null)
                {
                    return (bool)ViewState["AddingMinor"];
                }

                return null;
            }
            set
            {
                ViewState["AddingMinor"] = value;
            }
        }

        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsLocal && !Request.IsSecureConnection)
                Response.Redirect(Request.Url.ToString().Replace("http:", "https:"));

            ClientScript.RegisterStartupScript(GetType(), "hideProof", "hideProof();", true);

            if (!Page.IsPostBack)
            {
                PopulateDropdowns();
                Offenders = new List<Offender>();
                MinorVisitors = new List<MinorVisitor>();
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "scrollToAnchor", "scrollToAnchor();", true);
            }

            GenerateOffenderButtons();

            if (string.IsNullOrEmpty(hdnOffenderId.Value))
                GenerateMinorButtons(null);
            else
                GenerateMinorButtons(int.Parse(hdnOffenderId.Value));
        }

        [WebMethod]
        public static string SubmitReactVisitation(VisitationApplicationSubmission submission)
        {
            VisitationApplicationSubmission test;
            test = submission;
            Console.WriteLine("Hello World! Thank you, ");
            return "we made it";
        }

        /// <summary>
        /// btnAddMinor_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void btnAddMinor_Click(object sender, EventArgs e)
        {
            if (!ValidateOffenderForm())
                return;

            hdnMinorId.Value = string.Empty;
            txtMinorLegalFirstName.Text = string.Empty;
            txtMinorLegalMiddleName.Text = string.Empty;
            txtMinorLegalLastName.Text = string.Empty;
            txtMinorIdNumber.Text = string.Empty;
            ddlMinorIdType.SelectedIndex = 0;
            txtMinorSsn.Text = string.Empty;
            ddlMinorRace.SelectedIndex = 0;
            ddlMinorEthnicOrigin.SelectedIndex = 0;
            ddlMinorGender.SelectedIndex = 0;
            ddlMinorMonthDateOfBirth.SelectedIndex = 0;
            ddlMinorDayDateOfBirth.SelectedIndex = 0;
            ddlMinorYearDateOfBirth.SelectedIndex = 0;
            txtMinorPlaceOfBirth.Text = string.Empty;
            rblMinorContactType.Items[0].Selected = false;
            rblMinorContactType.Items[1].Selected = false;
            ddlMinorRelationshipQualifier.SelectedIndex = 0;
            ddlMinorRelationshipType.SelectedIndex = 0;
            rblMinorFelonyConviction.Items[0].Selected = false;
            rblMinorFelonyConviction.Items[1].Selected = false;
            rblMinorUnderActiveCourtSupervision.Items[0].Selected = false;
            rblMinorUnderActiveCourtSupervision.Items[1].Selected = false;
            rblMinorVictimOfCurrentCrime.Items[0].Selected = false;
            rblMinorVictimOfCurrentCrime.Items[1].Selected = false;
            rblMinorAssociatedWithGang.Items[0].Selected = false;
            rblMinorAssociatedWithGang.Items[1].Selected = false;
            chkMinorParentOrGuardian.Checked = false;

            AddingMinor = true;
            pnlMinors.Visible = true;

            SaveOffender();
        }

        /// <summary>
        /// Validate offender form
        /// </summary>
        /// <returns>Validation results</returns>
        private bool ValidateOffenderForm()
        {
            Page.Validate("Offender");
            var valid = Page.IsValid;

            if (rblContactType.SelectedItem != null && rblContactType.SelectedValue == "13" &&
                ddlRelationshipType.SelectedIndex == 0)
            {
                cvRelationshipType.IsValid = false;
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// btnSaveOffender_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void btnSaveOffender_Click(object sender, EventArgs e)
        {
            if (SaveOffender())
            {
                ResetOffenderFields();
                pnlMinors.Visible = false;
            }
        }

        private bool SaveOffender()
        {
            if (!ValidateOffenderForm())
                return false;

            if (string.IsNullOrEmpty(hdnOffenderId.Value))
            {
                OffenderIdIncrementer++;

                var newOffender = new Offender
                {
                    Id = OffenderIdIncrementer,
                    Name = txtOffenderName.Text,
                    Number = txtOffenderNumber.Text,
                    Facility = ddlOffenderFacility.SelectedValue,
                    ContactType = rblContactType.SelectedValue,
                    RelationshipQualifier = ddlRelationshipQualifier.SelectedValue,
                    RelationshipType = ddlRelationshipType.SelectedValue
                };

                Offenders.Add(newOffender);
                hdnOffenderId.Value = OffenderIdIncrementer.ToString(CultureInfo.InvariantCulture);

                var pendingMinors = MinorVisitors.Where(x => x.OffenderId == null);
                foreach (var minor in pendingMinors)
                {
                    minor.OffenderId = newOffender.Id;
                }
            }
            else
            {
                int offenderId;

                if (int.TryParse(hdnOffenderId.Value, out offenderId))
                {
                    var offender = Offenders.FirstOrDefault(x => x.Id == offenderId);

                    if (offender != null)
                    {
                        offender.Name = txtOffenderName.Text;
                        offender.Number = txtOffenderNumber.Text;
                        offender.Facility = ddlOffenderFacility.SelectedValue;
                        offender.ContactType = rblContactType.SelectedValue;
                        offender.RelationshipQualifier = ddlRelationshipQualifier.SelectedValue;
                        offender.RelationshipType = ddlRelationshipType.SelectedValue;
                    }
                }
            }

            GenerateOffenderButtons();

            pnlMinorList.Visible = false;

            return true;
        }

        /// <summary>
        /// Validate minor form
        /// </summary>
        /// <returns>Validation results</returns>
        private bool ValidateMinorForm()
        {
            Page.Validate("Minor");
            var valid = Page.IsValid;

            if (rblMinorContactType.SelectedItem != null && rblMinorContactType.SelectedValue == "13" &&
                ddlMinorRelationshipType.SelectedIndex == 0)
            {
                cvMinorRelationshipType.IsValid = false;
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// btnSaveMinor_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void btnSaveMinor_Click(object sender, EventArgs e)
        {
            if (!ValidateMinorForm())
                return;

            if (string.IsNullOrEmpty(hdnMinorId.Value))
            {
                MinorIdIncrementer++;

                var newMinor = new MinorVisitor
                {
                    Id = MinorIdIncrementer,
                    OffenderId = string.IsNullOrEmpty(hdnOffenderId.Value) ? null : (int?)int.Parse(hdnOffenderId.Value),
                    FirstName = txtMinorLegalFirstName.Text,
                    MiddleName = txtMinorLegalMiddleName.Text,
                    LastName = txtMinorLegalLastName.Text,
                    IdNumber = txtMinorIdNumber.Text,
                    IdType = ddlMinorIdType.SelectedValue,
                    Ssn = txtMinorSsn.Text,
                    Race = ddlMinorRace.SelectedValue,
                    EthnicOrigin = ddlMinorEthnicOrigin.SelectedValue,
                    Gender = ddlMinorGender.SelectedValue,
                    MonthDob = ddlMinorMonthDateOfBirth.SelectedValue,
                    DayDob = ddlMinorDayDateOfBirth.SelectedValue,
                    YearDob = ddlMinorYearDateOfBirth.SelectedValue,
                    PlaceOfBirth = txtMinorPlaceOfBirth.Text,
                    ContactType = rblMinorContactType.SelectedValue,
                    RelationshipQualifier = ddlMinorRelationshipQualifier.SelectedValue,
                    RelationshipType = ddlMinorRelationshipType.SelectedValue,
                    ConvictedFelon = rblMinorFelonyConviction.SelectedValue,
                    UnderActiveSupervision = rblMinorUnderActiveCourtSupervision.SelectedValue,
                    VictimOfCurrentCrime = rblMinorVictimOfCurrentCrime.SelectedValue,
                    GangAssociation = rblMinorAssociatedWithGang.SelectedValue,
                    ParentLegalGuardian = chkMinorParentOrGuardian.Checked
                };

                MinorVisitors.Add(newMinor);
            }
            else
            {
                int minorId;

                if (int.TryParse(hdnMinorId.Value, out minorId))
                {
                    var minor = MinorVisitors.FirstOrDefault(x => x.Id == minorId);

                    if (minor != null)
                    {
                        minor.FirstName = txtMinorLegalFirstName.Text;
                        minor.MiddleName = txtMinorLegalMiddleName.Text;
                        minor.LastName = txtMinorLegalLastName.Text;
                        minor.IdNumber = txtMinorIdNumber.Text;
                        minor.IdType = ddlMinorIdType.SelectedValue;
                        minor.Ssn = txtMinorSsn.Text;
                        minor.Race = ddlMinorRace.SelectedValue;
                        minor.EthnicOrigin = ddlMinorEthnicOrigin.SelectedValue;
                        minor.Gender = ddlMinorGender.SelectedValue;
                        minor.MonthDob = ddlMinorMonthDateOfBirth.SelectedValue;
                        minor.DayDob = ddlMinorDayDateOfBirth.SelectedValue;
                        minor.YearDob = ddlMinorYearDateOfBirth.SelectedValue;
                        minor.PlaceOfBirth = txtMinorPlaceOfBirth.Text;
                        minor.ContactType = rblMinorContactType.SelectedValue;
                        minor.RelationshipQualifier = ddlMinorRelationshipQualifier.SelectedValue;
                        minor.RelationshipType = ddlMinorRelationshipType.SelectedValue;
                        minor.ConvictedFelon = rblMinorFelonyConviction.SelectedValue;
                        minor.UnderActiveSupervision = rblMinorUnderActiveCourtSupervision.SelectedValue;
                        minor.VictimOfCurrentCrime = rblMinorVictimOfCurrentCrime.SelectedValue;
                        minor.GangAssociation = rblMinorAssociatedWithGang.SelectedValue;
                        minor.ParentLegalGuardian = chkMinorParentOrGuardian.Checked;
                    }
                }
            }

            if (string.IsNullOrEmpty(hdnOffenderId.Value))
                GenerateMinorButtons(null);
            else
                GenerateMinorButtons(int.Parse(hdnOffenderId.Value));

            AddingMinor = false;
            pnlMinorList.Visible = true;
            pnlMinors.Visible = false;
        }

        /// <summary>
        /// btnEditOffender_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void btnEditOffender_Click(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            var offenderId = int.Parse(btn.CommandArgument);
            var offender = Offenders.FirstOrDefault(x => x.Id == offenderId);

            if (offender != null)
            {
                MinorVisitors.RemoveAll(x => x.OffenderId == null);

                hdnOffenderId.Value = offenderId.ToString(CultureInfo.InvariantCulture);
                txtOffenderName.Text = offender.Name;
                txtOffenderNumber.Text = offender.Number;
                ddlOffenderFacility.SelectedValue = offender.Facility;
                rblContactType.SelectedValue = offender.ContactType;
                ddlRelationshipQualifier.SelectedValue = offender.RelationshipQualifier;
                ddlRelationshipType.SelectedValue = offender.RelationshipType;

                pnlMinorList.Controls.Clear();

                if (MinorVisitors.Count(x => x.OffenderId == offenderId) > 0)
                {
                    GenerateMinorButtons(offenderId);
                    pnlMinorList.Visible = true;
                }

                pnlMinors.Visible = false;
            }
        }

        /// <summary>
        /// btnEditMinor_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void btnEditMinor_Click(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            var minorId = int.Parse(btn.CommandArgument);
            var minor = MinorVisitors.FirstOrDefault(x => x.Id == minorId);

            if (minor != null)
            {
                hdnMinorId.Value = minorId.ToString(CultureInfo.InvariantCulture);
                txtMinorLegalFirstName.Text = minor.FirstName;
                txtMinorLegalMiddleName.Text = minor.MiddleName;
                txtMinorLegalLastName.Text = minor.LastName;
                txtMinorIdNumber.Text = minor.IdNumber;
                ddlMinorIdType.SelectedValue = minor.IdType;
                txtMinorSsn.Text = minor.Ssn;
                ddlMinorRace.SelectedValue = minor.Race;
                ddlMinorEthnicOrigin.SelectedValue = minor.EthnicOrigin;
                ddlMinorGender.SelectedValue = minor.Gender;
                ddlMinorMonthDateOfBirth.SelectedValue = minor.MonthDob;
                ddlMinorDayDateOfBirth.SelectedValue = minor.DayDob;
                ddlMinorYearDateOfBirth.SelectedValue = minor.YearDob;
                txtMinorPlaceOfBirth.Text = minor.PlaceOfBirth;
                rblMinorContactType.Items[0].Selected = (minor.ContactType == "13");
                rblMinorContactType.Items[1].Selected = (minor.ContactType == "6");
                ddlMinorRelationshipQualifier.SelectedValue = minor.RelationshipQualifier;
                ddlMinorRelationshipType.SelectedValue = minor.RelationshipType;
                rblMinorFelonyConviction.Items[0].Selected = (minor.ConvictedFelon.ToLower() == "yes");
                rblMinorFelonyConviction.Items[1].Selected = (minor.ConvictedFelon.ToLower() == "no");
                rblMinorUnderActiveCourtSupervision.Items[0].Selected = (minor.UnderActiveSupervision.ToLower() == "yes");
                rblMinorUnderActiveCourtSupervision.Items[1].Selected = (minor.UnderActiveSupervision.ToLower() == "no");
                rblMinorVictimOfCurrentCrime.Items[0].Selected = (minor.VictimOfCurrentCrime.ToLower() == "yes");
                rblMinorVictimOfCurrentCrime.Items[1].Selected = (minor.VictimOfCurrentCrime.ToLower() == "no");
                rblMinorAssociatedWithGang.Items[0].Selected = (minor.GangAssociation.ToLower() == "yes");
                rblMinorAssociatedWithGang.Items[1].Selected = (minor.GangAssociation.ToLower() == "no");
                chkMinorParentOrGuardian.Checked = minor.ParentLegalGuardian;

                pnlMinors.Visible = true;
            }
        }

        /// <summary>
        /// btnDeleteOffender_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void btnDeleteOffender_Click(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            int offenderId;

            if (int.TryParse(btn.CommandArgument, out offenderId))
            {
                MinorVisitors.RemoveAll(x => x.OffenderId == offenderId);
                Offenders.RemoveAll(x => x.Id == offenderId);

                GenerateOffenderButtons();

                ResetOffenderFields();
            }

            pnlMinorList.Visible = false;
            pnlMinors.Visible = false;
        }

        /// <summary>
        /// btnDeleteMinor_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void btnDeleteMinor_Click(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            int minorId;

            if (int.TryParse(btn.CommandArgument, out minorId))
            {
                var minor = MinorVisitors.FirstOrDefault(x => x.Id == minorId);

                MinorVisitors.RemoveAll(x => x.Id == minorId);

                GenerateMinorButtons(minor.OffenderId);
            }

            pnlMinors.Visible = false;
        }

        /// <summary>
        /// Validate main form
        /// </summary>
        /// <returns>Validation results</returns>
        private bool ValidateMainForm()
        {
            Page.Validate("Full");
            var valid = Page.IsValid;

            if (ddlMonthDateOfBirth.SelectedIndex > 0 && ddlDayDateOfBirth.SelectedIndex > 0 &&
                ddlYearDateOfBirth.SelectedIndex > 0)
            {
                DateTime dob;
                if (DateTime.TryParseExact(
                    ddlMonthDateOfBirth.SelectedValue + "/" + ddlDayDateOfBirth.SelectedValue + "/" +
                    ddlYearDateOfBirth.SelectedValue, "d", null, DateTimeStyles.None, out dob))
                {
                    var today = DateTime.Now;

                    if (chkEmancipatedMinor.Checked)
                    {
                        if (today.Month - dob.Month + (12 * (today.Year - dob.Year)) >= 216)
                        {
                            cvDateOfBirth.ErrorMessage =
                                "Emancipated minor visitor applicants must be under 18 years old to apply";
                            cvDateOfBirth.IsValid = false;
                            valid = false;
                        }
                    }
                    else
                    {
                        if (today.Month - dob.Month + (12 * (today.Year - dob.Year)) < 215)
                        {
                            cvDateOfBirth.ErrorMessage =
                                "Adult visitor applicants must be at least 18 years old to apply";
                            cvDateOfBirth.IsValid = false;
                            valid = false;
                        }
                    }
                }
                else
                {
                    cvDateOfBirth.ErrorMessage = "Invalid Date of Birth";
                    cvDateOfBirth.IsValid = false;
                    valid = false;
                }

            }
            if (ddlState.SelectedValue == "1047" && ddlCity.SelectedIndex == 0)
            {
                cvDdlCity.IsValid = false;
                valid = false;
            }
            if (ddlState.SelectedValue != "1047" && string.IsNullOrEmpty(txtCityTownOfResidence.Text.Trim()))
            {
                cvTxtCity.IsValid = false;
                valid = false;
            }
            if (ddlCountry.SelectedValue == "2397")
            {
                if (ddlState.SelectedIndex == 0)
                {
                    cvState.IsValid = false;
                    valid = false;
                }
                if (string.IsNullOrEmpty(txtZip.Text))
                {
                    cvZip.IsValid = false;
                    valid = false;
                }
            }
            if (txtEmailAddress.Text.Length > 0 &&
                !Regex.IsMatch(txtEmailAddress.Text,
                    @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?"))
            {
                revEmailAddress.IsValid = false;
                valid = false;
            }
            if (Offenders.Count == 0)
            {
                if (ValidateOffenderForm())
                {
                    var newOffender = new Offender
                    {
                        Name = txtOffenderName.Text,
                        Number = txtOffenderNumber.Text,
                        Facility = ddlOffenderFacility.SelectedValue,
                        ContactType = rblContactType.SelectedValue,
                        RelationshipQualifier = ddlRelationshipQualifier.SelectedValue,
                        RelationshipType = ddlRelationshipType.SelectedValue
                    };

                    Offenders.Add(newOffender);
                }
                else
                {
                    cvOffenders.IsValid = false;
                    valid = false;
                }
            }
            if (rblHaveVehicle.SelectedValue.ToLower() == "yes")
            {
                if (string.IsNullOrEmpty(txtVehicleMake.Text))
                {
                    cvVehicleMake.IsValid = false;
                    valid = false;
                }
                if (string.IsNullOrEmpty(txtVehicleModel.Text))
                {
                    cvVehicleModel.IsValid = false;
                    valid = false;
                }
                if (ddlVehicleYear.SelectedIndex == 0)
                {
                    cvVehicleYear.IsValid = false;
                    valid = false;
                }
                if (string.IsNullOrEmpty(txtVehiclePlateNumber.Text))
                {
                    cvVehiclePlateNumber.IsValid = false;
                    valid = false;
                }
            }
            if (AddingMinor.HasValue && AddingMinor.Value && Offenders.Count == 1)
            {
                if (ValidateMinorForm())
                {
                    var newMinor = new MinorVisitor
                    {
                        OffenderId = Offenders.First().Id,
                        FirstName = txtMinorLegalFirstName.Text,
                        MiddleName = txtMinorLegalMiddleName.Text,
                        LastName = txtMinorLegalLastName.Text,
                        IdNumber = txtMinorIdNumber.Text,
                        IdType = ddlMinorIdType.SelectedValue,
                        Ssn = txtMinorSsn.Text,
                        Race = ddlMinorRace.SelectedValue,
                        Gender = ddlMinorGender.SelectedValue,
                        MonthDob = ddlMinorMonthDateOfBirth.SelectedValue,
                        DayDob = ddlMinorDayDateOfBirth.SelectedValue,
                        YearDob = ddlMinorYearDateOfBirth.SelectedValue,
                        PlaceOfBirth = txtMinorPlaceOfBirth.Text,
                        ContactType = rblMinorContactType.SelectedValue,
                        RelationshipQualifier = ddlMinorRelationshipQualifier.SelectedValue,
                        RelationshipType = ddlMinorRelationshipType.SelectedValue,
                        ConvictedFelon = rblMinorFelonyConviction.SelectedValue,
                        UnderActiveSupervision = rblMinorUnderActiveCourtSupervision.SelectedValue,
                        VictimOfCurrentCrime = rblMinorVictimOfCurrentCrime.SelectedValue,
                        GangAssociation = rblMinorAssociatedWithGang.SelectedValue,
                        ParentLegalGuardian = chkMinorParentOrGuardian.Checked
                    };

                    MinorVisitors.Add(newMinor);
                }
                else
                {
                    valid = false;
                }
            }
            if (!chkAuthorizeRecordCheck.Checked)
            {
                cvAuthorizeRecordCheck.IsValid = false;
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// btnSubmit_Click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtVisitorProof.Text.Trim().ToLower() != "visitor")
            {
                divFormError.Visible = true;
                form1.Visible = false;
                return;
            }

            if (!ValidateMainForm())
            {
                ClientScript.RegisterStartupScript(GetType(), "validateFormServer", "validateFormServer();", true);
                return;
            }

            var generateXmlSuccessful = true;
            bool generateOffenderVisitationXmlFiles;
            if (bool.TryParse(ConfigurationManager.AppSettings["generateOffenderVisitationXmlFiles"],
                out generateOffenderVisitationXmlFiles) && generateOffenderVisitationXmlFiles)
            {
                generateXmlSuccessful = GenerateXmlFile();
            }

            if (generateXmlSuccessful)
            {
                SendVisitationEmails();
                Response.Redirect("ApplicationSubmitted.aspx");
            }
            else
            {
                Response.Redirect("ApplicationNotSubmitted.aspx");
            }
        }

        private void PopulateDropdowns()
        {
            #region ID Types
            ddlIdType.Items.Add(new ListItem(string.Empty));
            ddlIdType.Items.Add(new ListItem("Driver's License #", "1"));
            ddlIdType.Items.Add(new ListItem("Military ID", "12"));
            ddlIdType.Items.Add(new ListItem("Passport", "11"));
            ddlIdType.Items.Add(new ListItem("State Issued Photo ID", "13"));
            #endregion

            #region Races
            ddlRace.Items.Add(new ListItem(string.Empty));
            ddlRace.Items.Add(new ListItem("American Indian or Alaskan Native", "15"));
            ddlRace.Items.Add(new ListItem("Asian or Pacific Islander", "16"));
            ddlRace.Items.Add(new ListItem("Black", "17"));
            ddlRace.Items.Add(new ListItem("Unknown", "19"));
            ddlRace.Items.Add(new ListItem("White", "18"));
            #endregion

            #region Ethnic Origins
            ddlEthnicOrigin.Items.Add(new ListItem(string.Empty));
            ddlEthnicOrigin.Items.Add(new ListItem("Hispanic", "1"));
            ddlEthnicOrigin.Items.Add(new ListItem("Other", "0"));
            #endregion

            #region Genders
            ddlGender.Items.Add(new ListItem(string.Empty));
            ddlGender.Items.Add(new ListItem("Female", "13"));
            ddlGender.Items.Add(new ListItem("Male", "12"));
            #endregion

            #region Hair Colors
            ddlHairColor.Items.Add(new ListItem(string.Empty));
            ddlHairColor.Items.Add(new ListItem("Bald", "1"));
            ddlHairColor.Items.Add(new ListItem("Black", "2"));
            ddlHairColor.Items.Add(new ListItem("Blond or Strawberry", "3"));
            ddlHairColor.Items.Add(new ListItem("Blue", "4"));
            ddlHairColor.Items.Add(new ListItem("Brown", "5"));
            ddlHairColor.Items.Add(new ListItem("Gray or Partially Gray", "6"));
            ddlHairColor.Items.Add(new ListItem("Green", "7"));
            ddlHairColor.Items.Add(new ListItem("Orange", "8"));
            ddlHairColor.Items.Add(new ListItem("Pink", "9"));
            ddlHairColor.Items.Add(new ListItem("Purple", "10"));
            ddlHairColor.Items.Add(new ListItem("Red or Auburn", "11"));
            ddlHairColor.Items.Add(new ListItem("Sandy", "12"));
            ddlHairColor.Items.Add(new ListItem("White", "13"));
            #endregion

            #region Eye Colors
            ddlEyeColor.Items.Add(new ListItem(string.Empty));
            ddlEyeColor.Items.Add(new ListItem("Black", "1"));
            ddlEyeColor.Items.Add(new ListItem("Blue", "6"));
            ddlEyeColor.Items.Add(new ListItem("Brown", "2"));
            ddlEyeColor.Items.Add(new ListItem("Gray", "7"));
            ddlEyeColor.Items.Add(new ListItem("Green", "3"));
            ddlEyeColor.Items.Add(new ListItem("Hazel", "8"));
            ddlEyeColor.Items.Add(new ListItem("Maroon", "4"));
            ddlEyeColor.Items.Add(new ListItem("Multicolored", "9"));
            ddlEyeColor.Items.Add(new ListItem("Pink", "5"));
            #endregion

            #region Birth Months
            ddlMonthDateOfBirth.Items.Add(new ListItem(string.Empty));
            ddlMonthDateOfBirth.Items.Add(new ListItem("January", "01"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("February", "02"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("March", "03"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("April", "04"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("May", "05"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("June", "06"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("July", "07"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("August", "08"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("September", "09"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("October", "10"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("November", "11"));
            ddlMonthDateOfBirth.Items.Add(new ListItem("December", "12"));
            #endregion

            #region Birth Days
            ddlDayDateOfBirth.Items.Add(new ListItem(string.Empty));
            for (int i = 1; i <= 31; i++)
                ddlDayDateOfBirth.Items.Add(new ListItem(i.ToString(CultureInfo.CurrentCulture).PadLeft(2, '0')));
            #endregion

            #region Birth Years
            ddlYearDateOfBirth.Items.Add(new ListItem(string.Empty));
            for (int j = DateTime.Now.Year; j >= DateTime.Now.Year - 115; j--)
                ddlYearDateOfBirth.Items.Add(new ListItem(j.ToString(CultureInfo.CurrentCulture)));
            #endregion

            #region Cities
            ddlCity.Items.Add(new ListItem(string.Empty));
            ddlCity.Items.Add(new ListItem("Abingdon", "4112"));
            ddlCity.Items.Add(new ListItem("Accomac", "4113"));
            ddlCity.Items.Add(new ListItem("Achilles", "4114"));
            ddlCity.Items.Add(new ListItem("Afton", "4115"));
            ddlCity.Items.Add(new ListItem("Alberta", "4116"));
            ddlCity.Items.Add(new ListItem("Aldie", "4117"));
            ddlCity.Items.Add(new ListItem("Alexandria", "4118"));
            ddlCity.Items.Add(new ListItem("Allisonia", "6032"));
            ddlCity.Items.Add(new ListItem("Altavista", "4119"));
            ddlCity.Items.Add(new ListItem("Alton", "4120"));
            ddlCity.Items.Add(new ListItem("Amelia Court House", "4121"));
            ddlCity.Items.Add(new ListItem("Amherst", "4122"));
            ddlCity.Items.Add(new ListItem("Amissville", "4123"));
            ddlCity.Items.Add(new ListItem("Ammon", "4124"));
            ddlCity.Items.Add(new ListItem("Amonate", "4125"));
            ddlCity.Items.Add(new ListItem("Andover", "4126"));
            ddlCity.Items.Add(new ListItem("Annandale", "4127"));
            ddlCity.Items.Add(new ListItem("Appalachia", "4128"));
            ddlCity.Items.Add(new ListItem("Appomattox", "4129"));
            ddlCity.Items.Add(new ListItem("Ararat", "4130"));
            ddlCity.Items.Add(new ListItem("Arcola", "4131"));
            ddlCity.Items.Add(new ListItem("Ark", "4132"));
            ddlCity.Items.Add(new ListItem("Arlington", "4133"));
            ddlCity.Items.Add(new ListItem("Aroda", "4134"));
            ddlCity.Items.Add(new ListItem("Arrington", "4135"));
            ddlCity.Items.Add(new ListItem("Arvonia", "4136"));
            ddlCity.Items.Add(new ListItem("Ashburn", "4137"));
            ddlCity.Items.Add(new ListItem("Ashland", "4138"));
            ddlCity.Items.Add(new ListItem("Assawoman", "4139"));
            ddlCity.Items.Add(new ListItem("Atkins", "4140"));
            ddlCity.Items.Add(new ListItem("Atlantic", "4141"));
            ddlCity.Items.Add(new ListItem("Augusta Springs", "4142"));
            ddlCity.Items.Add(new ListItem("Austinville", "4143"));
            ddlCity.Items.Add(new ListItem("Axton", "4144"));
            ddlCity.Items.Add(new ListItem("Aylett", "4145"));
            ddlCity.Items.Add(new ListItem("Bacova", "4146"));
            ddlCity.Items.Add(new ListItem("Bailey's Crossroads", "5940"));
            ddlCity.Items.Add(new ListItem("Banco", "4147"));
            ddlCity.Items.Add(new ListItem("Bandy", "4148"));
            ddlCity.Items.Add(new ListItem("Barboursville", "4149"));
            ddlCity.Items.Add(new ListItem("Barhamsville", "4150"));
            ddlCity.Items.Add(new ListItem("Barren Springs", "4151"));
            ddlCity.Items.Add(new ListItem("Baskerville", "4152"));
            ddlCity.Items.Add(new ListItem("Bassett", "4153"));
            ddlCity.Items.Add(new ListItem("Bastian", "4154"));
            ddlCity.Items.Add(new ListItem("Basye", "4155"));
            ddlCity.Items.Add(new ListItem("Batesville", "4156"));
            ddlCity.Items.Add(new ListItem("Battery Park", "4157"));
            ddlCity.Items.Add(new ListItem("Bavon", "5941"));
            ddlCity.Items.Add(new ListItem("Bealeton", "4158"));
            ddlCity.Items.Add(new ListItem("Beaumont", "4159"));
            ddlCity.Items.Add(new ListItem("Beaverdam", "4160"));
            ddlCity.Items.Add(new ListItem("Bedford", "4161"));
            ddlCity.Items.Add(new ListItem("Bee", "4162"));
            ddlCity.Items.Add(new ListItem("Bellamy", "5942"));
            ddlCity.Items.Add(new ListItem("Belle Haven", "4163"));
            ddlCity.Items.Add(new ListItem("Belspring", "4164"));
            ddlCity.Items.Add(new ListItem("Ben Hur", "4165"));
            ddlCity.Items.Add(new ListItem("Bena", "4166"));
            ddlCity.Items.Add(new ListItem("Bent Mountain", "4167"));
            ddlCity.Items.Add(new ListItem("Bentonville", "4168"));
            ddlCity.Items.Add(new ListItem("Bergton", "4169"));
            ddlCity.Items.Add(new ListItem("Berryville", "4170"));
            ddlCity.Items.Add(new ListItem("Big Island", "4171"));
            ddlCity.Items.Add(new ListItem("Big Rock", "4172"));
            ddlCity.Items.Add(new ListItem("Big Stone Gap", "4173"));
            ddlCity.Items.Add(new ListItem("Birchleaf", "4174"));
            ddlCity.Items.Add(new ListItem("Birdsnest", "4175"));
            ddlCity.Items.Add(new ListItem("Bishop", "4176"));
            ddlCity.Items.Add(new ListItem("Blacksburg", "4177"));
            ddlCity.Items.Add(new ListItem("Blackstone", "4178"));
            ddlCity.Items.Add(new ListItem("Blackwater", "4179"));
            ddlCity.Items.Add(new ListItem("Blairs", "4180"));
            ddlCity.Items.Add(new ListItem("Blakes", "5943"));
            ddlCity.Items.Add(new ListItem("Bland", "4181"));
            ddlCity.Items.Add(new ListItem("Bloxom", "4182"));
            ddlCity.Items.Add(new ListItem("Blue Grass", "4183"));
            ddlCity.Items.Add(new ListItem("Blue Ridge", "4184"));
            ddlCity.Items.Add(new ListItem("Bluefield", "4185"));
            ddlCity.Items.Add(new ListItem("Bluemont", "4186"));
            ddlCity.Items.Add(new ListItem("Bohannon", "4187"));
            ddlCity.Items.Add(new ListItem("Boissevain", "4188"));
            ddlCity.Items.Add(new ListItem("Bon Air", "5944"));
            ddlCity.Items.Add(new ListItem("Boones Mill", "4189"));
            ddlCity.Items.Add(new ListItem("Boston", "4190"));
            ddlCity.Items.Add(new ListItem("Bowling Green", "4191"));
            ddlCity.Items.Add(new ListItem("Boyce", "4192"));
            ddlCity.Items.Add(new ListItem("Boydton", "4193"));
            ddlCity.Items.Add(new ListItem("Boykins", "4194"));
            ddlCity.Items.Add(new ListItem("Bracey", "4195"));
            ddlCity.Items.Add(new ListItem("Bradford", "5945"));
            ddlCity.Items.Add(new ListItem("Brambleton", "5946"));
            ddlCity.Items.Add(new ListItem("Branchville", "4196"));
            ddlCity.Items.Add(new ListItem("Brandy Station", "4197"));
            ddlCity.Items.Add(new ListItem("Breaks", "4198"));
            ddlCity.Items.Add(new ListItem("Bremo Bluff", "4199"));
            ddlCity.Items.Add(new ListItem("Bridgewater", "4200"));
            ddlCity.Items.Add(new ListItem("Brightwood", "4201"));
            ddlCity.Items.Add(new ListItem("Bristol", "4202"));
            ddlCity.Items.Add(new ListItem("Bristow", "4203"));
            ddlCity.Items.Add(new ListItem("Broad Run", "4204"));
            ddlCity.Items.Add(new ListItem("Broadford", "4205"));
            ddlCity.Items.Add(new ListItem("Broadway", "4206"));
            ddlCity.Items.Add(new ListItem("Brodnax", "4207"));
            ddlCity.Items.Add(new ListItem("Brooke", "4208"));
            ddlCity.Items.Add(new ListItem("Brookneal", "4209"));
            ddlCity.Items.Add(new ListItem("Brownsburg", "4210"));
            ddlCity.Items.Add(new ListItem("Brucetown", "4211"));
            ddlCity.Items.Add(new ListItem("Bruington", "4212"));
            ddlCity.Items.Add(new ListItem("Brwontown", "5947"));
            ddlCity.Items.Add(new ListItem("Buchanan", "4213"));
            ddlCity.Items.Add(new ListItem("Buckingham", "4214"));
            ddlCity.Items.Add(new ListItem("Buena Vista", "4215"));
            ddlCity.Items.Add(new ListItem("Buffalo Junction", "4216"));
            ddlCity.Items.Add(new ListItem("Bumpass", "4217"));
            ddlCity.Items.Add(new ListItem("Burgess", "4218"));
            ddlCity.Items.Add(new ListItem("Burke", "4219"));
            ddlCity.Items.Add(new ListItem("Burkes Garden", "4220"));
            ddlCity.Items.Add(new ListItem("Burkeville", "4221"));
            ddlCity.Items.Add(new ListItem("Burnsville", "5948"));
            ddlCity.Items.Add(new ListItem("Burr Hill", "4222"));
            ddlCity.Items.Add(new ListItem("Callands", "4223"));
            ddlCity.Items.Add(new ListItem("Callao", "4224"));
            ddlCity.Items.Add(new ListItem("Callaway", "4225"));
            ddlCity.Items.Add(new ListItem("Calverton", "4226"));
            ddlCity.Items.Add(new ListItem("Cana", "4227"));
            ddlCity.Items.Add(new ListItem("Cape Charles", "4228"));
            ddlCity.Items.Add(new ListItem("Capeville", "4229"));
            ddlCity.Items.Add(new ListItem("Capron", "4230"));
            ddlCity.Items.Add(new ListItem("Cardinal", "4231"));
            ddlCity.Items.Add(new ListItem("Caret", "4232"));
            ddlCity.Items.Add(new ListItem("Carrollton", "4233"));
            ddlCity.Items.Add(new ListItem("Carrsville", "4234"));
            ddlCity.Items.Add(new ListItem("Carson", "4235"));
            ddlCity.Items.Add(new ListItem("Cartersville", "4236"));
            ddlCity.Items.Add(new ListItem("Casanova", "4237"));
            ddlCity.Items.Add(new ListItem("Cascade", "4238"));
            ddlCity.Items.Add(new ListItem("Castleton", "4239"));
            ddlCity.Items.Add(new ListItem("Castlewood", "4240"));
            ddlCity.Items.Add(new ListItem("Catawba", "4241"));
            ddlCity.Items.Add(new ListItem("Catharpin", "4242"));
            ddlCity.Items.Add(new ListItem("Catlett", "4243"));
            ddlCity.Items.Add(new ListItem("Cauthornville", "5949"));
            ddlCity.Items.Add(new ListItem("Cedar Bluff", "4244"));
            ddlCity.Items.Add(new ListItem("Center Cross", "4245"));
            ddlCity.Items.Add(new ListItem("Centreville", "4246"));
            ddlCity.Items.Add(new ListItem("Ceres", "4247"));
            ddlCity.Items.Add(new ListItem("Champlain", "4248"));
            ddlCity.Items.Add(new ListItem("Chance", "5950"));
            ddlCity.Items.Add(new ListItem("Chantilly", "4249"));
            ddlCity.Items.Add(new ListItem("Charles City", "4250"));
            ddlCity.Items.Add(new ListItem("Charlotte Court House", "4251"));
            ddlCity.Items.Add(new ListItem("Charlottesville", "4252"));
            ddlCity.Items.Add(new ListItem("Chase City", "4253"));
            ddlCity.Items.Add(new ListItem("Chatham", "4254"));
            ddlCity.Items.Add(new ListItem("Check", "4255"));
            ddlCity.Items.Add(new ListItem("Cheriton", "4256"));
            ddlCity.Items.Add(new ListItem("Chesapeake", "4257"));
            ddlCity.Items.Add(new ListItem("Chester", "4258"));
            ddlCity.Items.Add(new ListItem("Chester Gap", "4259"));
            ddlCity.Items.Add(new ListItem("Chesterfield", "4260"));
            ddlCity.Items.Add(new ListItem("Chilhowie", "4261"));
            ddlCity.Items.Add(new ListItem("Chincoteague Island", "4262"));
            ddlCity.Items.Add(new ListItem("Christchurch", "4263"));
            ddlCity.Items.Add(new ListItem("Christiansburg", "4264"));
            ddlCity.Items.Add(new ListItem("Church Road", "4265"));
            ddlCity.Items.Add(new ListItem("Church View", "4266"));
            ddlCity.Items.Add(new ListItem("Churchville", "4267"));
            ddlCity.Items.Add(new ListItem("Claremont", "4268"));
            ddlCity.Items.Add(new ListItem("Clarksville", "4269"));
            ddlCity.Items.Add(new ListItem("Claudville", "4270"));
            ddlCity.Items.Add(new ListItem("Clear Brook", "4271"));
            ddlCity.Items.Add(new ListItem("Cleveland", "4272"));
            ddlCity.Items.Add(new ListItem("Clifford", "4273"));
            ddlCity.Items.Add(new ListItem("Clifton", "4274"));
            ddlCity.Items.Add(new ListItem("Clifton Forge", "4275"));
            ddlCity.Items.Add(new ListItem("Clinchco", "4276"));
            ddlCity.Items.Add(new ListItem("Clinchport", "5952"));
            ddlCity.Items.Add(new ListItem("Clintwood", "4277"));
            ddlCity.Items.Add(new ListItem("Clover", "4278"));
            ddlCity.Items.Add(new ListItem("Cloverdale", "4279"));
            ddlCity.Items.Add(new ListItem("Cluster Springs", "4280"));
            ddlCity.Items.Add(new ListItem("Cobbs Creek", "4281"));
            ddlCity.Items.Add(new ListItem("Coeburn", "4282"));
            ddlCity.Items.Add(new ListItem("Coleman Falls", "4283"));
            ddlCity.Items.Add(new ListItem("Coles Point", "4284"));
            ddlCity.Items.Add(new ListItem("Collinsville", "4285"));
            ddlCity.Items.Add(new ListItem("Cologne", "5953"));
            ddlCity.Items.Add(new ListItem("Colonial Beach", "4286"));
            ddlCity.Items.Add(new ListItem("Colonial Heights", "4287"));
            ddlCity.Items.Add(new ListItem("Columbia", "4288"));
            ddlCity.Items.Add(new ListItem("Concord", "4289"));
            ddlCity.Items.Add(new ListItem("Copper Hill", "4290"));
            ddlCity.Items.Add(new ListItem("Corbin", "4291"));
            ddlCity.Items.Add(new ListItem("Council", "5954"));
            ddlCity.Items.Add(new ListItem("Courtland", "4292"));
            ddlCity.Items.Add(new ListItem("Covesville", "4293"));
            ddlCity.Items.Add(new ListItem("Covington", "4294"));
            ddlCity.Items.Add(new ListItem("Craddockville", "4295"));
            ddlCity.Items.Add(new ListItem("Craigsville", "4296"));
            ddlCity.Items.Add(new ListItem("Crewe", "4297"));
            ddlCity.Items.Add(new ListItem("Criders", "4298"));
            ddlCity.Items.Add(new ListItem("Crimora", "4299"));
            ddlCity.Items.Add(new ListItem("Cripple Creek", "4300"));
            ddlCity.Items.Add(new ListItem("Critz", "4301"));
            ddlCity.Items.Add(new ListItem("Crockett", "4302"));
            ddlCity.Items.Add(new ListItem("Cross Junction", "4303"));
            ddlCity.Items.Add(new ListItem("Crozet", "4304"));
            ddlCity.Items.Add(new ListItem("Crozier", "4305"));
            ddlCity.Items.Add(new ListItem("Crystal Hill", "4306"));
            ddlCity.Items.Add(new ListItem("Cullen", "4307"));
            ddlCity.Items.Add(new ListItem("Culpeper", "4308"));
            ddlCity.Items.Add(new ListItem("Cumberland", "4309"));
            ddlCity.Items.Add(new ListItem("Dahlgren", "4310"));
            ddlCity.Items.Add(new ListItem("Dale City", "5955"));
            ddlCity.Items.Add(new ListItem("Daleville", "4311"));
            ddlCity.Items.Add(new ListItem("Damascus", "4312"));
            ddlCity.Items.Add(new ListItem("Dante", "4313"));
            ddlCity.Items.Add(new ListItem("Danville", "4314"));
            ddlCity.Items.Add(new ListItem("Davenport", "4315"));
            ddlCity.Items.Add(new ListItem("Davis Wharf", "4316"));
            ddlCity.Items.Add(new ListItem("Dayton", "4317"));
            ddlCity.Items.Add(new ListItem("Deerfield", "4318"));
            ddlCity.Items.Add(new ListItem("Delaplane", "4319"));
            ddlCity.Items.Add(new ListItem("Deltaville", "4320"));
            ddlCity.Items.Add(new ListItem("Dendron", "4321"));
            ddlCity.Items.Add(new ListItem("Dewitt", "4322"));
            ddlCity.Items.Add(new ListItem("Diggs", "4323"));
            ddlCity.Items.Add(new ListItem("Dillwyn", "4324"));
            ddlCity.Items.Add(new ListItem("Dinwiddie", "4325"));
            ddlCity.Items.Add(new ListItem("Disputanta", "4326"));
            ddlCity.Items.Add(new ListItem("Doe Hill", "4327"));
            ddlCity.Items.Add(new ListItem("Dogue", "4328"));
            ddlCity.Items.Add(new ListItem("Dolphin", "4329"));
            ddlCity.Items.Add(new ListItem("Doran", "4330"));
            ddlCity.Items.Add(new ListItem("Doswell", "4331"));
            ddlCity.Items.Add(new ListItem("Drakes Branch", "4332"));
            ddlCity.Items.Add(new ListItem("Draper", "4333"));
            ddlCity.Items.Add(new ListItem("Drewryville", "4334"));
            ddlCity.Items.Add(new ListItem("Dry Fork", "4335"));
            ddlCity.Items.Add(new ListItem("Dryden", "4336"));
            ddlCity.Items.Add(new ListItem("Dublin", "4337"));
            ddlCity.Items.Add(new ListItem("Duffield", "4338"));
            ddlCity.Items.Add(new ListItem("Dugspur", "4339"));
            ddlCity.Items.Add(new ListItem("Dulles", "4340"));
            ddlCity.Items.Add(new ListItem("Dumfries", "4341"));
            ddlCity.Items.Add(new ListItem("Dundas", "4342"));
            ddlCity.Items.Add(new ListItem("Dungannon", "4343"));
            ddlCity.Items.Add(new ListItem("Dunn Loring", "4344"));
            ddlCity.Items.Add(new ListItem("Dunnsville", "4345"));
            ddlCity.Items.Add(new ListItem("Dutton", "4346"));
            ddlCity.Items.Add(new ListItem("Dyke", "4347"));
            ddlCity.Items.Add(new ListItem("Eagle Rock", "4348"));
            ddlCity.Items.Add(new ListItem("Earlysville", "4349"));
            ddlCity.Items.Add(new ListItem("East Stone Gap", "4350"));
            ddlCity.Items.Add(new ListItem("Eastville", "4351"));
            ddlCity.Items.Add(new ListItem("Ebony", "4352"));
            ddlCity.Items.Add(new ListItem("Edinburg", "4353"));
            ddlCity.Items.Add(new ListItem("Edwardsville", "4354"));
            ddlCity.Items.Add(new ListItem("Eggleston", "4355"));
            ddlCity.Items.Add(new ListItem("Elberon", "4356"));
            ddlCity.Items.Add(new ListItem("Elk Creek", "4357"));
            ddlCity.Items.Add(new ListItem("Elkton", "4358"));
            ddlCity.Items.Add(new ListItem("Elkwood", "4359"));
            ddlCity.Items.Add(new ListItem("Elliston", "4360"));
            ddlCity.Items.Add(new ListItem("Emory", "4361"));
            ddlCity.Items.Add(new ListItem("Emporia", "4362"));
            ddlCity.Items.Add(new ListItem("Esmont", "4363"));
            ddlCity.Items.Add(new ListItem("Etlan", "4364"));
            ddlCity.Items.Add(new ListItem("Ettrick", "5956"));
            ddlCity.Items.Add(new ListItem("Evergreen", "4365"));
            ddlCity.Items.Add(new ListItem("Evington", "4366"));
            ddlCity.Items.Add(new ListItem("Ewing", "4367"));
            ddlCity.Items.Add(new ListItem("Exeter", "5957"));
            ddlCity.Items.Add(new ListItem("Exmore", "4368"));
            ddlCity.Items.Add(new ListItem("Faber", "4369"));
            ddlCity.Items.Add(new ListItem("Fairfax", "4370"));
            ddlCity.Items.Add(new ListItem("Fairfax Station", "4371"));
            ddlCity.Items.Add(new ListItem("Fairfield", "4372"));
            ddlCity.Items.Add(new ListItem("Fairlawn", "5958"));
            ddlCity.Items.Add(new ListItem("Falls Church", "4373"));
            ddlCity.Items.Add(new ListItem("Falls Mills", "4374"));
            ddlCity.Items.Add(new ListItem("Falmouth", "5959"));
            ddlCity.Items.Add(new ListItem("Fancy Gap", "4375"));
            ddlCity.Items.Add(new ListItem("Farmville", "4376"));
            ddlCity.Items.Add(new ListItem("Farnham", "4377"));
            ddlCity.Items.Add(new ListItem("Ferrum", "4378"));
            ddlCity.Items.Add(new ListItem("Fieldale", "4379"));
            ddlCity.Items.Add(new ListItem("Fincastle", "4380"));
            ddlCity.Items.Add(new ListItem("Fishers Hill", "4381"));
            ddlCity.Items.Add(new ListItem("Fishersville", "4382"));
            ddlCity.Items.Add(new ListItem("Flint Hill", "4383"));
            ddlCity.Items.Add(new ListItem("Floyd", "4384"));
            ddlCity.Items.Add(new ListItem("Ford", "4385"));
            ddlCity.Items.Add(new ListItem("Forest", "4386"));
            ddlCity.Items.Add(new ListItem("Fork Union", "4387"));
            ddlCity.Items.Add(new ListItem("Fort Belvoir", "4388"));
            ddlCity.Items.Add(new ListItem("Fort Blackmore", "4389"));
            ddlCity.Items.Add(new ListItem("Fort Defiance", "4390"));
            ddlCity.Items.Add(new ListItem("Fort Eustis", "4391"));
            ddlCity.Items.Add(new ListItem("Fort Lee", "4392"));
            ddlCity.Items.Add(new ListItem("Fort Mitchell", "4393"));
            ddlCity.Items.Add(new ListItem("Fort Monroe", "4394"));
            ddlCity.Items.Add(new ListItem("Fort Story", "5960"));
            ddlCity.Items.Add(new ListItem("Fort Valley", "4395"));
            ddlCity.Items.Add(new ListItem("Foster", "4396"));
            ddlCity.Items.Add(new ListItem("Franklin", "4397"));
            ddlCity.Items.Add(new ListItem("Franktown", "4398"));
            ddlCity.Items.Add(new ListItem("Fredericksburg", "4399"));
            ddlCity.Items.Add(new ListItem("Free Union", "4400"));
            ddlCity.Items.Add(new ListItem("Freeman", "4401"));
            ddlCity.Items.Add(new ListItem("Fries", "4402"));
            ddlCity.Items.Add(new ListItem("Front Royal", "4403"));
            ddlCity.Items.Add(new ListItem("Ft Myer", "4404"));
            ddlCity.Items.Add(new ListItem("Fulks Run", "4405"));
            ddlCity.Items.Add(new ListItem("Gainesville", "4406"));
            ddlCity.Items.Add(new ListItem("Galax", "4407"));
            ddlCity.Items.Add(new ListItem("Garrisonville", "4408"));
            ddlCity.Items.Add(new ListItem("Gasburg", "4409"));
            ddlCity.Items.Add(new ListItem("Gate City", "4410"));
            ddlCity.Items.Add(new ListItem("Glade Hill", "4411"));
            ddlCity.Items.Add(new ListItem("Glade Spring", "4412"));
            ddlCity.Items.Add(new ListItem("Gladstone", "4413"));
            ddlCity.Items.Add(new ListItem("Gladys", "4414"));
            ddlCity.Items.Add(new ListItem("Glasgow", "4415"));
            ddlCity.Items.Add(new ListItem("Glen Allen", "4416"));
            ddlCity.Items.Add(new ListItem("Glen Lyn", "4417"));
            ddlCity.Items.Add(new ListItem("Glen Wilton", "4418"));
            ddlCity.Items.Add(new ListItem("Gloucester", "4419"));
            ddlCity.Items.Add(new ListItem("Gloucester Point", "4420"));
            ddlCity.Items.Add(new ListItem("Goldbond", "5961"));
            ddlCity.Items.Add(new ListItem("Goldvein", "4421"));
            ddlCity.Items.Add(new ListItem("Goochland", "4422"));
            ddlCity.Items.Add(new ListItem("Goode", "4423"));
            ddlCity.Items.Add(new ListItem("Goodview", "4424"));
            ddlCity.Items.Add(new ListItem("Gordonsville", "4425"));
            ddlCity.Items.Add(new ListItem("Gore", "4426"));
            ddlCity.Items.Add(new ListItem("Goshen", "4427"));
            ddlCity.Items.Add(new ListItem("Grafton", "5962"));
            ddlCity.Items.Add(new ListItem("Graves Mill", "4428"));
            ddlCity.Items.Add(new ListItem("Great Falls", "4429"));
            ddlCity.Items.Add(new ListItem("Green Bay", "4430"));
            ddlCity.Items.Add(new ListItem("Greenbackville", "4431"));
            ddlCity.Items.Add(new ListItem("Greenbush", "4432"));
            ddlCity.Items.Add(new ListItem("Greenville", "4433"));
            ddlCity.Items.Add(new ListItem("Greenway", "4434"));
            ddlCity.Items.Add(new ListItem("Greenwood", "4435"));
            ddlCity.Items.Add(new ListItem("Gretna", "4436"));
            ddlCity.Items.Add(new ListItem("Grimstead", "4437"));
            ddlCity.Items.Add(new ListItem("Grottoes", "4438"));
            ddlCity.Items.Add(new ListItem("Grundy", "4439"));
            ddlCity.Items.Add(new ListItem("Gum Spring", "4440"));
            ddlCity.Items.Add(new ListItem("Gwynn", "4441"));
            ddlCity.Items.Add(new ListItem("Hacksneck", "4442"));
            ddlCity.Items.Add(new ListItem("Hadensville", "4443"));
            ddlCity.Items.Add(new ListItem("Hague", "4444"));
            ddlCity.Items.Add(new ListItem("Halifax", "4445"));
            ddlCity.Items.Add(new ListItem("Hallieford", "4446"));
            ddlCity.Items.Add(new ListItem("Hallwood", "4447"));
            ddlCity.Items.Add(new ListItem("Hamilton", "4448"));
            ddlCity.Items.Add(new ListItem("Hampden Sydney", "4449"));
            ddlCity.Items.Add(new ListItem("Hampton", "4450"));
            ddlCity.Items.Add(new ListItem("Hanover", "4451"));
            ddlCity.Items.Add(new ListItem("Harborton", "4452"));
            ddlCity.Items.Add(new ListItem("Hardy", "4453"));
            ddlCity.Items.Add(new ListItem("Hardyville", "4454"));
            ddlCity.Items.Add(new ListItem("Harman", "5963"));
            ddlCity.Items.Add(new ListItem("Harrisonburg", "4455"));
            ddlCity.Items.Add(new ListItem("Hartfield", "4456"));
            ddlCity.Items.Add(new ListItem("Hartwood", "4457"));
            ddlCity.Items.Add(new ListItem("Hayes", "4458"));
            ddlCity.Items.Add(new ListItem("Haymarket", "4459"));
            ddlCity.Items.Add(new ListItem("Haynesville", "4460"));
            ddlCity.Items.Add(new ListItem("Haysi", "4461"));
            ddlCity.Items.Add(new ListItem("Haywood", "4462"));
            ddlCity.Items.Add(new ListItem("Head Waters", "4463"));
            ddlCity.Items.Add(new ListItem("Heathsville", "4464"));
            ddlCity.Items.Add(new ListItem("Henrico", "6013"));
            ddlCity.Items.Add(new ListItem("Henry", "4465"));
            ddlCity.Items.Add(new ListItem("Herndon", "4466"));
            ddlCity.Items.Add(new ListItem("Highland Springs", "4467"));
            ddlCity.Items.Add(new ListItem("Hightown", "5964"));
            ddlCity.Items.Add(new ListItem("Hillsboro", "5965"));
            ddlCity.Items.Add(new ListItem("Hillsville", "4468"));
            ddlCity.Items.Add(new ListItem("Hiltons", "4469"));
            ddlCity.Items.Add(new ListItem("Hinton", "4470"));
            ddlCity.Items.Add(new ListItem("Hiwassee", "4471"));
            ddlCity.Items.Add(new ListItem("Hollins", "5966"));
            ddlCity.Items.Add(new ListItem("Honaker", "4472"));
            ddlCity.Items.Add(new ListItem("Hood", "4473"));
            ddlCity.Items.Add(new ListItem("Hopewell", "4474"));
            ddlCity.Items.Add(new ListItem("Horntown", "4475"));
            ddlCity.Items.Add(new ListItem("Horsepen", "4476"));
            ddlCity.Items.Add(new ListItem("Hot Springs", "4477"));
            ddlCity.Items.Add(new ListItem("Howardsville", "4478"));
            ddlCity.Items.Add(new ListItem("Howertons", "5968"));
            ddlCity.Items.Add(new ListItem("Huddleston", "4479"));
            ddlCity.Items.Add(new ListItem("Hudgins", "4480"));
            ddlCity.Items.Add(new ListItem("Hume", "4481"));
            ddlCity.Items.Add(new ListItem("Huntly", "4482"));
            ddlCity.Items.Add(new ListItem("Hurley", "4483"));
            ddlCity.Items.Add(new ListItem("Hurt", "4484"));
            ddlCity.Items.Add(new ListItem("Hustle", "4485"));
            ddlCity.Items.Add(new ListItem("Independence", "4486"));
            ddlCity.Items.Add(new ListItem("Indian Neck", "5969"));
            ddlCity.Items.Add(new ListItem("Indian Valley", "4487"));
            ddlCity.Items.Add(new ListItem("Iron Gate", "4488"));
            ddlCity.Items.Add(new ListItem("Irvington", "4489"));
            ddlCity.Items.Add(new ListItem("Isle Of Wight", "4490"));
            ddlCity.Items.Add(new ListItem("Ivanhoe", "4491"));
            ddlCity.Items.Add(new ListItem("Ivor", "4492"));
            ddlCity.Items.Add(new ListItem("Ivy", "4493"));
            ddlCity.Items.Add(new ListItem("Jamaica", "4494"));
            ddlCity.Items.Add(new ListItem("James Store", "5970"));
            ddlCity.Items.Add(new ListItem("Jamestown", "4495"));
            ddlCity.Items.Add(new ListItem("Jamesville", "4496"));
            ddlCity.Items.Add(new ListItem("Jarratt", "4497"));
            ddlCity.Items.Add(new ListItem("Java", "4498"));
            ddlCity.Items.Add(new ListItem("Jeffersonton", "4499"));
            ddlCity.Items.Add(new ListItem("Jenkins Bridge", "4500"));
            ddlCity.Items.Add(new ListItem("Jersey", "4501"));
            ddlCity.Items.Add(new ListItem("Jetersville", "4502"));
            ddlCity.Items.Add(new ListItem("Jewell Ridge", "4503"));
            ddlCity.Items.Add(new ListItem("Jewell Valley", "5971"));
            ddlCity.Items.Add(new ListItem("Jonesville", "4504"));
            ddlCity.Items.Add(new ListItem("Keeling", "4505"));
            ddlCity.Items.Add(new ListItem("Keen Mountain", "4506"));
            ddlCity.Items.Add(new ListItem("Keene", "4507"));
            ddlCity.Items.Add(new ListItem("Keezletown", "4508"));
            ddlCity.Items.Add(new ListItem("Keller", "4509"));
            ddlCity.Items.Add(new ListItem("Kenbridge", "4510"));
            ddlCity.Items.Add(new ListItem("Kents Store", "4511"));
            ddlCity.Items.Add(new ListItem("Keokee", "4512"));
            ddlCity.Items.Add(new ListItem("Keswick", "4513"));
            ddlCity.Items.Add(new ListItem("Keysville", "4514"));
            ddlCity.Items.Add(new ListItem("Kilmarnock", "4515"));
            ddlCity.Items.Add(new ListItem("King And Queen Court House", "4516"));
            ddlCity.Items.Add(new ListItem("King George", "4517"));
            ddlCity.Items.Add(new ListItem("King William", "4518"));
            ddlCity.Items.Add(new ListItem("Kingstowne", "5972"));
            ddlCity.Items.Add(new ListItem("Kinsale", "4519"));
            ddlCity.Items.Add(new ListItem("La Crosse", "4520"));
            ddlCity.Items.Add(new ListItem("Lacey Spring", "4521"));
            ddlCity.Items.Add(new ListItem("Lackey", "4522"));
            ddlCity.Items.Add(new ListItem("Ladysmith", "4523"));
            ddlCity.Items.Add(new ListItem("Lafayette", "5973"));
            ddlCity.Items.Add(new ListItem("Lake Ridge", "5974"));
            ddlCity.Items.Add(new ListItem("Lambsburg", "4524"));
            ddlCity.Items.Add(new ListItem("Lancaster", "4525"));
            ddlCity.Items.Add(new ListItem("Laneview", "4526"));
            ddlCity.Items.Add(new ListItem("Lanexa", "4527"));
            ddlCity.Items.Add(new ListItem("Lansdowne", "5975"));
            ddlCity.Items.Add(new ListItem("Laurel Fork", "4528"));
            ddlCity.Items.Add(new ListItem("Lawrenceville", "4529"));
            ddlCity.Items.Add(new ListItem("Lebanon", "4530"));
            ddlCity.Items.Add(new ListItem("Leesburg", "4531"));
            ddlCity.Items.Add(new ListItem("Leon", "4532"));
            ddlCity.Items.Add(new ListItem("Lewisetta", "5977"));
            ddlCity.Items.Add(new ListItem("Lexington", "4533"));
            ddlCity.Items.Add(new ListItem("Lightfoot", "4534"));
            ddlCity.Items.Add(new ListItem("Lignum", "4535"));
            ddlCity.Items.Add(new ListItem("Lincoln", "4536"));
            ddlCity.Items.Add(new ListItem("Linden", "4537"));
            ddlCity.Items.Add(new ListItem("Linville", "4538"));
            ddlCity.Items.Add(new ListItem("Little Plymouth", "4539"));
            ddlCity.Items.Add(new ListItem("Lively", "4540"));
            ddlCity.Items.Add(new ListItem("Locust Dale", "4541"));
            ddlCity.Items.Add(new ListItem("Locust Grove", "4542"));
            ddlCity.Items.Add(new ListItem("Locust Hill", "4543"));
            ddlCity.Items.Add(new ListItem("Locustville", "4544"));
            ddlCity.Items.Add(new ListItem("Long Island", "4545"));
            ddlCity.Items.Add(new ListItem("Loretto", "4546"));
            ddlCity.Items.Add(new ListItem("Lorton", "4547"));
            ddlCity.Items.Add(new ListItem("Lottsburg", "4548"));
            ddlCity.Items.Add(new ListItem("Louisa", "4549"));
            ddlCity.Items.Add(new ListItem("Lovettsville", "4550"));
            ddlCity.Items.Add(new ListItem("Lovingston", "4551"));
            ddlCity.Items.Add(new ListItem("Low Moor", "4552"));
            ddlCity.Items.Add(new ListItem("Lowesville", "5978"));
            ddlCity.Items.Add(new ListItem("Lowry", "4553"));
            ddlCity.Items.Add(new ListItem("Lunenburg", "4554"));
            ddlCity.Items.Add(new ListItem("Luray", "4555"));
            ddlCity.Items.Add(new ListItem("Lynch Station", "4556"));
            ddlCity.Items.Add(new ListItem("Lynchburg", "4557"));
            ddlCity.Items.Add(new ListItem("Lyndhurst", "4558"));
            ddlCity.Items.Add(new ListItem("Machipongo", "4559"));
            ddlCity.Items.Add(new ListItem("Macon", "4560"));
            ddlCity.Items.Add(new ListItem("Madison", "4561"));
            ddlCity.Items.Add(new ListItem("Madison Heights", "4562"));
            ddlCity.Items.Add(new ListItem("Maidens", "4563"));
            ddlCity.Items.Add(new ListItem("Manakin Sabot", "4564"));
            ddlCity.Items.Add(new ListItem("Manassas", "4565"));
            ddlCity.Items.Add(new ListItem("Manassas Park", "5979"));
            ddlCity.Items.Add(new ListItem("Mannboro", "4566"));
            ddlCity.Items.Add(new ListItem("Manquin", "4567"));
            ddlCity.Items.Add(new ListItem("Mappsville", "4568"));
            ddlCity.Items.Add(new ListItem("Marion", "4569"));
            ddlCity.Items.Add(new ListItem("Marionville", "4570"));
            ddlCity.Items.Add(new ListItem("Markham", "4571"));
            ddlCity.Items.Add(new ListItem("Marshall", "4572"));
            ddlCity.Items.Add(new ListItem("Martinsville", "4573"));
            ddlCity.Items.Add(new ListItem("Maryus", "4574"));
            ddlCity.Items.Add(new ListItem("Mascot", "4575"));
            ddlCity.Items.Add(new ListItem("Mason Neck", "5980"));
            ddlCity.Items.Add(new ListItem("Massies Mill", "5981"));
            ddlCity.Items.Add(new ListItem("Mathews", "4576"));
            ddlCity.Items.Add(new ListItem("Mattaponi", "4577"));
            ddlCity.Items.Add(new ListItem("Maurertown", "4578"));
            ddlCity.Items.Add(new ListItem("Mavisdale", "4579"));
            ddlCity.Items.Add(new ListItem("Max Meadows", "4580"));
            ddlCity.Items.Add(new ListItem("Maxie", "4581"));
            ddlCity.Items.Add(new ListItem("McClure", "4582"));
            ddlCity.Items.Add(new ListItem("McCoy", "4583"));
            ddlCity.Items.Add(new ListItem("McDowell", "4584"));
            ddlCity.Items.Add(new ListItem("McGaheysville", "4585"));
            ddlCity.Items.Add(new ListItem("McKenney", "4586"));
            ddlCity.Items.Add(new ListItem("McLean", "4587"));
            ddlCity.Items.Add(new ListItem("Meadows Of Dan", "4588"));
            ddlCity.Items.Add(new ListItem("Meadowview", "4589"));
            ddlCity.Items.Add(new ListItem("Mears", "4590"));
            ddlCity.Items.Add(new ListItem("Mechanicsville", "4591"));
            ddlCity.Items.Add(new ListItem("Meherrin", "4592"));
            ddlCity.Items.Add(new ListItem("Melfa", "4593"));
            ddlCity.Items.Add(new ListItem("Mendota", "4594"));
            ddlCity.Items.Add(new ListItem("Meredithville", "4595"));
            ddlCity.Items.Add(new ListItem("Merrifield", "4596"));
            ddlCity.Items.Add(new ListItem("Merry Point", "4597"));
            ddlCity.Items.Add(new ListItem("Middlebrook", "4598"));
            ddlCity.Items.Add(new ListItem("Middleburg", "4599"));
            ddlCity.Items.Add(new ListItem("Middletown", "4600"));
            ddlCity.Items.Add(new ListItem("Midland", "4601"));
            ddlCity.Items.Add(new ListItem("Midlothian", "4602"));
            ddlCity.Items.Add(new ListItem("Milford", "4603"));
            ddlCity.Items.Add(new ListItem("Millboro", "4604"));
            ddlCity.Items.Add(new ListItem("Millers Tavern", "4605"));
            ddlCity.Items.Add(new ListItem("Millwood", "4606"));
            ddlCity.Items.Add(new ListItem("Mine Run", "5982"));
            ddlCity.Items.Add(new ListItem("Mineral", "4607"));
            ddlCity.Items.Add(new ListItem("Mint Spring", "4608"));
            ddlCity.Items.Add(new ListItem("Mitchells", "4609"));
            ddlCity.Items.Add(new ListItem("Mobjack", "5983"));
            ddlCity.Items.Add(new ListItem("Modest Town", "4610"));
            ddlCity.Items.Add(new ListItem("Mollusk", "4611"));
            ddlCity.Items.Add(new ListItem("Moneta", "4612"));
            ddlCity.Items.Add(new ListItem("Monroe", "4613"));
            ddlCity.Items.Add(new ListItem("Montclair", "5984"));
            ddlCity.Items.Add(new ListItem("Montebello", "4614"));
            ddlCity.Items.Add(new ListItem("Monterey", "4615"));
            ddlCity.Items.Add(new ListItem("Montpelier", "4616"));
            ddlCity.Items.Add(new ListItem("Montpelier Station", "4617"));
            ddlCity.Items.Add(new ListItem("Montross", "4618"));
            ddlCity.Items.Add(new ListItem("Montvale", "4619"));
            ddlCity.Items.Add(new ListItem("Moon", "4620"));
            ddlCity.Items.Add(new ListItem("Morattico", "4621"));
            ddlCity.Items.Add(new ListItem("Moseley", "4622"));
            ddlCity.Items.Add(new ListItem("Mount Crawford", "4623"));
            ddlCity.Items.Add(new ListItem("Mount Holly", "4624"));
            ddlCity.Items.Add(new ListItem("Mount Jackson", "4625"));
            ddlCity.Items.Add(new ListItem("Mount Sidney", "4626"));
            ddlCity.Items.Add(new ListItem("Mount Solon", "4627"));
            ddlCity.Items.Add(new ListItem("Mount Vernon", "4628"));
            ddlCity.Items.Add(new ListItem("Mouth Of Wilson", "4629"));
            ddlCity.Items.Add(new ListItem("Mustoe", "4630"));
            ddlCity.Items.Add(new ListItem("Narrows", "4631"));
            ddlCity.Items.Add(new ListItem("Naruna", "4632"));
            ddlCity.Items.Add(new ListItem("Nassawadox", "4633"));
            ddlCity.Items.Add(new ListItem("Nathalie", "4634"));
            ddlCity.Items.Add(new ListItem("Natural Bridge", "4635"));
            ddlCity.Items.Add(new ListItem("Natural Bridge Station", "4636"));
            ddlCity.Items.Add(new ListItem("Naxera", "5985"));
            ddlCity.Items.Add(new ListItem("Nellysford", "4637"));
            ddlCity.Items.Add(new ListItem("Nelson", "4638"));
            ddlCity.Items.Add(new ListItem("Nelsonia", "4639"));
            ddlCity.Items.Add(new ListItem("New Baltimore", "5986"));
            ddlCity.Items.Add(new ListItem("New Canton", "4640"));
            ddlCity.Items.Add(new ListItem("New Castle", "4641"));
            ddlCity.Items.Add(new ListItem("New Church", "4642"));
            ddlCity.Items.Add(new ListItem("New Hope", "4643"));
            ddlCity.Items.Add(new ListItem("New Kent", "4644"));
            ddlCity.Items.Add(new ListItem("New Market", "4645"));
            ddlCity.Items.Add(new ListItem("New Point", "4646"));
            ddlCity.Items.Add(new ListItem("New River", "4647"));
            ddlCity.Items.Add(new ListItem("Newbern", "4648"));
            ddlCity.Items.Add(new ListItem("Newington", "4649"));
            ddlCity.Items.Add(new ListItem("Newport", "4650"));
            ddlCity.Items.Add(new ListItem("Newport News", "4651"));
            ddlCity.Items.Add(new ListItem("Newsoms", "4652"));
            ddlCity.Items.Add(new ListItem("Newtown", "4653"));
            ddlCity.Items.Add(new ListItem("Nickelsville", "4654"));
            ddlCity.Items.Add(new ListItem("Ninde", "4655"));
            ddlCity.Items.Add(new ListItem("Nokesville", "4656"));
            ddlCity.Items.Add(new ListItem("Nora", "4657"));
            ddlCity.Items.Add(new ListItem("Norfolk", "4658"));
            ddlCity.Items.Add(new ListItem("Norge", "4659"));
            ddlCity.Items.Add(new ListItem("North", "4660"));
            ddlCity.Items.Add(new ListItem("North Garden", "4661"));
            ddlCity.Items.Add(new ListItem("North Springfield", "5987"));
            ddlCity.Items.Add(new ListItem("North Tazewell", "4662"));
            ddlCity.Items.Add(new ListItem("Norton", "4663"));
            ddlCity.Items.Add(new ListItem("Norwood", "4664"));
            ddlCity.Items.Add(new ListItem("Nottoway", "4665"));
            ddlCity.Items.Add(new ListItem("Nuttsville", "4666"));
            ddlCity.Items.Add(new ListItem("Oak Grove", "5988"));
            ddlCity.Items.Add(new ListItem("Oak Hall", "4667"));
            ddlCity.Items.Add(new ListItem("Oak Hill", "5989"));
            ddlCity.Items.Add(new ListItem("Oakpark", "4668"));
            ddlCity.Items.Add(new ListItem("Oakton", "4669"));
            ddlCity.Items.Add(new ListItem("Oakwood", "4670"));
            ddlCity.Items.Add(new ListItem("Occoquan", "4671"));
            ddlCity.Items.Add(new ListItem("Oilville", "4672"));
            ddlCity.Items.Add(new ListItem("Oldhams", "4673"));
            ddlCity.Items.Add(new ListItem("Onancock", "4674"));
            ddlCity.Items.Add(new ListItem("Onemo", "4675"));
            ddlCity.Items.Add(new ListItem("Onley", "4676"));
            ddlCity.Items.Add(new ListItem("Ophelia", "4677"));
            ddlCity.Items.Add(new ListItem("Orange", "4678"));
            ddlCity.Items.Add(new ListItem("Ordinary", "4679"));
            ddlCity.Items.Add(new ListItem("Oriskany", "4680"));
            ddlCity.Items.Add(new ListItem("Orkney Springs", "4681"));
            ddlCity.Items.Add(new ListItem("Orlean", "4682"));
            ddlCity.Items.Add(new ListItem("Oyster", "4683"));
            ddlCity.Items.Add(new ListItem("Paeonian Springs", "4684"));
            ddlCity.Items.Add(new ListItem("Paint Bank", "4685"));
            ddlCity.Items.Add(new ListItem("Painter", "4686"));
            ddlCity.Items.Add(new ListItem("Palmyra", "4687"));
            ddlCity.Items.Add(new ListItem("Pamplin", "4688"));
            ddlCity.Items.Add(new ListItem("Paris", "4689"));
            ddlCity.Items.Add(new ListItem("Parksley", "4690"));
            ddlCity.Items.Add(new ListItem("Parrott", "4691"));
            ddlCity.Items.Add(new ListItem("Partlow", "4692"));
            ddlCity.Items.Add(new ListItem("Patrick Springs", "4693"));
            ddlCity.Items.Add(new ListItem("Patterson", "5990"));
            ddlCity.Items.Add(new ListItem("Pearisburg", "4694"));
            ddlCity.Items.Add(new ListItem("Pembroke", "4695"));
            ddlCity.Items.Add(new ListItem("Penhook", "4696"));
            ddlCity.Items.Add(new ListItem("Penn Laird", "4697"));
            ddlCity.Items.Add(new ListItem("Pennington Gap", "4698"));
            ddlCity.Items.Add(new ListItem("Petersburg", "4699"));
            ddlCity.Items.Add(new ListItem("Phenix", "4700"));
            ddlCity.Items.Add(new ListItem("Philomont", "4701"));
            ddlCity.Items.Add(new ListItem("Pilgrims Knob", "4702"));
            ddlCity.Items.Add(new ListItem("Pilot", "4703"));
            ddlCity.Items.Add(new ListItem("Piney River", "4704"));
            ddlCity.Items.Add(new ListItem("Pittsville", "4705"));
            ddlCity.Items.Add(new ListItem("Plain View", "5991"));
            ddlCity.Items.Add(new ListItem("Pleasant Valley", "4706"));
            ddlCity.Items.Add(new ListItem("Pocahontas", "4707"));
            ddlCity.Items.Add(new ListItem("Poquoson", "4708"));
            ddlCity.Items.Add(new ListItem("Port Haywood", "4709"));
            ddlCity.Items.Add(new ListItem("Port Republic", "4710"));
            ddlCity.Items.Add(new ListItem("Port Royal", "4711"));
            ddlCity.Items.Add(new ListItem("Portsmouth", "4712"));
            ddlCity.Items.Add(new ListItem("Potomac Falls", "5992"));
            ddlCity.Items.Add(new ListItem("Pound", "4713"));
            ddlCity.Items.Add(new ListItem("Pounding Mill", "4714"));
            ddlCity.Items.Add(new ListItem("Powhatan", "4715"));
            ddlCity.Items.Add(new ListItem("Pratts", "4716"));
            ddlCity.Items.Add(new ListItem("Prince George", "4717"));
            ddlCity.Items.Add(new ListItem("Prince William", "5993"));
            ddlCity.Items.Add(new ListItem("Prospect", "4718"));
            ddlCity.Items.Add(new ListItem("Providence Forge", "4719"));
            ddlCity.Items.Add(new ListItem("Pulaski", "4720"));
            ddlCity.Items.Add(new ListItem("Pungoteague", "4721"));
            ddlCity.Items.Add(new ListItem("Purcellville", "4722"));
            ddlCity.Items.Add(new ListItem("Quantico", "4723"));
            ddlCity.Items.Add(new ListItem("Quicksburg", "4724"));
            ddlCity.Items.Add(new ListItem("Quinby", "4725"));
            ddlCity.Items.Add(new ListItem("Quinque", "4726"));
            ddlCity.Items.Add(new ListItem("Quinton", "4727"));
            ddlCity.Items.Add(new ListItem("Radford", "4728"));
            ddlCity.Items.Add(new ListItem("Radiant", "4729"));
            ddlCity.Items.Add(new ListItem("Randolph", "4730"));
            ddlCity.Items.Add(new ListItem("Raphine", "4731"));
            ddlCity.Items.Add(new ListItem("Rapidan", "4732"));
            ddlCity.Items.Add(new ListItem("Rappahannock Academy", "4733"));
            ddlCity.Items.Add(new ListItem("Raven", "4734"));
            ddlCity.Items.Add(new ListItem("Rawlings", "4735"));
            ddlCity.Items.Add(new ListItem("Rectortown", "4736"));
            ddlCity.Items.Add(new ListItem("Red Ash", "4737"));
            ddlCity.Items.Add(new ListItem("Red House", "4738"));
            ddlCity.Items.Add(new ListItem("Red Oak", "4739"));
            ddlCity.Items.Add(new ListItem("Redart", "5994"));
            ddlCity.Items.Add(new ListItem("Redwood", "4740"));
            ddlCity.Items.Add(new ListItem("Reedville", "4741"));
            ddlCity.Items.Add(new ListItem("Remington", "4742"));
            ddlCity.Items.Add(new ListItem("Rescue", "4743"));
            ddlCity.Items.Add(new ListItem("Reston", "4744"));
            ddlCity.Items.Add(new ListItem("Reva", "4745"));
            ddlCity.Items.Add(new ListItem("Rhoadesville", "4746"));
            ddlCity.Items.Add(new ListItem("Rice", "4747"));
            ddlCity.Items.Add(new ListItem("Rich Creek", "4748"));
            ddlCity.Items.Add(new ListItem("Richardsville", "4749"));
            ddlCity.Items.Add(new ListItem("Richlands", "4750"));
            ddlCity.Items.Add(new ListItem("Richmond", "4751"));
            ddlCity.Items.Add(new ListItem("Ridgeway", "4752"));
            ddlCity.Items.Add(new ListItem("Rileyville", "4753"));
            ddlCity.Items.Add(new ListItem("Riner", "4754"));
            ddlCity.Items.Add(new ListItem("Ringgold", "4755"));
            ddlCity.Items.Add(new ListItem("Ripplemead", "4756"));
            ddlCity.Items.Add(new ListItem("Riverton", "5995"));
            ddlCity.Items.Add(new ListItem("Rixeyville", "4757"));
            ddlCity.Items.Add(new ListItem("Roanoke", "4758"));
            ddlCity.Items.Add(new ListItem("Rochelle", "4759"));
            ddlCity.Items.Add(new ListItem("Rockbridge Baths", "4760"));
            ddlCity.Items.Add(new ListItem("Rockville", "4761"));
            ddlCity.Items.Add(new ListItem("Rocky Gap", "4762"));
            ddlCity.Items.Add(new ListItem("Rocky Mount", "4763"));
            ddlCity.Items.Add(new ListItem("Rollins Fork", "4764"));
            ddlCity.Items.Add(new ListItem("Rose Hill", "4765"));
            ddlCity.Items.Add(new ListItem("Rosedale", "4766"));
            ddlCity.Items.Add(new ListItem("Roseland", "4767"));
            ddlCity.Items.Add(new ListItem("Round Hill", "4768"));
            ddlCity.Items.Add(new ListItem("Rowe", "4769"));
            ddlCity.Items.Add(new ListItem("Ruby", "4770"));
            ddlCity.Items.Add(new ListItem("Ruckersville", "4771"));
            ddlCity.Items.Add(new ListItem("Rural Retreat", "4772"));
            ddlCity.Items.Add(new ListItem("Rustburg", "4773"));
            ddlCity.Items.Add(new ListItem("Ruther Glen", "4774"));
            ddlCity.Items.Add(new ListItem("Ruthville", "4775"));
            ddlCity.Items.Add(new ListItem("Saint Charles", "4776"));
            ddlCity.Items.Add(new ListItem("Saint Paul", "4777"));
            ddlCity.Items.Add(new ListItem("Saint Stephens Church", "4778"));
            ddlCity.Items.Add(new ListItem("Salem", "4779"));
            ddlCity.Items.Add(new ListItem("Saltville", "4780"));
            ddlCity.Items.Add(new ListItem("Saluda", "4781"));
            ddlCity.Items.Add(new ListItem("Sandston", "4782"));
            ddlCity.Items.Add(new ListItem("Sandy Hook", "4783"));
            ddlCity.Items.Add(new ListItem("Sandy Level", "4784"));
            ddlCity.Items.Add(new ListItem("Sandy Point", "4785"));
            ddlCity.Items.Add(new ListItem("Sanford", "4786"));
            ddlCity.Items.Add(new ListItem("Saxe", "4787"));
            ddlCity.Items.Add(new ListItem("Saxis", "4788"));
            ddlCity.Items.Add(new ListItem("Schley", "4789"));
            ddlCity.Items.Add(new ListItem("Schuyler", "4790"));
            ddlCity.Items.Add(new ListItem("Scottsburg", "4791"));
            ddlCity.Items.Add(new ListItem("Scottsville", "4792"));
            ddlCity.Items.Add(new ListItem("Seaford", "4793"));
            ddlCity.Items.Add(new ListItem("Sealston", "4794"));
            ddlCity.Items.Add(new ListItem("Seaview", "4795"));
            ddlCity.Items.Add(new ListItem("Sedley", "4796"));
            ddlCity.Items.Add(new ListItem("Selma", "4797"));
            ddlCity.Items.Add(new ListItem("Seven Mile Ford", "5996"));
            ddlCity.Items.Add(new ListItem("Severn", "4798"));
            ddlCity.Items.Add(new ListItem("Shacklefords", "4799"));
            ddlCity.Items.Add(new ListItem("Sharps", "4800"));
            ddlCity.Items.Add(new ListItem("Shawsville", "4801"));
            ddlCity.Items.Add(new ListItem("Shenandoah", "4802"));
            ddlCity.Items.Add(new ListItem("Sherando", "5997"));
            ddlCity.Items.Add(new ListItem("Shiloh", "5998"));
            ddlCity.Items.Add(new ListItem("Shipman", "4803"));
            ddlCity.Items.Add(new ListItem("Shortt Gap", "4804"));
            ddlCity.Items.Add(new ListItem("Singers Glen", "4805"));
            ddlCity.Items.Add(new ListItem("Skippers", "4806"));
            ddlCity.Items.Add(new ListItem("Skipwith", "4807"));
            ddlCity.Items.Add(new ListItem("Smithfield", "4808"));
            ddlCity.Items.Add(new ListItem("Somerset", "4809"));
            ddlCity.Items.Add(new ListItem("Somerville", "4810"));
            ddlCity.Items.Add(new ListItem("South Boston", "4811"));
            ddlCity.Items.Add(new ListItem("South Hill", "4812"));
            ddlCity.Items.Add(new ListItem("South Riding", "5999"));
            ddlCity.Items.Add(new ListItem("Sparta", "4813"));
            ddlCity.Items.Add(new ListItem("Speedwell", "4814"));
            ddlCity.Items.Add(new ListItem("Spencer", "4815"));
            ddlCity.Items.Add(new ListItem("Sperryville", "4816"));
            ddlCity.Items.Add(new ListItem("Spotsylvania", "4817"));
            ddlCity.Items.Add(new ListItem("Spottswood", "6000"));
            ddlCity.Items.Add(new ListItem("Spout Spring", "4818"));
            ddlCity.Items.Add(new ListItem("Spring Grove", "4819"));
            ddlCity.Items.Add(new ListItem("Springfield", "4820"));
            ddlCity.Items.Add(new ListItem("Stafford", "4821"));
            ddlCity.Items.Add(new ListItem("Staffordsville", "4822"));
            ddlCity.Items.Add(new ListItem("Stanardsville", "4823"));
            ddlCity.Items.Add(new ListItem("Stanley", "4824"));
            ddlCity.Items.Add(new ListItem("Stanleytown", "4825"));
            ddlCity.Items.Add(new ListItem("Star Tannery", "4826"));
            ddlCity.Items.Add(new ListItem("State Farm", "4827"));
            ddlCity.Items.Add(new ListItem("Staunton", "4828"));
            ddlCity.Items.Add(new ListItem("Steeles Tavern", "4829"));
            ddlCity.Items.Add(new ListItem("Stephens City", "4830"));
            ddlCity.Items.Add(new ListItem("Stephenson", "4831"));
            ddlCity.Items.Add(new ListItem("Sterling", "4832"));
            ddlCity.Items.Add(new ListItem("Stevensburg", "4833"));
            ddlCity.Items.Add(new ListItem("Stevensville", "4834"));
            ddlCity.Items.Add(new ListItem("Stone Ridge", "6001"));
            ddlCity.Items.Add(new ListItem("Stonega", "6002"));
            ddlCity.Items.Add(new ListItem("Stony Creek", "4835"));
            ddlCity.Items.Add(new ListItem("Strasburg", "4836"));
            ddlCity.Items.Add(new ListItem("Stratford", "4837"));
            ddlCity.Items.Add(new ListItem("Stuart", "4838"));
            ddlCity.Items.Add(new ListItem("Stuarts Draft", "4839"));
            ddlCity.Items.Add(new ListItem("Studley", "4840"));
            ddlCity.Items.Add(new ListItem("Suffolk", "4841"));
            ddlCity.Items.Add(new ListItem("Sugar Grove", "4842"));
            ddlCity.Items.Add(new ListItem("Sumerduck", "4843"));
            ddlCity.Items.Add(new ListItem("Supply", "6003"));
            ddlCity.Items.Add(new ListItem("Surry", "4844"));
            ddlCity.Items.Add(new ListItem("Susan", "4845"));
            ddlCity.Items.Add(new ListItem("Sussex", "4846"));
            ddlCity.Items.Add(new ListItem("Sutherland", "4847"));
            ddlCity.Items.Add(new ListItem("Sutherlin", "4848"));
            ddlCity.Items.Add(new ListItem("Sweet Briar", "4849"));
            ddlCity.Items.Add(new ListItem("Swoope", "4850"));
            ddlCity.Items.Add(new ListItem("Swords Creek", "4851"));
            ddlCity.Items.Add(new ListItem("Syria", "4852"));
            ddlCity.Items.Add(new ListItem("Tabb", "6004"));
            ddlCity.Items.Add(new ListItem("Tangier", "4853"));
            ddlCity.Items.Add(new ListItem("Tannersville", "4854"));
            ddlCity.Items.Add(new ListItem("Tappahannock", "4855"));
            ddlCity.Items.Add(new ListItem("Tasley", "4856"));
            ddlCity.Items.Add(new ListItem("Tazewell", "4857"));
            ddlCity.Items.Add(new ListItem("Temperanceville", "4858"));
            ddlCity.Items.Add(new ListItem("Thaxton", "4859"));
            ddlCity.Items.Add(new ListItem("The Plains", "4860"));
            ddlCity.Items.Add(new ListItem("Thornburg", "4861"));
            ddlCity.Items.Add(new ListItem("Timberville", "4862"));
            ddlCity.Items.Add(new ListItem("Toano", "4863"));
            ddlCity.Items.Add(new ListItem("Toms Brook", "4864"));
            ddlCity.Items.Add(new ListItem("Topping", "4865"));
            ddlCity.Items.Add(new ListItem("Townsend", "4866"));
            ddlCity.Items.Add(new ListItem("Trammel", "6005"));
            ddlCity.Items.Add(new ListItem("Trevilians", "4867"));
            ddlCity.Items.Add(new ListItem("Triangle", "4868"));
            ddlCity.Items.Add(new ListItem("Troutdale", "4869"));
            ddlCity.Items.Add(new ListItem("Troutville", "4870"));
            ddlCity.Items.Add(new ListItem("Troy", "4871"));
            ddlCity.Items.Add(new ListItem("Tyro", "4872"));
            ddlCity.Items.Add(new ListItem("Union Hall", "4873"));
            ddlCity.Items.Add(new ListItem("Unionville", "4874"));
            ddlCity.Items.Add(new ListItem("University Of Richmond", "4875"));
            ddlCity.Items.Add(new ListItem("Upperville", "4876"));
            ddlCity.Items.Add(new ListItem("Urbanna", "4877"));
            ddlCity.Items.Add(new ListItem("Valentines", "4878"));
            ddlCity.Items.Add(new ListItem("Vansant", "4879"));
            ddlCity.Items.Add(new ListItem("Vernon Hill", "4880"));
            ddlCity.Items.Add(new ListItem("Verona", "4881"));
            ddlCity.Items.Add(new ListItem("Vesta", "4882"));
            ddlCity.Items.Add(new ListItem("Vesuvius", "4883"));
            ddlCity.Items.Add(new ListItem("Victoria", "4884"));
            ddlCity.Items.Add(new ListItem("Vienna", "4885"));
            ddlCity.Items.Add(new ListItem("Viewtown", "4886"));
            ddlCity.Items.Add(new ListItem("Village", "4887"));
            ddlCity.Items.Add(new ListItem("Villamont", "4888"));
            ddlCity.Items.Add(new ListItem("Vinton", "4889"));
            ddlCity.Items.Add(new ListItem("Virgilina", "4890"));
            ddlCity.Items.Add(new ListItem("Virginia Beach", "4891"));
            ddlCity.Items.Add(new ListItem("Wachapreague", "4892"));
            ddlCity.Items.Add(new ListItem("Wake", "4893"));
            ddlCity.Items.Add(new ListItem("Wakefield", "4894"));
            ddlCity.Items.Add(new ListItem("Walkerton", "4895"));
            ddlCity.Items.Add(new ListItem("Wallops Island", "4896"));
            ddlCity.Items.Add(new ListItem("Walters", "6006"));
            ddlCity.Items.Add(new ListItem("Wardtown", "4897"));
            ddlCity.Items.Add(new ListItem("Ware Neck", "4898"));
            ddlCity.Items.Add(new ListItem("Warfield", "4899"));
            ddlCity.Items.Add(new ListItem("Warm Springs", "4900"));
            ddlCity.Items.Add(new ListItem("Warrenton", "4901"));
            ddlCity.Items.Add(new ListItem("Warsaw", "4902"));
            ddlCity.Items.Add(new ListItem("Washington", "4903"));
            ddlCity.Items.Add(new ListItem("Water View", "4904"));
            ddlCity.Items.Add(new ListItem("Waterford", "4905"));
            ddlCity.Items.Add(new ListItem("Wattsville", "4906"));
            ddlCity.Items.Add(new ListItem("Waverly", "4907"));
            ddlCity.Items.Add(new ListItem("Waynesboro", "4908"));
            ddlCity.Items.Add(new ListItem("Weber City", "4909"));
            ddlCity.Items.Add(new ListItem("Weems", "4910"));
            ddlCity.Items.Add(new ListItem("Weirwood", "6007"));
            ddlCity.Items.Add(new ListItem("West Augusta", "4911"));
            ddlCity.Items.Add(new ListItem("West Mclean", "4912"));
            ddlCity.Items.Add(new ListItem("West Point", "4913"));
            ddlCity.Items.Add(new ListItem("West Springfield", "6008"));
            ddlCity.Items.Add(new ListItem("Weyers Cave", "4914"));
            ddlCity.Items.Add(new ListItem("White Hall", "4915"));
            ddlCity.Items.Add(new ListItem("White Marsh", "4916"));
            ddlCity.Items.Add(new ListItem("White Plains", "4917"));
            ddlCity.Items.Add(new ListItem("White Post", "4918"));
            ddlCity.Items.Add(new ListItem("White Stone", "4919"));
            ddlCity.Items.Add(new ListItem("Whitetop", "4920"));
            ddlCity.Items.Add(new ListItem("Whitewood", "4921"));
            ddlCity.Items.Add(new ListItem("Wicomico", "4922"));
            ddlCity.Items.Add(new ListItem("Wicomico Church", "4923"));
            ddlCity.Items.Add(new ListItem("Williamsburg", "4924"));
            ddlCity.Items.Add(new ListItem("Williamsville", "4925"));
            ddlCity.Items.Add(new ListItem("Willis", "4926"));
            ddlCity.Items.Add(new ListItem("Willis Wharf", "4927"));
            ddlCity.Items.Add(new ListItem("Wilsons", "4928"));
            ddlCity.Items.Add(new ListItem("Winchester", "4929"));
            ddlCity.Items.Add(new ListItem("Windsor", "4930"));
            ddlCity.Items.Add(new ListItem("Wingina", "4931"));
            ddlCity.Items.Add(new ListItem("Wintergreen", "6009"));
            ddlCity.Items.Add(new ListItem("Wirtz", "4932"));
            ddlCity.Items.Add(new ListItem("Wise", "4933"));
            ddlCity.Items.Add(new ListItem("Withams", "4934"));
            ddlCity.Items.Add(new ListItem("Wolford", "4935"));
            ddlCity.Items.Add(new ListItem("Wolftown", "4936"));
            ddlCity.Items.Add(new ListItem("Woodberry Forest", "4937"));
            ddlCity.Items.Add(new ListItem("Woodbridge", "4938"));
            ddlCity.Items.Add(new ListItem("Woodford", "4939"));
            ddlCity.Items.Add(new ListItem("Woodlawn", "4940"));
            ddlCity.Items.Add(new ListItem("Woods Cross Roads", "4941"));
            ddlCity.Items.Add(new ListItem("Woodstock", "4942"));
            ddlCity.Items.Add(new ListItem("Woodville", "4943"));
            ddlCity.Items.Add(new ListItem("Woolwine", "4944"));
            ddlCity.Items.Add(new ListItem("Wylliesburg", "4945"));
            ddlCity.Items.Add(new ListItem("Wytheville", "4946"));
            ddlCity.Items.Add(new ListItem("Yale", "4947"));
            ddlCity.Items.Add(new ListItem("Yards", "6010"));
            ddlCity.Items.Add(new ListItem("Yorktown", "4948"));
            ddlCity.Items.Add(new ListItem("Zacata", "4949"));
            ddlCity.Items.Add(new ListItem("Zuni", "4950"));
            #endregion

            #region States
            ddlState.Items.Add(new ListItem(string.Empty));
            ddlState.Items.Add(new ListItem("Alabama", "1000"));
            ddlState.Items.Add(new ListItem("Alaska", "1001"));
            ddlState.Items.Add(new ListItem("Arizona", "1002"));
            ddlState.Items.Add(new ListItem("Arkansas", "1003"));
            ddlState.Items.Add(new ListItem("California", "1004"));
            ddlState.Items.Add(new ListItem("Colorado", "1005"));
            ddlState.Items.Add(new ListItem("Connecticut", "1006"));
            ddlState.Items.Add(new ListItem("Delaware", "1007"));
            ddlState.Items.Add(new ListItem("District Of Columbia", "1008"));
            ddlState.Items.Add(new ListItem("Florida", "1009"));
            ddlState.Items.Add(new ListItem("Georgia", "1010"));
            ddlState.Items.Add(new ListItem("Hawaii", "1011"));
            ddlState.Items.Add(new ListItem("Idaho", "1012"));
            ddlState.Items.Add(new ListItem("Illinois", "1013"));
            ddlState.Items.Add(new ListItem("Indiana", "1014"));
            ddlState.Items.Add(new ListItem("Iowa", "1015"));
            ddlState.Items.Add(new ListItem("Kansas", "1016"));
            ddlState.Items.Add(new ListItem("Kentucky", "1017"));
            ddlState.Items.Add(new ListItem("Louisiana", "1018"));
            ddlState.Items.Add(new ListItem("Maine", "1019"));
            ddlState.Items.Add(new ListItem("Maryland", "1020"));
            ddlState.Items.Add(new ListItem("Massachusetts", "1021"));
            ddlState.Items.Add(new ListItem("Michigan", "1022"));
            ddlState.Items.Add(new ListItem("Minnesota", "1023"));
            ddlState.Items.Add(new ListItem("Mississippi", "1024"));
            ddlState.Items.Add(new ListItem("Missouri", "1025"));
            ddlState.Items.Add(new ListItem("Montana", "1026"));
            ddlState.Items.Add(new ListItem("Nebraska", "1027"));
            ddlState.Items.Add(new ListItem("Nevada", "1028"));
            ddlState.Items.Add(new ListItem("New Hampshire", "1029"));
            ddlState.Items.Add(new ListItem("New Jersey", "1030"));
            ddlState.Items.Add(new ListItem("New Mexico", "1031"));
            ddlState.Items.Add(new ListItem("New York", "1032"));
            ddlState.Items.Add(new ListItem("North Carolina", "1033"));
            ddlState.Items.Add(new ListItem("North Dakota", "1034"));
            ddlState.Items.Add(new ListItem("Ohio", "1035"));
            ddlState.Items.Add(new ListItem("Oklahoma", "1036"));
            ddlState.Items.Add(new ListItem("Oregon", "1037"));
            ddlState.Items.Add(new ListItem("Pennsylvania", "1038"));
            ddlState.Items.Add(new ListItem("Puerto Rico", "1039"));
            ddlState.Items.Add(new ListItem("Rhode Island", "1040"));
            ddlState.Items.Add(new ListItem("South Carolina", "1041"));
            ddlState.Items.Add(new ListItem("South Dakota", "1042"));
            ddlState.Items.Add(new ListItem("Tennessee", "1043"));
            ddlState.Items.Add(new ListItem("Texas", "1044"));
            ddlState.Items.Add(new ListItem("Utah", "1045"));
            ddlState.Items.Add(new ListItem("Vermont", "1046"));
            ddlState.Items.Add(new ListItem("Virginia", "1047"));
            ddlState.Items.Add(new ListItem("Washington", "1048"));
            ddlState.Items.Add(new ListItem("West Virginia", "1049"));
            ddlState.Items.Add(new ListItem("Wisconsin", "1050"));
            ddlState.Items.Add(new ListItem("Wyoming", "1051"));
            ddlState.Items.Add(new ListItem("US Virgin Islands", "1052"));
            ddlState.SelectedValue = "1047";
            #endregion

            #region Countries
            ddlCountry.Items.Add(new ListItem("United States", "2397"));
            ddlCountry.Items.Add(new ListItem("Afghanistan", "2199"));
            ddlCountry.Items.Add(new ListItem("Aguascalientes (Mexico)", "2200"));
            ddlCountry.Items.Add(new ListItem("Albania", "2196"));
            ddlCountry.Items.Add(new ListItem("Alberta (Canada)", "1053"));
            ddlCountry.Items.Add(new ListItem("Algeria", "2203"));
            ddlCountry.Items.Add(new ListItem("All Others", "2411"));
            ddlCountry.Items.Add(new ListItem("Andorra", "2198"));
            ddlCountry.Items.Add(new ListItem("Angola", "2204"));
            ddlCountry.Items.Add(new ListItem("Anguilla (UK)", "6066"));
            ddlCountry.Items.Add(new ListItem("Antigua and Barbuda", "2201"));
            ddlCountry.Items.Add(new ListItem("Argentina", "2207"));
            ddlCountry.Items.Add(new ListItem("Armenia", "6068"));
            ddlCountry.Items.Add(new ListItem("Aruba (Neth Antilles)", "6067"));
            ddlCountry.Items.Add(new ListItem("Ashmore and Cartier Islands (Australia)", "6225"));
            ddlCountry.Items.Add(new ListItem("Australia", "2206"));
            ddlCountry.Items.Add(new ListItem("Austria", "2208"));
            ddlCountry.Items.Add(new ListItem("Azerbaijan", "6069"));
            ddlCountry.Items.Add(new ListItem("Azores Islands", "6197"));
            ddlCountry.Items.Add(new ListItem("Bahamas, The", "2212"));
            ddlCountry.Items.Add(new ListItem("Bahrain", "2213"));
            ddlCountry.Items.Add(new ListItem("Baja California (Mexico)", "2210"));
            ddlCountry.Items.Add(new ListItem("Baja California Sur (Mexico)", "2217"));
            ddlCountry.Items.Add(new ListItem("Balearic Islands", "6200"));
            ddlCountry.Items.Add(new ListItem("Bangladesh", "6070"));
            ddlCountry.Items.Add(new ListItem("Barbados", "2211"));
            ddlCountry.Items.Add(new ListItem("Bassas Da India (France)", "6201"));
            ddlCountry.Items.Add(new ListItem("Belarus, Republic of", "6073"));
            ddlCountry.Items.Add(new ListItem("Belgium", "2214"));
            ddlCountry.Items.Add(new ListItem("Belize( frmr. British Honduras)", "2215"));
            ddlCountry.Items.Add(new ListItem("Benin (fmr. Dahomey)", "6071"));
            ddlCountry.Items.Add(new ListItem("Bermuda", "2218"));
            ddlCountry.Items.Add(new ListItem("Bhutan", "2219"));
            ddlCountry.Items.Add(new ListItem("Bolivia", "2223"));
            ddlCountry.Items.Add(new ListItem("Bosnia and Herzegovina", "6034"));
            ddlCountry.Items.Add(new ListItem("Botswana", "2221"));
            ddlCountry.Items.Add(new ListItem("Bouvet Island (Norway)", "6202"));
            ddlCountry.Items.Add(new ListItem("Brazil", "2225"));
            ddlCountry.Items.Add(new ListItem("British Columbia (Canada)", "1054"));
            ddlCountry.Items.Add(new ListItem("British Indian Ocean Territory (UK)", "6203"));
            ddlCountry.Items.Add(new ListItem("British Virgin Islands", "6072"));
            ddlCountry.Items.Add(new ListItem("Brunei", "2224"));
            ddlCountry.Items.Add(new ListItem("Bulgaria", "2222"));
            ddlCountry.Items.Add(new ListItem("Burkina Faso (fmr. Upper Volta)", "6175"));
            ddlCountry.Items.Add(new ListItem("Burma", "2220"));
            ddlCountry.Items.Add(new ListItem("Burundi", "2216"));
            ddlCountry.Items.Add(new ListItem("Cambodia", "2234"));
            ddlCountry.Items.Add(new ListItem("Cameroon", "2237"));
            ddlCountry.Items.Add(new ListItem("Campeche (Mexico)", "2229"));
            ddlCountry.Items.Add(new ListItem("Canada", "2228"));
            ddlCountry.Items.Add(new ListItem("Canary Islands", "6204"));
            ddlCountry.Items.Add(new ListItem("Cape Verde", "2244"));
            ddlCountry.Items.Add(new ListItem("Caroline Islands", "2231"));
            ddlCountry.Items.Add(new ListItem("Cayman Islands (UK)", "2239"));
            ddlCountry.Items.Add(new ListItem("Central African Republic", "2245"));
            ddlCountry.Items.Add(new ListItem("Chad", "2230"));
            ddlCountry.Items.Add(new ListItem("Chiapas (Mexico)", "2233"));
            ddlCountry.Items.Add(new ListItem("Chihuahua (Mexico)", "2232"));
            ddlCountry.Items.Add(new ListItem("Chile, Republic of", "2240"));
            ddlCountry.Items.Add(new ListItem("China, Peoples Republic of", "2238"));
            ddlCountry.Items.Add(new ListItem("Christmas Island (Australia)", "6074"));
            ddlCountry.Items.Add(new ListItem("Clipperton Island", "6205"));
            ddlCountry.Items.Add(new ListItem("Coahuila (Mexico)", "2243"));
            ddlCountry.Items.Add(new ListItem("Cocos Islands (Australia)", "6075"));
            ddlCountry.Items.Add(new ListItem("Colima (Mexico)", "2236"));
            ddlCountry.Items.Add(new ListItem("Colombia", "2226"));
            ddlCountry.Items.Add(new ListItem("Comoros, Federal Islamic Republic of", "6076"));
            ddlCountry.Items.Add(new ListItem("Congo", "2246"));
            ddlCountry.Items.Add(new ListItem("Cook Islands", "6077"));
            ddlCountry.Items.Add(new ListItem("Coral Sea Islands (Australia)", "6220"));
            ddlCountry.Items.Add(new ListItem("Costa Rica, Republic of", "2241"));
            ddlCountry.Items.Add(new ListItem("Côte d`Ivoire (fmr. Ivory Coast)", "2294"));
            ddlCountry.Items.Add(new ListItem("Croatia", "6078"));
            ddlCountry.Items.Add(new ListItem("Cuba, Republic of", "2227"));
            ddlCountry.Items.Add(new ListItem("Cyprus", "2242"));
            ddlCountry.Items.Add(new ListItem("Czec Republic", "6079"));
            ddlCountry.Items.Add(new ListItem("Czechoslovakia", "2235"));
            ddlCountry.Items.Add(new ListItem("Dahomey (now Benin)", "2250"));
            ddlCountry.Items.Add(new ListItem("Democratic Republic Congo", "6080"));
            ddlCountry.Items.Add(new ListItem("Denmark", "2251"));
            ddlCountry.Items.Add(new ListItem("Distrito Federal (Mexico, D.F.", "2249"));
            ddlCountry.Items.Add(new ListItem("Djibouti, Republic of", "6081"));
            ddlCountry.Items.Add(new ListItem("Dominica", "2252"));
            ddlCountry.Items.Add(new ListItem("Dominican Republic", "2254"));
            ddlCountry.Items.Add(new ListItem("Durango (Mexico)", "2253"));
            ddlCountry.Items.Add(new ListItem("East Germany", "2258"));
            ddlCountry.Items.Add(new ListItem("Ecuador", "2261"));
            ddlCountry.Items.Add(new ListItem("Egypt", "2262"));
            ddlCountry.Items.Add(new ListItem("El Salvador", "2257"));
            ddlCountry.Items.Add(new ListItem("England", "2259"));
            ddlCountry.Items.Add(new ListItem("Equatorial Guinea", "2256"));
            ddlCountry.Items.Add(new ListItem("Eritrea", "6082"));
            ddlCountry.Items.Add(new ListItem("Estonia", "2260"));
            ddlCountry.Items.Add(new ListItem("Ethiopia", "2255"));
            ddlCountry.Items.Add(new ListItem("Europa Island (France)", "6206"));
            ddlCountry.Items.Add(new ListItem("Falkland Islands", "6083"));
            ddlCountry.Items.Add(new ListItem("Faroe Islands", "6207"));
            ddlCountry.Items.Add(new ListItem("Fiji Islands", "2264"));
            ddlCountry.Items.Add(new ListItem("Finland", "2263"));
            ddlCountry.Items.Add(new ListItem("France", "2265"));
            ddlCountry.Items.Add(new ListItem("French Guiana", "6084"));
            ddlCountry.Items.Add(new ListItem("French Polynesia (France)", "6085"));
            ddlCountry.Items.Add(new ListItem("French Southern & Antarctic Lands", "6086"));
            ddlCountry.Items.Add(new ListItem("Gabon", "2267"));
            ddlCountry.Items.Add(new ListItem("Gambia", "2273"));
            ddlCountry.Items.Add(new ListItem("Gaza", "6226"));
            ddlCountry.Items.Add(new ListItem("Georgia (fmr. Gruzinskaya)", "6087"));
            ddlCountry.Items.Add(new ListItem("Germany", "2269"));
            ddlCountry.Items.Add(new ListItem("Ghana", "2270"));
            ddlCountry.Items.Add(new ListItem("Gibraltar", "6088"));
            ddlCountry.Items.Add(new ListItem("Glorioso Islands (France)", "6208"));
            ddlCountry.Items.Add(new ListItem("Greece", "2268"));
            ddlCountry.Items.Add(new ListItem("Greenland", "2275"));
            ddlCountry.Items.Add(new ListItem("Grenada", "2272"));
            ddlCountry.Items.Add(new ListItem("Guadeloupe, Department of", "2276"));
            ddlCountry.Items.Add(new ListItem("Guam", "2274"));
            ddlCountry.Items.Add(new ListItem("Guanajuato (Mexico)", "2279"));
            ddlCountry.Items.Add(new ListItem("Guatemala", "2278"));
            ddlCountry.Items.Add(new ListItem("Guernsey, Bailiwick of", "6198"));
            ddlCountry.Items.Add(new ListItem("Guerrero (Mexico)", "2277"));
            ddlCountry.Items.Add(new ListItem("Guiana/Guyana", "2280"));
            ddlCountry.Items.Add(new ListItem("Guinea", "2271"));
            ddlCountry.Items.Add(new ListItem("Guinea Bissau", "6089"));
            ddlCountry.Items.Add(new ListItem("Haiti", "2284"));
            ddlCountry.Items.Add(new ListItem("Heard Island and McDonald Islands, Territory of", "6227"));
            ddlCountry.Items.Add(new ListItem("Hidalgo (Mexico)", "2283"));
            ddlCountry.Items.Add(new ListItem("Honduras", "2281"));
            ddlCountry.Items.Add(new ListItem("Hong Kong", "2282"));
            ddlCountry.Items.Add(new ListItem("Hungary", "2285"));
            ddlCountry.Items.Add(new ListItem("Iceland", "2286"));
            ddlCountry.Items.Add(new ListItem("India", "2288"));
            ddlCountry.Items.Add(new ListItem("Indonesia", "2289"));
            ddlCountry.Items.Add(new ListItem("Iran", "2291"));
            ddlCountry.Items.Add(new ListItem("Iraq", "2290"));
            ddlCountry.Items.Add(new ListItem("Ireland", "2287"));
            ddlCountry.Items.Add(new ListItem("Isle of Man (UK)", "6209"));
            ddlCountry.Items.Add(new ListItem("Israel", "2292"));
            ddlCountry.Items.Add(new ListItem("Italy", "2293"));
            ddlCountry.Items.Add(new ListItem("Jalisco (Mexico)", "2296"));
            ddlCountry.Items.Add(new ListItem("Jamaica", "2297"));
            ddlCountry.Items.Add(new ListItem("Jan Mayen (Norway)", "6210"));
            ddlCountry.Items.Add(new ListItem("Japan", "2295"));
            ddlCountry.Items.Add(new ListItem("Jersey, Bailiwick of", "6199"));
            ddlCountry.Items.Add(new ListItem("Jordan", "2298"));
            ddlCountry.Items.Add(new ListItem("Juan de Nova Island", "6211"));
            ddlCountry.Items.Add(new ListItem("Kazakhstan", "6091"));
            ddlCountry.Items.Add(new ListItem("Kenya", "2299"));
            ddlCountry.Items.Add(new ListItem("Kiribati", "6092"));
            ddlCountry.Items.Add(new ListItem("Korea", "2300"));
            ddlCountry.Items.Add(new ListItem("Kosovo", "6228"));
            ddlCountry.Items.Add(new ListItem("Kuwait", "2301"));
            ddlCountry.Items.Add(new ListItem("Kyrgyzstan", "6212"));
            ddlCountry.Items.Add(new ListItem("Laos", "2307"));
            ddlCountry.Items.Add(new ListItem("Latvia", "2308"));
            ddlCountry.Items.Add(new ListItem("Lebanon", "2306"));
            ddlCountry.Items.Add(new ListItem("Lesotho", "2303"));
            ddlCountry.Items.Add(new ListItem("Liberia", "2302"));
            ddlCountry.Items.Add(new ListItem("Libya", "2310"));
            ddlCountry.Items.Add(new ListItem("Liechtenstein", "2305"));
            ddlCountry.Items.Add(new ListItem("Lithuania", "2304"));
            ddlCountry.Items.Add(new ListItem("Luxembourg", "2309"));
            ddlCountry.Items.Add(new ListItem("Macau", "6094"));
            ddlCountry.Items.Add(new ListItem("Macedonia", "6093"));
            ddlCountry.Items.Add(new ListItem("Madeira Islands", "6213"));
            ddlCountry.Items.Add(new ListItem("Malagasy Republic (inc. Madagascar)", "2320"));
            ddlCountry.Items.Add(new ListItem("Malawi", "2312"));
            ddlCountry.Items.Add(new ListItem("Malaysia", "2328"));
            ddlCountry.Items.Add(new ListItem("Maldives", "2324"));
            ddlCountry.Items.Add(new ListItem("Mali", "2317"));
            ddlCountry.Items.Add(new ListItem("Malta", "2327"));
            ddlCountry.Items.Add(new ListItem("Manahiki Island", "6214"));
            ddlCountry.Items.Add(new ListItem("Manitoba (Canada)", "1055"));
            ddlCountry.Items.Add(new ListItem("Marianas Islands", "2316"));
            ddlCountry.Items.Add(new ListItem("Marshall Islands", "2314"));
            ddlCountry.Items.Add(new ListItem("Martinique", "2413"));
            ddlCountry.Items.Add(new ListItem("Mauritania", "2323"));
            ddlCountry.Items.Add(new ListItem("Mauritius", "6095"));
            ddlCountry.Items.Add(new ListItem("Mayotte", "6177"));
            ddlCountry.Items.Add(new ListItem("Mexico", "2318"));
            ddlCountry.Items.Add(new ListItem("Mexico, State of (Mexico)", "2326"));
            ddlCountry.Items.Add(new ListItem("Michoacan (Mexico)", "2311"));
            ddlCountry.Items.Add(new ListItem("Micronesia, Federated States", "6096"));
            ddlCountry.Items.Add(new ListItem("Midway Islands", "2325"));
            ddlCountry.Items.Add(new ListItem("Moldova", "6215"));
            ddlCountry.Items.Add(new ListItem("Monaco", "2315"));
            ddlCountry.Items.Add(new ListItem("Mongolia", "2313"));
            ddlCountry.Items.Add(new ListItem("Montenegro", "2202"));
            ddlCountry.Items.Add(new ListItem("Montserrat (UK)", "6097"));
            ddlCountry.Items.Add(new ListItem("Morelos (Mexico)", "2322"));
            ddlCountry.Items.Add(new ListItem("Morocco", "2321"));
            ddlCountry.Items.Add(new ListItem("Mozambique", "6098"));
            ddlCountry.Items.Add(new ListItem("Nauru", "2337"));
            ddlCountry.Items.Add(new ListItem("Nayarit (Mexico)", "2329"));
            ddlCountry.Items.Add(new ListItem("Nepal", "2335"));
            ddlCountry.Items.Add(new ListItem("Netherlands", "2338"));
            ddlCountry.Items.Add(new ListItem("Netherlands Antilles", "2341"));
            ddlCountry.Items.Add(new ListItem("Neuvo Leon (Mexico)", "2332"));
            ddlCountry.Items.Add(new ListItem("New Brunswick (Canada)", "1056"));
            ddlCountry.Items.Add(new ListItem("New Caledonia", "2336"));
            ddlCountry.Items.Add(new ListItem("New Guinea", "2334"));
            ddlCountry.Items.Add(new ListItem("New Zealand", "2342"));
            ddlCountry.Items.Add(new ListItem("Newfoundland and Labrador (Canada)", "1057"));
            ddlCountry.Items.Add(new ListItem("Nicaragua", "2339"));
            ddlCountry.Items.Add(new ListItem("Niger", "2333"));
            ddlCountry.Items.Add(new ListItem("Nigeria", "2330"));
            ddlCountry.Items.Add(new ListItem("Niue", "6099"));
            ddlCountry.Items.Add(new ListItem("Norfolk Island, Territory of", "6221"));
            ddlCountry.Items.Add(new ListItem("North Korea", "6100"));
            ddlCountry.Items.Add(new ListItem("North Vietnam", "6260"));
            ddlCountry.Items.Add(new ListItem("Northern Ireland", "2331"));
            ddlCountry.Items.Add(new ListItem("Northwest Territories (Canada)", "1058"));
            ddlCountry.Items.Add(new ListItem("Norway", "2340"));
            ddlCountry.Items.Add(new ListItem("Nova Scotia (Canada)", "1059"));
            ddlCountry.Items.Add(new ListItem("Nunavut", "1060"));
            ddlCountry.Items.Add(new ListItem("Oaxaca (Mexico)", "2343"));
            ddlCountry.Items.Add(new ListItem("Okinawa", "6216"));
            ddlCountry.Items.Add(new ListItem("Oman", "6101"));
            ddlCountry.Items.Add(new ListItem("Ontario (Canada)", "1061"));
            ddlCountry.Items.Add(new ListItem("Pakistan", "2347"));
            ddlCountry.Items.Add(new ListItem("Palau", "6102"));
            ddlCountry.Items.Add(new ListItem("Panama", "2348"));
            ddlCountry.Items.Add(new ListItem("Panama Canal Zone (US)", "2248"));
            ddlCountry.Items.Add(new ListItem("Paracel Islands", "6229"));
            ddlCountry.Items.Add(new ListItem("Paraguay", "2352"));
            ddlCountry.Items.Add(new ListItem("Peru", "2351"));
            ddlCountry.Items.Add(new ListItem("Philippines", "2346"));
            ddlCountry.Items.Add(new ListItem("Pitcairn Island", "2345"));
            ddlCountry.Items.Add(new ListItem("Poland", "2349"));
            ddlCountry.Items.Add(new ListItem("Portugal", "2350"));
            ddlCountry.Items.Add(new ListItem("Prince Edward Island (Canada)", "1062"));
            ddlCountry.Items.Add(new ListItem("Puebla (Mexico)", "2344"));
            ddlCountry.Items.Add(new ListItem("Qatar", "2353"));
            ddlCountry.Items.Add(new ListItem("Quebec (Canada)", "1063"));
            ddlCountry.Items.Add(new ListItem("Queretaro (Mexico)", "2355"));
            ddlCountry.Items.Add(new ListItem("Quintana Roo (Mexico)", "2354"));
            ddlCountry.Items.Add(new ListItem("Reunion", "2356"));
            ddlCountry.Items.Add(new ListItem("Rhodesia", "2357"));
            ddlCountry.Items.Add(new ListItem("Rumania", "2358"));
            ddlCountry.Items.Add(new ListItem("Russia", "6255"));
            ddlCountry.Items.Add(new ListItem("Russian Federation", "6104"));
            ddlCountry.Items.Add(new ListItem("Rwanda", "2359"));
            ddlCountry.Items.Add(new ListItem("San Luis Potosi (Mexico)", "2369"));
            ddlCountry.Items.Add(new ListItem("San Marino", "2365"));
            ddlCountry.Items.Add(new ListItem("Sao Tome and Principe", "6107"));
            ddlCountry.Items.Add(new ListItem("Saskatchewan (Canada)", "1064"));
            ddlCountry.Items.Add(new ListItem("Saudi Arabia", "2361"));
            ddlCountry.Items.Add(new ListItem("Scotland", "2375"));
            ddlCountry.Items.Add(new ListItem("Senegal", "2364"));
            ddlCountry.Items.Add(new ListItem("Serbia", "6103"));
            ddlCountry.Items.Add(new ListItem("Seychelles", "2362"));
            ddlCountry.Items.Add(new ListItem("Sierra Leone", "2360"));
            ddlCountry.Items.Add(new ListItem("Sikkim", "2368"));
            ddlCountry.Items.Add(new ListItem("Sinaloa (Mexico)", "2366"));
            ddlCountry.Items.Add(new ListItem("Singapore", "2374"));
            ddlCountry.Items.Add(new ListItem("Slovak Republic/Slovikia", "6108"));
            ddlCountry.Items.Add(new ListItem("Slovenia", "6109"));
            ddlCountry.Items.Add(new ListItem("Solomon Islands", "6110"));
            ddlCountry.Items.Add(new ListItem("Somalia", "2370"));
            ddlCountry.Items.Add(new ListItem("Sonora (Mexico)", "2371"));
            ddlCountry.Items.Add(new ListItem("South Africa", "2363"));
            ddlCountry.Items.Add(new ListItem("South Georgia and the South Sandwich Islands", "6219"));
            ddlCountry.Items.Add(new ListItem("South Korea", "6105"));
            ddlCountry.Items.Add(new ListItem("South Vietnam", "6261"));
            ddlCountry.Items.Add(new ListItem("Southern Yemen", "2376"));
            ddlCountry.Items.Add(new ListItem("South-West Africa (fmr. Namibia)", "2367"));
            ddlCountry.Items.Add(new ListItem("Soviet Union", "2380"));
            ddlCountry.Items.Add(new ListItem("Spain", "2372"));
            ddlCountry.Items.Add(new ListItem("Spratly Islands", "6231"));
            ddlCountry.Items.Add(new ListItem("Sri Lanka (fmr. Ceylon)", "2247"));
            ddlCountry.Items.Add(new ListItem("St. Kitts/Nevis", "6112"));
            ddlCountry.Items.Add(new ListItem("St. Lucia", "6113"));
            ddlCountry.Items.Add(new ListItem("St. Pierre and Miquelon", "6114"));
            ddlCountry.Items.Add(new ListItem("St. Vincent and the Grenadines", "6115"));
            ddlCountry.Items.Add(new ListItem("St.Helena", "6111"));
            ddlCountry.Items.Add(new ListItem("Sudan", "2377"));
            ddlCountry.Items.Add(new ListItem("Surinam", "2414"));
            ddlCountry.Items.Add(new ListItem("Svalbard", "2378"));
            ddlCountry.Items.Add(new ListItem("Swaziland", "2379"));
            ddlCountry.Items.Add(new ListItem("Sweden", "2373"));
            ddlCountry.Items.Add(new ListItem("Switzerland", "2382"));
            ddlCountry.Items.Add(new ListItem("Syria", "2381"));
            ddlCountry.Items.Add(new ListItem("Tabasco (Mexico)", "2384"));
            ddlCountry.Items.Add(new ListItem("Taiwan, Republic of China", "6116"));
            ddlCountry.Items.Add(new ListItem("Tajikistan", "6117"));
            ddlCountry.Items.Add(new ListItem("Tamaulipas (Mexico)", "2383"));
            ddlCountry.Items.Add(new ListItem("Tanzania", "2394"));
            ddlCountry.Items.Add(new ListItem("Thailand", "2387"));
            ddlCountry.Items.Add(new ListItem("Tlaxcala (Mexico)", "2388"));
            ddlCountry.Items.Add(new ListItem("Togo", "2390"));
            ddlCountry.Items.Add(new ListItem("Tokelau", "6222"));
            ddlCountry.Items.Add(new ListItem("Tonga", "2386"));
            ddlCountry.Items.Add(new ListItem("Tongareva", "6223"));
            ddlCountry.Items.Add(new ListItem("Trinidad And Tobago", "2391"));
            ddlCountry.Items.Add(new ListItem("Tromelin Island", "6224"));
            ddlCountry.Items.Add(new ListItem("Trucial States", "2385"));
            ddlCountry.Items.Add(new ListItem("Trust Territory of the Pacific Islands", "6230"));
            ddlCountry.Items.Add(new ListItem("Tuamotu Archipelago", "6257"));
            ddlCountry.Items.Add(new ListItem("Tunisia", "2392"));
            ddlCountry.Items.Add(new ListItem("Turkey", "2393"));
            ddlCountry.Items.Add(new ListItem("Turkmenistan", "6118"));
            ddlCountry.Items.Add(new ListItem("Turks and Caicos Islands", "6119"));
            ddlCountry.Items.Add(new ListItem("Tuvalu", "6120"));
            ddlCountry.Items.Add(new ListItem("Uganda", "2396"));
            ddlCountry.Items.Add(new ListItem("Ukraine", "6036"));
            ddlCountry.Items.Add(new ListItem("United Arab Republic", "2395"));
            ddlCountry.Items.Add(new ListItem("United Kingdom", "6178"));
            ddlCountry.Items.Add(new ListItem("Unknown", "6258"));
            ddlCountry.Items.Add(new ListItem("Uruguay", "2399"));
            ddlCountry.Items.Add(new ListItem("Uzbekistan", "6121"));
            ddlCountry.Items.Add(new ListItem("Vanuatu, Republic of (fmr. New Hebrides)", "6217"));
            ddlCountry.Items.Add(new ListItem("Vatican City/Holy See", "6090"));
            ddlCountry.Items.Add(new ListItem("Venezuela", "2402"));
            ddlCountry.Items.Add(new ListItem("Veracruz (Mexico)", "2400"));
            ddlCountry.Items.Add(new ListItem("Vietnam", "6259"));
            ddlCountry.Items.Add(new ListItem("Vietnam, Socialist Republic", "2401"));
            ddlCountry.Items.Add(new ListItem("Wake Island", "2404"));
            ddlCountry.Items.Add(new ListItem("Wales", "2405"));
            ddlCountry.Items.Add(new ListItem("Wallis and Futuna Islands", "6122"));
            ddlCountry.Items.Add(new ListItem("West Bank", "6176"));
            ddlCountry.Items.Add(new ListItem("West Germany", "2403"));
            ddlCountry.Items.Add(new ListItem("West Indies", "2406"));
            ddlCountry.Items.Add(new ListItem("Western Sahara", "6123"));
            ddlCountry.Items.Add(new ListItem("Western Samoa", "2407"));
            ddlCountry.Items.Add(new ListItem("Yemen", "2408"));
            ddlCountry.Items.Add(new ListItem("Yemen Arab Republic", "6266"));
            ddlCountry.Items.Add(new ListItem("Yucatan (Mexico)", "2410"));
            ddlCountry.Items.Add(new ListItem("Yugoslavia", "2409"));
            ddlCountry.Items.Add(new ListItem("Yukon (Canada)", "1065"));
            ddlCountry.Items.Add(new ListItem("Zacatecas (Mexico)", "2412"));
            ddlCountry.Items.Add(new ListItem("Zaire, Republic of (fmr. Congo)", "6218"));
            ddlCountry.Items.Add(new ListItem("Zambia", "2415"));
            ddlCountry.Items.Add(new ListItem("Zimbabwe", "6138"));
            #endregion

            #region Phone Types
            ddlPhoneType.Items.Add(new ListItem(string.Empty));
            ddlPhoneType.Items.Add(new ListItem("Business", "50"));
            ddlPhoneType.Items.Add(new ListItem("Cell Phone", "51"));
            ddlPhoneType.Items.Add(new ListItem("Fax", "52"));
            ddlPhoneType.Items.Add(new ListItem("Pager", "54"));
            ddlPhoneType.Items.Add(new ListItem("Residential", "53"));
            #endregion

            #region Offender Facilities
            ddlOffenderFacility.Items.Add(new ListItem(string.Empty));
            ddlOffenderFacility.Items.Add(new ListItem("Augusta Correctional Center", "1927"));
            ddlOffenderFacility.Items.Add(new ListItem("Baskerville Correctional Center", "1879"));
            ddlOffenderFacility.Items.Add(new ListItem("Bland Correctional Center", "1912"));
            ddlOffenderFacility.Items.Add(new ListItem("Brunswick Work Center", "2150"));
            ddlOffenderFacility.Items.Add(new ListItem("Buckingham Correctional Center", "1928"));
            ddlOffenderFacility.Items.Add(new ListItem("Caroline Correctional Unit", "1877"));
            ddlOffenderFacility.Items.Add(new ListItem("Central Virginia Correctional Unit", "1888"));
            ddlOffenderFacility.Items.Add(new ListItem("Coffeewood Correctional Center", "1929"));
            ddlOffenderFacility.Items.Add(new ListItem("Cold Springs Correctional Unit", "1885"));
            ddlOffenderFacility.Items.Add(new ListItem("Deep Meadow Correctional Center", "1922"));
            ddlOffenderFacility.Items.Add(new ListItem("Deerfield Correctional Center", "1948"));
            ddlOffenderFacility.Items.Add(new ListItem("Deerfield Mens Work Center", "2153"));
            ddlOffenderFacility.Items.Add(new ListItem("Deerfield Women's Work Center", "2503"));
            ddlOffenderFacility.Items.Add(new ListItem("Detainer Unit", "6136"));
            ddlOffenderFacility.Items.Add(new ListItem("Dillwyn Correctional Center", "1941"));
            ddlOffenderFacility.Items.Add(new ListItem("Fluvanna Correctional Center", "1951"));
            ddlOffenderFacility.Items.Add(new ListItem("Green Rock Correctional Center", "2587"));
            ddlOffenderFacility.Items.Add(new ListItem("Greensville Correctional Center", "1932"));
            ddlOffenderFacility.Items.Add(new ListItem("Greensville Work Center", "1908"));
            ddlOffenderFacility.Items.Add(new ListItem("Halifax Correctional Unit", "1897"));
            ddlOffenderFacility.Items.Add(new ListItem("Haynesville Correctional Center", "1942"));
            ddlOffenderFacility.Items.Add(new ListItem("Haynesville Correctional Unit 17", "1892"));
            ddlOffenderFacility.Items.Add(new ListItem("Indian Creek Correctional Center", "1943"));
            ddlOffenderFacility.Items.Add(new ListItem("Interstate Compact-Facility", "6129"));
            ddlOffenderFacility.Items.Add(new ListItem("James River Work Center", "1909"));
            ddlOffenderFacility.Items.Add(new ListItem("Keen Mountain Correctional Center", "1940"));
            ddlOffenderFacility.Items.Add(new ListItem("Lawrenceville Correctional Center", "1950"));
            ddlOffenderFacility.Items.Add(new ListItem("Lunenburg Correctional Center", "1931"));
            ddlOffenderFacility.Items.Add(new ListItem("Marion Correctional Treatment Center", "1961"));
            ddlOffenderFacility.Items.Add(new ListItem("Mecklenburg Correctional Center", "1923"));
            ddlOffenderFacility.Items.Add(new ListItem("Nottoway Correctional Center", "1930"));
            ddlOffenderFacility.Items.Add(new ListItem("Nottoway Work Center", "1910"));
            ddlOffenderFacility.Items.Add(new ListItem("Patrick Henry Correctional Unit", "1902"));
            ddlOffenderFacility.Items.Add(new ListItem("Pocahontas State Correctional Center", "2586"));
            ddlOffenderFacility.Items.Add(new ListItem("Powhatan County Jail", "2100"));
            ddlOffenderFacility.Items.Add(new ListItem("Powhatan Reception And Classification Ctr", "2181"));
            ddlOffenderFacility.Items.Add(new ListItem("Red Onion State Prison", "1945"));
            ddlOffenderFacility.Items.Add(new ListItem("River North Correctional Center", "6264"));
            ddlOffenderFacility.Items.Add(new ListItem("Rustburg Correctional Unit", "1884"));
            ddlOffenderFacility.Items.Add(new ListItem("St. Brides Correctional Center", "1947"));
            ddlOffenderFacility.Items.Add(new ListItem("Sussex I State Prison", "1938"));
            ddlOffenderFacility.Items.Add(new ListItem("Sussex II State Prison", "1939"));
            ddlOffenderFacility.Items.Add(new ListItem("Virginia Correctional Center For Women", "1916"));
            ddlOffenderFacility.Items.Add(new ListItem("Wallens Ridge State Prison", "1946"));
            ddlOffenderFacility.Items.Add(new ListItem("Wise Correctional Unit", "1893"));
            #endregion

            #region Relationship Qualifiers
            ddlRelationshipQualifier.Items.Add(new ListItem(string.Empty));
            ddlRelationshipQualifier.Items.Add(new ListItem("Adoptive", "5"));
            ddlRelationshipQualifier.Items.Add(new ListItem("Ex", "1"));
            ddlRelationshipQualifier.Items.Add(new ListItem("Foster", "3"));
            ddlRelationshipQualifier.Items.Add(new ListItem("Great", "7"));
            ddlRelationshipQualifier.Items.Add(new ListItem("Half", "2"));
            ddlRelationshipQualifier.Items.Add(new ListItem("In-Law", "6"));
            ddlRelationshipQualifier.Items.Add(new ListItem("Step", "4"));
            #endregion

            #region Relationship Types
            ddlRelationshipType.Items.Add(new ListItem(string.Empty));
            ddlRelationshipType.Items.Add(new ListItem("Aunt", "1"));
            ddlRelationshipType.Items.Add(new ListItem("Boy Friend", "2"));
            ddlRelationshipType.Items.Add(new ListItem("Brother", "3"));
            ddlRelationshipType.Items.Add(new ListItem("Cousin", "10"));
            ddlRelationshipType.Items.Add(new ListItem("Daughter", "11"));
            ddlRelationshipType.Items.Add(new ListItem("Father", "6"));
            ddlRelationshipType.Items.Add(new ListItem("Fiancé(e)", "17"));
            ddlRelationshipType.Items.Add(new ListItem("Girl Friend", "5"));
            ddlRelationshipType.Items.Add(new ListItem("Granddaughter", "18"));
            ddlRelationshipType.Items.Add(new ListItem("Grandfather", "7"));
            ddlRelationshipType.Items.Add(new ListItem("Grandmother", "8"));
            ddlRelationshipType.Items.Add(new ListItem("Grandson", "19"));
            ddlRelationshipType.Items.Add(new ListItem("Mother", "14"));
            ddlRelationshipType.Items.Add(new ListItem("Nephew", "21"));
            ddlRelationshipType.Items.Add(new ListItem("Niece", "20"));
            ddlRelationshipType.Items.Add(new ListItem("Sister", "12"));
            ddlRelationshipType.Items.Add(new ListItem("Son", "13"));
            ddlRelationshipType.Items.Add(new ListItem("Spouse", "9"));
            ddlRelationshipType.Items.Add(new ListItem("Uncle", "16"));
            #endregion

            #region Vehicle Years
            ddlVehicleYear.Items.Add(new ListItem(string.Empty));
            for (int k = DateTime.Now.Year + 1; k >= 1900; k--)
                ddlVehicleYear.Items.Add(new ListItem(k.ToString(CultureInfo.CurrentCulture)));
            #endregion

            #region P&P Districts
            ddlProbationParoleDistrict.Items.Add(new ListItem(string.Empty));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Abingdon", "1630"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Accomac", "1617"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Alexandria", "1649"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Arlington", "1623"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Ashland", "1654"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Bedford", "1633"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Charlottesville", "1622"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Chesapeake", "1644"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Chesterfield", "1640"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Culpeper", "1639"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Danville", "1627"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Emporia", "1651"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Fairfax", "1642"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Farmville", "1637"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Fincastle", "1653"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Franklin", "1655"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Fredericksburg", "1634"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Gloucester", "1618"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Hampton", "1643"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Harrisonburg", "1652"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Henrico", "1645"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Leesburg", "1638"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Lynchburg", "1626"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Manassas", "1648"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Martinsville", "1635"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Newport News", "1632"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Norfolk", "1615"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Norton", "1631"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Petersburg", "1620"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Portsmouth", "1616"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Radford", "1641"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Richmond", "1614"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Roanoke", "1628"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Rocky Mount", "1650"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("South Boston", "1621"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Staunton", "1625"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Suffolk", "1619"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Tazewell", "1656"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Virginia Beach", "1636"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Warsaw", "1646"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Williamsburg", "1647"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Winchester", "1624"));
            ddlProbationParoleDistrict.Items.Add(new ListItem("Wytheville", "1629"));
            #endregion

            #region Minor ID Types
            ddlMinorIdType.Items.Add(new ListItem(string.Empty));
            ddlMinorIdType.Items.Add(new ListItem("Driver's License #", "1"));
            ddlMinorIdType.Items.Add(new ListItem("Military ID", "12"));
            ddlMinorIdType.Items.Add(new ListItem("Passport", "11"));
            ddlMinorIdType.Items.Add(new ListItem("State Issued Photo ID", "13"));
            #endregion

            #region Minor Races
            ddlMinorRace.Items.Add(new ListItem(string.Empty));
            ddlMinorRace.Items.Add(new ListItem("American Indian or Alaskan Native", "15"));
            ddlMinorRace.Items.Add(new ListItem("Asian or Pacific Islander", "16"));
            ddlMinorRace.Items.Add(new ListItem("Black", "17"));
            ddlMinorRace.Items.Add(new ListItem("Unknown", "19"));
            ddlMinorRace.Items.Add(new ListItem("White", "18"));
            #endregion

            #region Minor Ethnic Origins
            ddlMinorEthnicOrigin.Items.Add(new ListItem(string.Empty));
            ddlMinorEthnicOrigin.Items.Add(new ListItem("Hispanic", "1"));
            ddlMinorEthnicOrigin.Items.Add(new ListItem("Other", "0"));
            #endregion

            #region Minor Genders
            ddlMinorGender.Items.Add(new ListItem(string.Empty));
            ddlMinorGender.Items.Add(new ListItem("Female", "13"));
            ddlMinorGender.Items.Add(new ListItem("Male", "12"));
            #endregion

            #region Minor Birth Months
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem(string.Empty));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("January", "01"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("February", "02"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("March", "03"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("April", "04"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("May", "05"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("June", "06"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("July", "07"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("August", "08"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("September", "09"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("October", "10"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("November", "11"));
            ddlMinorMonthDateOfBirth.Items.Add(new ListItem("December", "12"));
            #endregion

            #region Minor Birth Days
            ddlMinorDayDateOfBirth.Items.Add(new ListItem(string.Empty));
            for (int i = 1; i <= 31; i++)
                ddlMinorDayDateOfBirth.Items.Add(new ListItem(i.ToString(CultureInfo.CurrentCulture).PadLeft(2, '0')));
            #endregion

            #region Minor Birth Years
            ddlMinorYearDateOfBirth.Items.Add(new ListItem(string.Empty));
            for (int j = DateTime.Now.Year; j >= DateTime.Now.Year - 115; j--)
                ddlMinorYearDateOfBirth.Items.Add(new ListItem(j.ToString(CultureInfo.CurrentCulture)));
            #endregion

            #region Minor Relationship Qualifiers
            ddlMinorRelationshipQualifier.Items.Add(new ListItem(string.Empty));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("Adoptive", "5"));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("Ex", "1"));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("Foster", "3"));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("Great", "7"));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("Half", "2"));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("In-Law", "6"));
            ddlMinorRelationshipQualifier.Items.Add(new ListItem("Step", "4"));
            #endregion

            #region Minor Relationship Types
            ddlMinorRelationshipType.Items.Add(new ListItem(string.Empty));
            ddlMinorRelationshipType.Items.Add(new ListItem("Aunt", "1"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Boy Friend", "2"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Brother", "3"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Cousin", "10"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Daughter", "11"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Father", "6"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Fiancé(e)", "17"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Girl Friend", "5"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Granddaughter", "18"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Grandfather", "7"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Grandmother", "8"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Grandson", "19"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Mother", "14"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Nephew", "21"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Niece", "20"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Sister", "12"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Son", "13"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Spouse", "9"));
            ddlMinorRelationshipType.Items.Add(new ListItem("Uncle", "16"));
            #endregion
        }

        private void GenerateOffenderButtons()
        {
            pnlOffenderList.Controls.Clear();

            if (Offenders != null)
            {
                foreach (var offender in Offenders)
                {
                    var litOffender = new Literal { Text = offender.Name + " " };
                    pnlOffenderList.Controls.Add(litOffender);

                    var btnEditOffender = new Button { ID = "btnEditOffender" + offender.Id, Text = "Edit" };
                    btnEditOffender.Click += btnEditOffender_Click;
                    btnEditOffender.CommandArgument = offender.Id.ToString(CultureInfo.InvariantCulture);
                    pnlOffenderList.Controls.Add(btnEditOffender);

                    var litSpace = new Literal { Text = "&nbsp;" };
                    pnlOffenderList.Controls.Add(litSpace);

                    var btnDeleteOffender = new Button { ID = "btnDeleteOffender" + offender.Id, Text = "Delete" };
                    btnDeleteOffender.Click += btnDeleteOffender_Click;
                    btnDeleteOffender.CommandArgument = offender.Id.ToString(CultureInfo.InvariantCulture);
                    pnlOffenderList.Controls.Add(btnDeleteOffender);

                    var litBr = new Literal { Text = "<br />" };
                    pnlOffenderList.Controls.Add(litBr);
                }
            }
        }

        private void GenerateMinorButtons(int? offenderId)
        {
            pnlMinorList.Controls.Clear();

            if (MinorVisitors != null)
            {
                var minorsByOffenderId = offenderId.HasValue
                    ? MinorVisitors.Where(x => x.OffenderId == offenderId).ToList()
                    : MinorVisitors.Where(x => x.OffenderId == null).ToList();

                foreach (var minor in minorsByOffenderId)
                {
                    var litMinor = new Literal { Text = minor.FirstName + " " + minor.LastName + " " };
                    pnlMinorList.Controls.Add(litMinor);

                    var btnEditMinor = new Button { ID = "btnEditMinor" + minor.Id, Text = "Edit" };
                    btnEditMinor.Click += btnEditMinor_Click;
                    btnEditMinor.CommandArgument = minor.Id.ToString(CultureInfo.InvariantCulture);
                    pnlMinorList.Controls.Add(btnEditMinor);

                    var litSpace = new Literal { Text = "&nbsp;" };
                    pnlMinorList.Controls.Add(litSpace);

                    var btnDeleteMinor = new Button { ID = "btnDeleteMinor" + minor.Id, Text = "Delete" };
                    btnDeleteMinor.Click += btnDeleteMinor_Click;
                    btnDeleteMinor.CommandArgument = minor.Id.ToString(CultureInfo.InvariantCulture);
                    pnlMinorList.Controls.Add(btnDeleteMinor);

                    var litBr = new Literal { Text = "<br />" };
                    pnlMinorList.Controls.Add(litBr);
                }
            }
        }

        private void ResetOffenderFields()
        {
            hdnOffenderId.Value = string.Empty;
            txtOffenderName.Text = string.Empty;
            txtOffenderNumber.Text = string.Empty;
            ddlOffenderFacility.SelectedIndex = 0;
            rblContactType.Items[0].Selected = false;
            rblContactType.Items[1].Selected = false;
            ddlRelationshipQualifier.SelectedIndex = 0;
            ddlRelationshipType.SelectedIndex = 0;
        }

        private bool GenerateXmlFile()
        {
            // get the xsd file
            var offenderVisitationXsdFile = ConfigurationManager.AppSettings["offenderVisitationXsdFile"];

            // if xsd file does not exist, notify someone
            if (!File.Exists(offenderVisitationXsdFile))
            {
                var message = new MailMessage
                {
                    From =
                        new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"],
                            "Visitation Applications (VADOC)")
                };
                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["offenderVisitationXmlEmailAddress"]));
                message.Subject = "Public Website - Visitation Issue - XSD File Is Missing";
                message.Priority = MailPriority.High;
                message.IsBodyHtml = true;

                var body = new StringBuilder();
                body.Append(
                    "The Offender Visitation XSD file does not exist. Please copy the file to the following location: " +
                    offenderVisitationXsdFile);

                message.Body = body.ToString();

                SendEmail(message);

                return false;
            }

            var applicantId = 1;
            var minorApplicantsXElement = new XElement("minorApplicants");

            // loop through minor visitors and generate xml for them
            foreach (var minorVisitor in MinorVisitors)
            {
                applicantId++;

                minorVisitor.ApplicantId = applicantId;

                minorApplicantsXElement.Add(
                    new XElement("minorApplicant",
                        new XElement("applicantId", applicantId),
                        new XElement("lastName", minorVisitor.LastName),
                        new XElement("firstName", minorVisitor.FirstName),
                        new XElement("middleName", minorVisitor.MiddleName),
                        !string.IsNullOrEmpty(minorVisitor.IdNumber)
                            ? new XElement("idCardNumber",
                                minorVisitor.IdNumber)
                            : null,
                        !string.IsNullOrEmpty(minorVisitor.IdType)
                            ? new XElement("idCardType",
                                minorVisitor.IdType)
                            : null,
                        !string.IsNullOrEmpty(minorVisitor.Ssn)
                            ? new XElement("ssn", minorVisitor.Ssn)
                            : null,
                        new XElement("race", minorVisitor.Race),
                        new XElement("gender", minorVisitor.Gender),
                        new XElement("birthDate",
                            minorVisitor.YearDob + "-" + minorVisitor.MonthDob + "-" + minorVisitor.DayDob),
                        new XElement("placeOfBirth", minorVisitor.PlaceOfBirth),
                        !string.IsNullOrEmpty(minorVisitor.EthnicOrigin)
                            ? new XElement("ethnicOrigin",
                                minorVisitor.EthnicOrigin)
                            : null));
            }

            const int adultVisitorApplicantId = 1;
            var offendersXElement = new XElement("offenders");

            // loop through offenders and generate xml for them
            foreach (var offender in Offenders)
            {
                var visitationApplicantsXElement = new XElement("visitationApplicants");

                // adult visitor's offender entry
                visitationApplicantsXElement.Add(
                    new XElement("visitationApplicant",
                        new XElement("applicantId", adultVisitorApplicantId),
                        new XElement("contactType", offender.ContactType),
                        offender.ContactType == "13" && !string.IsNullOrEmpty(offender.RelationshipQualifier)
                            ? new XElement("relationshipQualifier", offender.RelationshipQualifier)
                            : null,
                        offender.ContactType == "13" && !string.IsNullOrEmpty(offender.RelationshipType)
                            ? new XElement("relationshipTypeId", offender.RelationshipType)
                            : null));

                // minor visitors' offender entries
                var offender1 = offender;
                var minorsForOffender = MinorVisitors.Where(x => x.OffenderId == offender1.Id);

                if (minorsForOffender != null)
                {
                    foreach (var minorVisitor in minorsForOffender)
                    {
                        visitationApplicantsXElement.Add(
                            new XElement("visitationApplicant",
                                new XElement("applicantId", minorVisitor.ApplicantId),
                                new XElement("contactType", minorVisitor.ContactType),
                                minorVisitor.ContactType == "13" &&
                                !string.IsNullOrEmpty(minorVisitor.RelationshipQualifier)
                                    ? new XElement("relationshipQualifier", minorVisitor.RelationshipQualifier)
                                    : null,
                                minorVisitor.ContactType == "13" && !string.IsNullOrEmpty(minorVisitor.RelationshipType)
                                    ? new XElement("relationshipTypeId", minorVisitor.RelationshipType)
                                    : null));
                    }
                }

                offendersXElement.Add(
                    new XElement("offender",
                        new XElement("offenderId", offender.Number),
                        new XElement("locationId", offender.Facility),
                        visitationApplicantsXElement));
            }

            var doc = new XDocument(
                new XElement("visitationRegistrationRequest",
                    new XElement("registrationId", Guid.NewGuid().ToString("N")),
                    new XElement("requestDate", DateTime.Now.ToString("yyyy-MM-dd")),
                    new XElement("primaryApplicant",
                        new XElement("applicantId", adultVisitorApplicantId),
                        new XElement("lastName", txtVisitorLegalLastName.Text),
                        new XElement("firstName", txtVisitorLegalFirstName.Text),
                        new XElement("middleName", txtVisitorLegalMiddleName.Text),
                        !string.IsNullOrEmpty(txtVisitorMaidenName.Text)
                            ? new XElement("maidenName",
                                txtVisitorMaidenName.Text)
                            : null,
                        !string.IsNullOrEmpty(txtVisitorAliasName.Text)
                            ? new XElement("alias",
                                txtVisitorAliasName.Text)
                            : null,
                        new XElement("idCardNumber", txtIdNumber.Text),
                        new XElement("idCardType", ddlIdType.SelectedValue),
                        new XElement("ssn", txtSsn.Text),
                        new XElement("race", ddlRace.SelectedValue),
                        new XElement("gender", ddlGender.SelectedValue),
                        new XElement("birthDate",
                            ddlYearDateOfBirth.SelectedValue + "-" + ddlMonthDateOfBirth.SelectedValue + "-" +
                            ddlDayDateOfBirth.SelectedValue),
                        new XElement("placeOfBirth", txtPlaceOfBirth.Text),
                        !string.IsNullOrEmpty(ddlEthnicOrigin.SelectedValue)
                            ? new XElement("ethnicOrigin", ddlEthnicOrigin.SelectedValue)
                            : null,
                        new XElement("hairColor", ddlHairColor.SelectedValue),
                        new XElement("eyeColor", ddlEyeColor.SelectedValue),
                        new XElement("heightFt", txtHeightFeet.Text),
                        new XElement("heightIn", txtHeightInches.Text),
                        new XElement("weight", txtWeight.Text),
                        new XElement("streetAddress", txtStreetAddress.Text),
                        ddlState.SelectedValue == "1047"
                            ? new XElement("city",
                                ddlCity.SelectedValue)
                            : null,
                        ddlState.SelectedValue != "1047"
                            ? new XElement("outOfStateCity",
                                txtCityTownOfResidence.Text)
                            : null,
                        !string.IsNullOrEmpty(ddlState.SelectedValue)
                            ? new XElement("state", ddlState.SelectedValue)
                            : null,
                        new XElement("zipCode", txtZip.Text),
                        new XElement("country", ddlCountry.SelectedValue),
                        new XElement("emailAddress", txtEmailAddress.Text),
                        new XElement("phoneNumber", txtPhoneNumber.Text),
                        new XElement("phoneType", ddlPhoneType.SelectedValue),
                        new XElement("emancipatedMinor", chkEmancipatedMinor.Checked ? "Y" : "N"),
                        rblHaveVehicle.SelectedValue.ToLower() == "yes"
                            ? new XElement("vehicles", new XElement("vehicleInfo",
                                new XElement("make", txtVehicleMake.Text),
                                new XElement("model", txtVehicleModel.Text),
                                new XElement("modelYear", ddlVehicleYear.SelectedValue),
                                new XElement("plateNumber", txtVehiclePlateNumber.Text)))
                            : null),
                    MinorVisitors.Any() ? new XElement(minorApplicantsXElement) : null,
                    new XElement(offendersXElement)));

            var offenderVisitationXmlStagingFolder =
                ConfigurationManager.AppSettings["offenderVisitationXmlStagingFolder"];
            var xmlFilename = "VisitorApplication_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml";

            // create staging directory, if it doesn't exist
            if (!Directory.Exists(offenderVisitationXmlStagingFolder))
                Directory.CreateDirectory(offenderVisitationXmlStagingFolder);

            // save file to staging location for validation
            doc.Save(offenderVisitationXmlStagingFolder + xmlFilename);

            // load xml file for validation
            var xdoc = XDocument.Load(offenderVisitationXmlStagingFolder + xmlFilename);
            var schemas = new XmlSchemaSet();
            schemas.Add(string.Empty, offenderVisitationXsdFile);

            try
            {
                // validate xml file against xsd
                xdoc.Validate(schemas, null);
            }
            catch (XmlSchemaValidationException ex)
            {
                // if unsuccessful, move file to failed folder
                var offenderVisitationXmlFailedFolder =
                    ConfigurationManager.AppSettings["offenderVisitationXmlFailedFolder"];

                // create failed directory, if it doesn't exist
                if (!Directory.Exists(offenderVisitationXmlFailedFolder))
                    Directory.CreateDirectory(offenderVisitationXmlFailedFolder);

                // move file
                File.Move(offenderVisitationXmlStagingFolder + xmlFilename,
                    offenderVisitationXmlFailedFolder + xmlFilename);

                // notify someone of failed validation
                var message = new MailMessage
                {
                    From =
                        new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"],
                            "Visitation Applications (VADOC)")
                };
                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["offenderVisitationXmlEmailAddress"]));
                message.Subject = "Public Website – Visitation Application – Message Failed XML Validation";
                message.Priority = MailPriority.High;
                message.IsBodyHtml = true;

                var body = new StringBuilder();
                body.Append(
                    "The generated Offender Visitation XML file (" + xmlFilename +
                    ") did not validate against the XSD file and has been moved to the failed folder (" +
                    offenderVisitationXmlFailedFolder + ").<br /><br />The error message is: " +
                    ex.Message);

                message.Body = body.ToString();

                SendEmail(message);

                return false;
            }

            // if successful, move file to transmit folder
            var offenderVisitationXmlTransmitFolder =
                ConfigurationManager.AppSettings["offenderVisitationXmlTransmitFolder"];

            // create transmit directory, if it doesn't exist
            if (!Directory.Exists(offenderVisitationXmlTransmitFolder))
                Directory.CreateDirectory(offenderVisitationXmlTransmitFolder);

            // move file
            File.Move(offenderVisitationXmlStagingFolder + xmlFilename,
                offenderVisitationXmlTransmitFolder + xmlFilename);

            return true;
        }

        private void SendVisitationEmails()
        {
            var message = new MailMessage
            {
                From =
                        new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"],
                                        "Visitation Applications (VADOC)")
            };
            message.To.Add(new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"]));
            message.Subject = "Visitation Form Submission";
            message.IsBodyHtml = true;

            const string htmlBreak = "<br />";
            var body = new StringBuilder();
            body.Append("<h3>Visitation Web Form<h3>" + htmlBreak);
            body.Append("<b>Visitor Information</b>" + htmlBreak);
            body.Append("Emancipated Minor? " + (chkEmancipatedMinor.Checked ? "Yes" : "No") + htmlBreak);
            body.Append("Visitor's Legal First Name: " + txtVisitorLegalFirstName.Text + htmlBreak);
            body.Append("Visitor's Legal Middle Name: " + txtVisitorLegalMiddleName.Text + htmlBreak);
            body.Append("Visitor's Legal Last Name: " + txtVisitorLegalLastName.Text + htmlBreak);
            body.Append("Visitor's Maiden Name: " + txtVisitorMaidenName.Text + htmlBreak);
            body.Append("Visitor's Alias Name: " + txtVisitorAliasName.Text + htmlBreak);
            body.Append("E-mail Address: " + txtEmailAddress.Text + htmlBreak);
            body.Append("Date of Birth: " + ddlMonthDateOfBirth.SelectedValue + "/" + ddlDayDateOfBirth.SelectedValue +
                        "/" + ddlYearDateOfBirth.SelectedValue + htmlBreak);
            body.Append("Place of Birth: " + txtPlaceOfBirth.Text + htmlBreak);
            body.Append("Race: " + ddlRace.SelectedItem.Text + htmlBreak);
            body.Append("Ethnic Origin: " + ddlEthnicOrigin.SelectedItem.Text + htmlBreak);
            body.Append("Gender: " + ddlGender.SelectedItem.Text + htmlBreak);
            body.Append("Eye Color: " + ddlEyeColor.SelectedItem.Text + htmlBreak);
            body.Append("Hair Color: " + ddlHairColor.SelectedItem.Text + htmlBreak);
            body.Append("Height: " + txtHeightFeet.Text + " Ft. " + txtHeightInches.Text + " In." + htmlBreak);
            body.Append("Weight: " + txtWeight.Text + " Lbs." + htmlBreak);
            body.Append("Street Address/PO Box: " + txtStreetAddress.Text + htmlBreak);
            body.Append("City/Town/Other Principal Subdivision: " +
                        (ddlCity.SelectedIndex > 0 ? ddlCity.SelectedItem.Text : txtCityTownOfResidence.Text) +
                        htmlBreak);
            body.Append("State: " + ddlState.SelectedItem.Text + htmlBreak);
            body.Append("Postal Code (ZIP): " + txtZip.Text + htmlBreak);
            body.Append("Country: " + ddlCountry.SelectedItem.Text + htmlBreak);
            body.Append("Phone Number: " + txtPhoneNumber.Text + htmlBreak);
            body.Append("Phone Type: " + ddlPhoneType.SelectedItem.Text + htmlBreak);
            body.Append("ID Number: " + txtIdNumber.Text + htmlBreak);
            body.Append("ID Type: " + ddlIdType.SelectedItem.Text + htmlBreak);
            body.Append("SSN (last 4): " + txtSsn.Text + htmlBreak + htmlBreak);

            foreach (var offender in Offenders)
            {
                body.Append("<b>Information on Offender</b>" + htmlBreak);
                body.Append("Offender's Incarcerated Name: " + offender.Name + htmlBreak);
                body.Append("Offender's Incarcerated Number: " + offender.Number + htmlBreak);
                body.Append("Offender's Facility: " +
                            (!string.IsNullOrEmpty(offender.Facility)
                                ? ddlOffenderFacility.Items.FindByValue(offender.Facility).Text
                                : string.Empty) +
                            htmlBreak);
                body.Append("Relationship to Offender: " + (!string.IsNullOrEmpty(offender.ContactType)
                    ? rblContactType.Items.FindByValue(offender.ContactType).Text
                    : string.Empty) + " (" +
                            (!string.IsNullOrEmpty(offender.RelationshipQualifier)
                                ? ddlRelationshipQualifier.Items.FindByValue(offender.RelationshipQualifier).Text + " "
                                : string.Empty) +
                            (!string.IsNullOrEmpty(offender.RelationshipType)
                                ? ddlRelationshipType.Items.FindByValue(offender.RelationshipType).Text
                                : string.Empty) + ")" + htmlBreak +
                            htmlBreak);

                var offenderId = offender.Id;
                var minorsForOffender = MinorVisitors.Where(x => x.OffenderId == offenderId);
                foreach (var minor in minorsForOffender)
                {
                    body.Append("<b>Minor Visitor Information</b>" + htmlBreak);
                    body.Append("Minor Visitor's Legal First Name: " + minor.FirstName + htmlBreak);
                    body.Append("Minor Visitor's Legal Middle Name: " + minor.MiddleName + htmlBreak);
                    body.Append("Minor Visitor's Legal Last Name: " + minor.LastName + htmlBreak);
                    body.Append("ID Number: " + minor.IdNumber + htmlBreak);
                    body.Append("ID Type: " +
                                (!string.IsNullOrEmpty(minor.IdType)
                                    ? ddlMinorIdType.Items.FindByValue(minor.IdType).Text
                                    : string.Empty) + htmlBreak);
                    body.Append("SSN (last 4): " + minor.Ssn + htmlBreak);
                    body.Append("Race: " +
                                (!string.IsNullOrEmpty(minor.Race)
                                    ? ddlMinorRace.Items.FindByValue(minor.Race).Text
                                    : string.Empty) + htmlBreak);
                    body.Append("Ethnic Origin: " +
                                (!string.IsNullOrEmpty(minor.EthnicOrigin)
                                    ? ddlMinorEthnicOrigin.Items.FindByValue(minor.EthnicOrigin).Text
                                    : string.Empty) +
                                htmlBreak);
                    body.Append("Gender: " +
                                (!string.IsNullOrEmpty(minor.Gender)
                                    ? ddlMinorGender.Items.FindByValue(minor.Gender).Text
                                    : string.Empty) + htmlBreak);
                    body.Append("Date of Birth: " + minor.MonthDob + "/" + minor.DayDob + "/" + minor.YearDob +
                                htmlBreak);
                    body.Append("Place of Birth: " + minor.PlaceOfBirth + htmlBreak);
                    body.Append("Minor's relationship to Offender: " + (!string.IsNullOrEmpty(minor.ContactType)
                        ? rblMinorContactType.Items.FindByValue(minor.ContactType).Text
                        : string.Empty) + " (" + (!string.IsNullOrEmpty(minor.RelationshipQualifier)
                            ? ddlMinorRelationshipQualifier.Items.FindByValue(minor.RelationshipQualifier).Text + " "
                            : string.Empty) + (!string.IsNullOrEmpty(minor.RelationshipType)
                                ? ddlMinorRelationshipType.Items.FindByValue(minor.RelationshipType).Text
                                : string.Empty) + ")" +
                                htmlBreak);
                    body.Append("<b>Conditions (Minor Visitor)</b>" + htmlBreak);
                    body.Append("Has the minor been convicted of a felony in any jurisdiction? " + minor.ConvictedFelon +
                                htmlBreak);
                    body.Append("Is the minor currently under active Court supervision? " + minor.UnderActiveSupervision +
                                htmlBreak);
                    body.Append(
                        "Is the minor a victim or family of a victim of the current crime committed by the offender with whom they wish to visit? " +
                        minor.VictimOfCurrentCrime + htmlBreak);
                    body.Append(
                        "Is the minor now or have they ever been a member or associated with any gang, motorcycle club, racial supremacy group, or other such group or organization as defined in Code of Virginia &sect;18.2-46.1? " +
                        minor.GangAssociation + htmlBreak);
                    body.Append(
                        "I am the parent or legal guardian of the minor identified above or I am presenting written notarized approval from the parent or legal guardian for the minor to visit with me: " +
                        (minor.ParentLegalGuardian ? "Yes" : "No") + htmlBreak + htmlBreak);
                }
            }

            body.Append("<b>Vehicle Information</b>" + htmlBreak);
            body.Append("Own a vehicle? " + rblHaveVehicle.SelectedValue + htmlBreak);
            body.Append("Make: " + txtVehicleMake.Text + htmlBreak);
            body.Append("Model: " + txtVehicleModel.Text + htmlBreak);
            body.Append("Year: " + ddlVehicleYear.SelectedValue + htmlBreak);
            body.Append("Plate Number: " + txtVehiclePlateNumber.Text + htmlBreak + htmlBreak);

            body.Append("<b>Conditions</b>" + htmlBreak);
            body.Append("Have you been convicted of a felony in any jurisdiction? " +
                        rblFelonyConviction.SelectedValue + htmlBreak);
            body.Append(
                "Have you ever been employed by, volunteered with, or contracted by the Virginia Department of Corrections or Virginia Department of Correctional Education? " +
                rblEmployeeOfDoc.SelectedValue + htmlBreak);
            body.Append("Are you currently under active parole or probation supervision? " +
                        rblUnderActiveParole.SelectedValue + htmlBreak);
            body.Append("P&P District: " + ddlProbationParoleDistrict.SelectedItem.Text + htmlBreak);
            body.Append("DOC ID Number: " + txtDocIdNumber.Text + htmlBreak);
            body.Append(
                "Are you a victim or family of a victim of the current crime committed by the offender with whom you wish to visit? " +
                rblVictimOfCurrentCrime.SelectedValue + htmlBreak);
            body.Append(
                "Are you now or have you ever been a member or associated with any gang, motorcycle club, racial supremacy group, or other such group or organization as defined in Code of Virginia &sect;18.2-46.1? " +
                rblAssociatedWithGang.SelectedValue + htmlBreak + htmlBreak);

            body.Append(
                "I authorize the Department of Corrections to conduct a criminal records check, or to use any Department of Corrections records to verify accuracy of information provided on this form: " +
                (chkAuthorizeRecordCheck.Checked ? "Yes" : "No") + htmlBreak + htmlBreak);

            message.Body = body.ToString();

            SendEmail(message);

            message = new MailMessage
            {
                From = new MailAddress(ConfigurationManager.AppSettings["visitationApplicationsEmailAddress"], "Visitation Applications (VADOC)")
            };
            message.To.Add(new MailAddress(txtEmailAddress.Text));
            message.Subject = "Virginia Department of Corrections Visitation Application";
            message.IsBodyHtml = true;

            var now = DateTime.Now;

            body = new StringBuilder();
            body.Append(txtVisitorLegalFirstName.Text + " " + txtVisitorLegalLastName.Text + ":" + htmlBreak + htmlBreak);
            body.Append("Your Visitation Application was submitted at " + now.ToString("h:mm") + " " + (now.Hour > 11 ? "p.m." : "a.m.") + " on " + now.ToString("MMMM dd, yyyy") + "." + htmlBreak + htmlBreak);
            body.Append("In-state applicants should allow 30 days for applications to be processed. Out of state applicants should allow 90 days." + htmlBreak + htmlBreak);
            body.Append("Information regarding visitation can be found on our website at: http://vadoc.virginia.gov/offenders/visitation/default.shtm.");

            message.Body = body.ToString();

            SendEmail(message);
        }

        private static void SendEmail(MailMessage message)
        {
            var client = new SmtpClient(ConfigurationManager.AppSettings["smtpClientAddress"]);

            client.Send(message);
        }

        /// <summary>
        /// Offender class
        /// </summary>
        [Serializable]
        public class Offender
        {
            /// <summary>
            /// Id
            /// </summary>
            public int Id;
            /// <summary>
            /// Name
            /// </summary>
            public string Name;
            /// <summary>
            /// Number
            /// </summary>
            public string Number;
            /// <summary>
            /// Facility
            /// </summary>
            public string Facility;
            /// <summary>
            /// Facility Name
            /// </summary>
            public string FacilityName;
            /// <summary>
            /// ContactType
            /// </summary>
            public string ContactType;
            /// <summary>
            /// RelationshipQualifier
            /// </summary>
            public string RelationshipQualifier;
            /// <summary>
            /// RelationshipType
            /// </summary>
            public string RelationshipType;
            /// <summary>
            /// RelationshipName
            /// </summary>
            public string RelationshipName;
        }

        /// <summary>
        /// MinorVisitor class
        /// </summary>
        [Serializable]
        public class MinorVisitor
        {
            /// <summary>
            /// Id
            /// </summary>
            public int Id;
            /// <summary>
            /// OffenderId
            /// </summary>
            public int? OffenderId;
            /// <summary>
            /// FirstName
            /// </summary>
            public string FirstName;
            /// <summary>
            /// MiddleName
            /// </summary>
            public string MiddleName;
            /// <summary>
            /// LastName
            /// </summary>
            public string LastName;
            /// <summary>
            /// IdNumber
            /// </summary>
            public string IdNumber;
            /// <summary>
            /// IdType
            /// </summary>
            public string IdType;
            /// <summary>
            /// SSN
            /// </summary>
            public string Ssn;
            /// <summary>
            /// Race
            /// </summary>
            public string Race;
            /// <summary>
            /// EthnicOrigin
            /// </summary>
            public string EthnicOrigin;
            /// <summary>
            /// Gender
            /// </summary>
            public string Gender;
            /// <summary>
            /// MonthDob
            /// </summary>
            public string MonthDob;
            /// <summary>
            /// DayDob
            /// </summary>
            public string DayDob;
            /// <summary>
            /// YearDob
            /// </summary>
            public string YearDob;
            /// <summary>
            /// PlaceOfBirth
            /// </summary>
            public string PlaceOfBirth;
            /// <summary>
            /// ContactType
            /// </summary>
            public string ContactType;
            /// <summary>
            /// RelationshipQualifier
            /// </summary>
            public string RelationshipQualifier;
            /// <summary>
            /// RelationshipType
            /// </summary>
            public string RelationshipType;
            /// <summary>
            /// ConvictedFelon
            /// </summary>
            public string ConvictedFelon;
            /// <summary>
            /// UnderActiveSupervision
            /// </summary>
            public string UnderActiveSupervision;
            /// <summary>
            /// VictimOfCurrentCrime
            /// </summary>
            public string VictimOfCurrentCrime;
            /// <summary>
            /// GangAssociation
            /// </summary>
            public string GangAssociation;
            /// <summary>
            /// ParentLegalGuardian
            /// </summary>
            public bool ParentLegalGuardian;
            /// <summary>
            /// ApplicantId
            /// </summary>
            public int? ApplicantId;
        }

        /// <summary>
        /// Visitation Application Form Submission Model
        /// </summary>
        public class VisitationApplicationSubmission
        {
            private string vadocemployee;
            [Required]
            public string VADOCEmployee
            {
                get { return vadocemployee; }
                set { vadocemployee = value.Trim().ToLower(); }
            }

            private string vadocid;

            public string VADOCId
            {
                get { return vadocid; }
                set { vadocid = value.Trim().ToLower(); }
            }

            private string activeparole;

            [Required]
            public string ActiveParole
            {
                get { return activeparole; }
                set { activeparole = value.Trim().ToLower(); }
            }

            private string ppdistrict;

            public string PpDistrict
            {
                get { return ppdistrict; }
                set { ppdistrict = value.Trim().ToLower(); }
            }

            private string applicantcity;
            [Required]
            public string ApplicantCity
            {
                get { return applicantcity; }
                set { applicantcity = value.Trim().ToLower(); }
            }

            private string applicantcountrycode;

            [Required]
            public string ApplicantCountryCode
            {
                get { return applicantcountrycode; }
                set { applicantcountrycode = value.Trim().ToLower(); }
            }

            private string applicantcountryname;

            public string ApplicantCountryName
            {
                get { return applicantcountryname; }
                set { applicantcountryname = value.Trim().ToLower(); }
            }

            [Required]
            [DataType(DataType.Date)]
            public DateTime ApplicantDOB { get; set; }

            [Required(ErrorMessage = "Email is required.")]
            [EmailAddress(ErrorMessage = "Please provide a valid email address.")]
            public string ApplicantEmail { get; set; }

            private string applicanteyes;
            [Required]
            public string ApplicantEyes
            {
                get { return applicanteyes; }
                set { applicanteyes = value.Trim().ToLower(); }
            }

            private string applicanteyesname;

            public string ApplicantEyesName
            {
                get { return applicanteyesname; }
                set { applicanteyesname = value.Trim().ToLower(); }
            }

            private string applicanthair;
            [Required]
            public string ApplicantHair
            {
                get { return applicanthair; }
                set { applicanthair = value.Trim().ToLower(); }
            }

            private string applicanthairname;

            public string ApplicantHairName
            {
                get { return applicanthairname; }
                set { applicanthairname = value.Trim().ToLower(); }
            }

            private string applicantheightft;
            [Required]
            public string ApplicantHeightFt
            {
                get { return applicantheightft; }
                set { applicantheightft = value.Trim(); }
            }

            private string applicantheightin;
            [Required]
            public string ApplicantHeightIn
            {
                get { return applicantheightin; }
                set { applicantheightin = value.Trim(); }
            }

            private string applicantidnumber;
            [Required]
            public string ApplicantIdNumber
            {
                get { return applicantidnumber; }
                set { applicantidnumber = value.Trim(); }
            }

            private string applicantidtype;
            [Required]
            public string ApplicantIdType
            {
                get { return applicantidtype; }
                set { applicantidtype = value.Trim().ToLower(); }
            }

            private string applicantidtypename;
            public string ApplicantIdTypeName
            {
                get { return applicantidtypename; }
                set { applicantidtypename = value.Trim().ToLower(); }
            }

            private string applicantphone;
            [Required]
            [DataType(DataType.PhoneNumber)]
            [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
            public string ApplicantPhone
            {
                get { return applicantphone; }
                set { applicantphone = value.Trim().ToLower(); }
            }

            private string applicantphonetype;
            [Required]
            public string ApplicantPhoneType
            {
                get { return applicantphonetype; }
                set { applicantphonetype = value.Trim().ToLower(); }
            }

            private string applicantphonetypename;
            public string ApplicantPhoneTypeName
            {
                get { return applicantphonetypename; }
                set { applicantphonetypename = value.Trim().ToLower(); }
            }

            private string applicantrace;
            [Required]
            public string ApplicantRace
            {
                get { return applicantrace; }
                set { applicantrace = value.Trim().ToLower(); }
            }

            private string applicantracename;

            public string ApplicantRaceName
            {
                get { return applicantracename; }
                set { applicantracename = value.Trim().ToLower(); }
            }

            private string ethnicorigin;
            public string EthnicOrigin
            {
                get { return ethnicorigin; }
                set { ethnicorigin = value.Trim().ToLower(); }
            }

            private string applicantsex;
            [Required]
            public string ApplicantSex
            {
                get { return applicantsex; }
                set { applicantsex = value.Trim().ToLower(); }
            }

            private string applicantsexname;

            public string ApplicantSexName
            {
                get { return applicantsexname; }
                set { applicantsexname = value.Trim().ToLower(); }
            }

            private string applicantssn;
            [Required]
            public string ApplicantSsn
            {
                get { return applicantssn; }
                set { applicantssn = value.Trim(); }
            }

            private string applicantstate;
            [Required]
            public string ApplicantState
            {
                get { return applicantstate; }
                set { applicantstate = value.Trim().ToLower(); }
            }

            private string applicantstatenumcode;
            public string ApplicantStateNumCode
            {
                get { return applicantstatenumcode; }
                set { applicantstatenumcode = value.Trim().ToLower(); }
            }

            private string applicantstreet;
            [Required]
            public string ApplicantStreet
            {
                get { return applicantstreet; }
                set { applicantstreet = value.Trim().ToLower(); }
            }

            private string applicantweight;
            [Required]
            public string ApplicantWeight
            {
                get { return applicantweight; }
                set { applicantweight = value.Trim(); }
            }

            private string applicantzip;
            [Required]
            public string ApplicantZip
            {
                get { return applicantzip; }
                set { applicantzip = value.Trim(); }
            }

            private string applicantrehire;
            public string ApplicantRehire
            {
                get { return applicantrehire; }
                set { applicantrehire = value.Trim().ToLower(); }
            }

            private string employmentalias;
            public string EmploymentAlias
            {
                get { return employmentalias; }
                set { employmentalias = value.Trim().ToLower(); }
            }

            [DataType(DataType.Date)]
            public DateTime EmploymentStartDate { get; set; }

            [DataType(DataType.Date)]
            public DateTime EmploymentEndDate { get; set; }

            private string employmentstatus;
            public string EmploymentStatus
            {
                get { return employmentstatus; }
                set { employmentstatus = value.Trim().ToLower(); }
            }

            private string employmenttitle;
            public string EmploymentTitle
            {
                get { return employmenttitle; }
                set { employmenttitle = value.Trim().ToLower(); }
            }

            private string felonyconviction;
            [Required]
            public string FelonyConviction
            {
                get { return felonyconviction; }
                set { felonyconviction = value.Trim().ToLower(); }
            }

            private string gangassociation;
            [Required]
            public string GangAssociation
            {
                get { return gangassociation; }
                set { gangassociation = value.Trim().ToLower(); }
            }

            private string protectiveorder;
            [Required]
            public string ProtectiveOrder
            {
                get { return protectiveorder; }
                set { protectiveorder = value.Trim().ToLower(); }
            }

            private string legalfirstname;
            [Required]
            public string LegalFirstName
            {
                get { return legalfirstname; }
                set { legalfirstname = value.Trim().ToLower(); }
            }

            private string legallastname;
            [Required]
            public string LegalLastName
            {
                get { return legallastname; }
                set { legallastname = value.Trim().ToLower(); }
            }

            private string legalmaidenname;
            public string LegalMaidenName
            {
                get { return legalmaidenname; }
                set { legalmaidenname = value.Trim().ToLower(); }
            }

            private string legalmiddlename;
            [Required]
            public string LegalMiddleName
            {
                get { return legalmiddlename; }
                set { legalmiddlename = value.Trim().ToLower(); }
            }

            private string alias;
            public string Alias
            {
                get { return alias; }
                set { alias = value.Trim().ToLower(); }
            }

            private string applicantplaceofbirth;
            public string ApplicantPlaceOfBirth
            {
                get { return applicantplaceofbirth; }
                set { applicantplaceofbirth = value.Trim().ToLower(); }
            }

            private string meetoffender;
            public string MeetOffender
            {
                get { return meetoffender; }
                set { meetoffender = value.Trim().ToLower(); }
            }

            public bool MinorCertification { get; set; }

            public string SelectedMinorsJson { get; set; }

            public string SelectedOffendersJson { get; set; }
            public List<Offender> SelectedOffenders { get; set; }

            public List<MinorVisitor> SelectedMinors { get; set; }

            [Required]
            public bool FinalAuthorization { get; set; }
        }
    }
}
