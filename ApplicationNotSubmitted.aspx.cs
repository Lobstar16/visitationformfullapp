﻿using System;

namespace COV.DOC.Visitation.WebApplication
{
    /// <summary>
    /// ApplicationNotSubmitted class
    /// </summary>
    public partial class ApplicationNotSubmitted : System.Web.UI.Page
    {
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
